<?php
/**
* SDK moneyBox para conexion a webservice api rest en moneybox.bussiness
*
* @author Angel Cantu Jauregui
* @date 01 Enero, 2019
*/
class moneyBox{
	const API= 'https://api.moneybox.business';
	const API_SANDBOX= 'https://sandbox.moneybox.business';
	const API_DEV= 'https://dev.moneybox.business';

	/**
	* URL del Webservice
	*/
	private $wsUrl= NULL;
	/**
	* variable UserAgent
	*/
	private $userAgent= "User-Agent: SDK-moneyBox";
	/**
	* se guarda la Cabecera Response
	*/
	private $headerResponse=NULL;
	/**
	* se guarda la Cabecera Request
	*/
	private $headerRequest=NULL;
	/**
	* parametros del usuario a enviar
	*/
	private $requestData=NULL;
	/**
	* se guarda el resultado en limpio en array
	*/
	private $result=NULL;
	/**
	* se guarda el resultado en limpio en json
	*/
	private $result_json=NULL;
	/**
	* se guarda el usuario
	*/
	private $user=NULL;
	/**
	* se guarda el password
	*/
	private $pass=NULL;
	/**
	* se guarda la ruta a consumir en el webservice
	*/
	private $path=NULL;
	/**
	* se guarda el mensaje de error
	*/
	private $error=NULL;
	/**
	* se guarda el codigo de error
	*/
	private $error_code=NULL;
	/**
	* se guarda los detalels del error
	*/
	private $error_details=NULL;

	/**
	* Establece el server moneyBox
	*
	* @param string $a el servidor a conectar
	*/
	public function setApiServer($a="api") {
		if( !strcmp($a, "sandbox") ) {
			$this->wsUrl= self::API_SANDBOX;
		}
		else if( !strcmp($a, "dev") ) {
			$this->wsUrl= self::API_DEV;
		}
		else {
			$this->wsUrl= self::API;
		}
	}

	/**
	* Retorna el Server moneyBox
	*
	* @return string contenido de variable
	*/
	public function getApiServer() {
		return $this->wsUrl;
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ((is_array($a) && $a["request_header"]) ? $a["request_header"].$this->getDataToSend():NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $response
	*
	* @param string si indica "json" retorna los datos en json parseado, lo contrario en arreglo
	* @return string contenido de variable
	*/
	public function getRespuesta($a=NULL) {
		return (!strcmp(strtolower($a), "json") ? $this->result_json:$this->result);
	}

	/**
	* Establece el contenido de la variable $response
	*
	* @param string contenido de variable
	*/
	public function setRespuesta($a=NULL) {
		# print_r($a);
		$this->result= ($a ? json_decode($a):NULL);
		$this->result_json= ($a ? $a:NULL);

		if( !isset($this->result->result) && !isset($this->result->error) && !isset($this->result->error_code) )
			$this->setError("No se obtuvo respuesta del servidor", "001");
		else if( isset($this->result->error_code) ) {
			$this->setError($this->result->error, $this->result->error_code);
		}
		
		if( count($this->result->debug) ) {
			$this->setErrorDetails($this->result->debug);
		}
		if( count($this->result->error_detail) ) {
			$this->setErrorDetails($this->result->error_detail);
		}
		if( count($this->result->error_details) ) {
			$this->setErrorDetails($this->result->error_details);
		}
	}

	/**
	* Retorna contenido de la variable $error
	*
	* @return string contenido de variable
	*/
	public function getError() {
		return ($this->error_code ? $this->error_code.' - ':'').$this->error;
	}

	/**
	* Retorna detalles del error
	*
	* @return string contenido de variable
	*/
	public function getErrorDetails() {
		if( count($this->error_details) )
			return $this->error_details;
		else
			return 0;
	}

	/**
	* establece detalles del error
	*
	* @return string contenido de variable
	*/
	public function setErrorDetails($a=NULL) {
		$this->error_details= $a;
	}

	/**
	* Establece el contenido de la variable $error
	*
	* @param string contenido de variable
	* @param string codigo de error
	*/
	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->error_code= ($c ? $c:NULL);
	}

	/**
	* Establece la ruta de consuta al webservice
	*
	* @param string ruta que va a consumir
	*/
	public function setPath($a=NULL) {
		$this->path= ($a ? $a:NULL);
	}

	/**
	* Retorna la ruta de consuta al webservice
	*
	* @return string ruta que va a consumir
	*/
	public function getPath() {
		return $this->path;
	}

	/**
	* Establece el nombre de usuario para conectar al webservice
	*
	* @param string el nombre de usuario
	*/
	public function setUsername($a=NULL) {
		$this->user= ($a ? $a:NULL);
	}

	/**
	* Retorna el nombre de usuario para conectar al webservice
	*
	* @return string el nombre de usuario
	*/
	public function getUsername() {
		return $this->user;
	}

	/**
	* Establece el password para conectar al webservice
	*
	* @param string el password
	*/
	public function setPassword($a=NULL) {
		$this->pass= ($a ? $a:NULL);
	}

	/**
	* Retorna el password para conectar al webservice
	*
	* @return string el password
	*/
	public function getPassword() {
		return $this->pass;
	}

	/**
	* Establece los datos a enviar al webservice
	*
	* @param array los datos
	*/
	public function setDataSend($a=NULL) {
		$this->requestData= ($a ? $a:NULL);
	}

	/**
	* Retorna los datos a enviar al webservice
	*
	* @return array los datos
	*/
	public function getDataSend() {
		return $this->requestData;
	}

	/**
	* Verifica cabeceras de una respuesta para determinar si fue error o exito, setea variable $success y $error
	*/
	public function debugResponse() {
		if( !$this->getHeaderResponse() )	$this->setError("no se pudo obtener respuesta del servidor");
		else {
			#echo "\n\n## REQUEST:\n";
			#print_r($this->getHeaderRequest());
			#echo "\n\n## RESPONSE:\n";
			#print_r($this->getHeaderResponse());

			$a= explode( "\r\n\r\n", $this->getHeaderResponse());
			if( !$a[1] ) {
				print_r($a);
				$this->setError("el paquete recibido por el webservice esta vacio");
			}
			else { 	
				if( strstr($a[0], "100 Continue") )
					$this->setRespuesta($a[2]);
				else
					$this->setRespuesta($a[1]);
			}
			unset($a);
		}
	}

	/**
	* Retorna la informacion estandarizada hacia el Werbservice
	*/
	private function getDataToSend() {
		$r= array( 
			"user"=>$this->user, 
			"pass"=>$this->pass, 
			"metodo"=>$this->path, 
			"data"=>$this->requestData
		);

	# print_r(json_encode($r));
	return json_encode($r);
	}

	/**
	* Enviar los datos al webservice de moneyBox
	*/
	public function sendToMoneyBox() {
		if( !$this->path )	$this->setError("no indico la ruta a consumir");
		else if( !$this->user )	$this->setError("no indico el nombre de usuario");
		else if( !$this->pass )	$this->setError("no indico la clave de acceso");
		else {
			$s= curl_init();
			curl_setopt($s, CURLOPT_URL, $this->getApiServer());
			curl_setopt($s, CURLOPT_HTTPHEADER, array($this->userAgent, "Content-Type: application/json; charset=UTF-8") );
			curl_setopt($s, CURLOPT_HEADER, 1 );
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $this->getDataToSend());
			curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
			/**
			* Habilita las siguientes lineas si tienes fallo en obtener el Certificado
			*/
			curl_setopt($s, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($s, CURLOPT_VERBOSE, TRUE);
			curl_setopt($s, CURLINFO_HEADER_OUT, true);
			curl_setopt($s, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			$resp= curl_exec($s);
			$rq= curl_getinfo($s);
			$this->setHeaderRequest($rq); // request
			$this->setHeaderResponse($resp); // response
			$this->debugResponse();
			curl_close($s);
			unset($rq, $resp, $s, $schema);
		}
	}

	/**
	* Funcion principal para comunicacion con el webservice
	*
	* @param string el nombre de usuario
	* @param string el password
	* @param string direccion de la ruta a consumir en el webservice
	* @param array datos a enviar
	*/
	public function __construct($username=NULL, $password=NULL, $webServicePathUrl=NULL, $data=NULL, $serverApi="api") {
		$this->path= ($webServicePathUrl ? $webServicePathUrl:NULL);
		$this->user= ($username ? $username:NULL);
		$this->pass= ($password ? $password:NULL);
		$this->requestData= ((is_array($data) && count($data)) ? $data:NULL);
		$this->setApiServer($serverApi ? $serverApi:"api");
		$this->sendToMoneyBox();
	}
}
?>
