#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'demo';
$pass= 'gratis123';

/**
* departamento laboral - agregar
*/
/*
$path= 'nominas/departamentos/add';
$data= array("nombre"=>"Gerencia Administrativa");
*/

/**
* departamento laboral - listar
*/
#$path= 'nominas/departamentos/list';
#$data= array();

/**
* departamento laboral - actualizar
*/
/*
$path= 'nominas/departamentos/update';
$data= array(
	"id_departamento"=>"6pduas", 
	"nombre"=>"Gerencia Administrativa Mod"
);
*/

/**
* departamento laboral - eliminar
*/
#$path= 'nominas/departamentos/del';
#$data=array("id_departamento"=>"6pduas");

/**
* departamento laboral - buscar
*/
#$path= 'nominas/departamentos/search';
#$data=array("id_departamento"=>"ungy"); # busqueda por id
#$data=array("nombre"=>"Gerencia"); # busqueda por nombre

/**
* empleado - agregar
*/
/*
$path= 'nominas/empleados/create';
$data= array(
	"id_control"=>"123456", 			# numero de empleado
	"nombre"=>"Angel",					# nombre del empleado
	"apellidoP"=>"Cantu", 				# apellido paterno
	"apellidoM"=>"Jauregui", 			# apellido materno
	"email"=>"siegroupmx@gmai.com", 	# email del empleado
	"estado_civil"=>"3", 				# codigo de estado civil
	"sexo"=>"1", 						# codigo de sexualidad
	"telefono"=>"8998711722", 			# numero de telefono
	"calle"=>"20 de noviembre", 		# direccion calle
	"colonia"=>"Fracc reynosa", 		# direccion colonia
	"num_ext"=>"300", 					# direccion numero exterior
	"cp"=>"88780", 						# direccion codigo postal
	"ciudad"=>"810",					# codigo de ciudad
	"estado"=>"28", 					# codigo de estado
	"pais"=>"151", 						# codigo de pais
	"fecha_ingreso"=>"2020-01-01", 		# fecha que inicio relacion laboral
	"fecha_nacimiento"=>"1984-12-14", 	# fecha nacimiento
	"curp"=>"CAJA841214HTSNRN00", 		# curp para mexico
	# "imagen"=>"_imagen_bse64_",		# imagen en base64, solo JPG
	"rfc"=>"CAJA841214PA9", 			# rfc para mexico
	"banco_cuenta"=>"112233445566",		# numero de cuenta clabe
	"banco"=>"012", 					# numero de banco (lista SAT)
	"regimen_fiscal"=>"2", 				# codigo de regimen fiscal
	"nss"=>"F7778899", 					# numero de seguro social
	"riesgo_seguro"=>"1", 				# codigo de riesgo seguro laboral
	"jornada"=>"1", 					# codigo de jornada laboral
	"contrato"=>"1", 					# codigo tipo de contrato
	"empresa_departamento"=>"cjd9m", 	# codigo de departamento laboral
	"empresa_puesto"=>"Jefaso", 		# codigo de puesto laboral
	"salario_gravado"=>"1100", 			# salario gravado (opcional)
	"salario_exento"=>"0", 				# salario exento (opcional)
	"salario_he"=>"0", 					# salario por hora extra (opcional)
	"salario_hed"=>"0", 				# salario por hora extra doble (opcional)
	"salario_het"=>"0", 				# salario por hora extra triple (opcional)
	"conceptos"=>array(				# opcional
		"1"=>array(					# 1er concepto tipo percepcion, persibe 1,100.00
			"tipo"=>"p", 			# compuesto por 1,000 gravado y 100 exento
			"clave"=>"001", 		# usando clave 001 del catalogo del SAT
			"gravado"=>"1100", 
			"exento"=>"0"
		), 
		"2"=>array(					# 1er concepto tipo deduccion, deduce 100
			"tipo"=>"d", 			# compuesto por 80 gravado y 20 exento
			"clave"=>"D002", 		# usando clave 001 del catalogo del SAT
			"gravado"=>"100", 
			"exento"=>"0"
		)
	)
); # vacio
*/

/**
* empleado - actualizar
*/
/*
$path= 'nominas/empleados/update';
$data= array(
	"id_empleado"=>"jlopm", 			# id como se guardo en moneyBox
	"id_control"=>"2020202", 			# numero de empleado
	"nombre"=>"Angel",					# nombre del empleado
	"apellidoP"=>"Cantu", 				# apellido paterno
	"apellidoM"=>"Jauregui", 			# apellido materno
	"email"=>"siegroupmx@gmai.com", 	# email del empleado
	"estado_civil"=>"3", 				# codigo de estado civil
	"sexo"=>"1", 						# codigo de sexualidad
	"telefono"=>"8998711722", 			# numero de telefono
	"calle"=>"20 de noviembre", 		# direccion calle
	"colonia"=>"Fracc reynosa", 		# direccion colonia
	"num_ext"=>"300", 					# direccion numero exterior
	"cp"=>"88780", 						# direccion codigo postal
	"ciudad"=>"810",					# codigo de ciudad
	"estado"=>"28", 					# codigo de estado
	"pais"=>"151", 						# codigo de pais
	"fecha_ingreso"=>"2020-01-01", 		# fecha que inicio relacion laboral
	"fecha_nacimiento"=>"1984-12-14", 	# fecha nacimiento
	"curp"=>"CAJA841214HTSNRN00", 		# curp para mexico
	# "imagen"=>"_imagen_bse64_",		# imagen en base64, solo JPG
	"rfc"=>"CAJA841214PA9", 			# rfc para mexico
	"banco_cuenta"=>"112233445566",		# numero de cuenta clabe
	"banco"=>"012", 					# numero de banco (lista SAT)
	"regimen_fiscal"=>"2", 				# codigo de regimen fiscal
	"nss"=>"F7778899", 					# numero de seguro social
	"riesgo_seguro"=>"1", 				# codigo de riesgo seguro laboral
	"jornada"=>"1", 					# codigo de jornada laboral
	"contrato"=>"1", 					# codigo tipo de contrato
	"empresa_departamento"=>"cjd9m", 	# codigo de departamento laboral
	"empresa_puesto"=>"Jefaso", 		# codigo de puesto laboral
	"salario_gravado"=>"1100", 			# salario gravado (opcional)
	"salario_exento"=>"0", 				# salario exento (opcional)
	"salario_he"=>"0", 					# salario por hora extra (opcional)
	"salario_hed"=>"0", 				# salario por hora extra doble (opcional)
	"salario_het"=>"0", 				# salario por hora extra triple (opcional)
	"conceptos"=>array(				# opcional
		"1"=>array(					# 1er concepto tipo percepcion, persibe 1,100.00
			"tipo"=>"p", 			# compuesto por 1,000 gravado y 100 exento
			"clave"=>"001", 		# usando clave 001 del catalogo del SAT
			"gravado"=>"2100", 
			"exento"=>"0"
		), 
		"2"=>array(					# 1er concepto tipo deduccion, deduce 100
			"tipo"=>"d", 			# compuesto por 80 gravado y 20 exento
			"clave"=>"D002", 		# usando clave 001 del catalogo del SAT
			"gravado"=>"100", 
			"exento"=>"0"
		)
	)
); # vacio
*/

/**
* empleado - listar
*/
#$path= 'nominas/empleados/list';
#$data= array(); # vacio

/**
* empleado - obtener
*/
#$path= 'nominas/empleados/search';
#$data= array( "id_empleado"=>"vthmv2ui" ); # apartir del id de moneybox
#$data= array( "id_control"=>"2020202" ); # apartir del id de moneybox
#$data= array( "rfc"=>"121212" ); # apartir del rfc (mexico)
#$data= array( "nit"=>"121212" ); # apartir del nit (colombia)

/**
* empleado - eliminar
*/
#$path= 'nominas/empleados/delete';
#$data= array( "id_empleado"=>"vthmv2ui" ); # apartir del id de moneybox
#$data= array( "id_control"=>"123456" ); # apartir del id de moneybox
#$data= array( "rfc"=>"121212" ); # apartir del rfc (mexico)
#$data= array( "nit"=>"121212" ); # apartir del nit (colombia)

/**
* nomina - guardar (prenomina)
*/
/*
$path= 'nominas/deploy/save';
$data= array(
	"titulo"=>"Semana 040", 
	"fecha_corte_inicio"=>"2021-09-06",  # YYYY-MM-DD
	"fecha_corte_fin"=>"2021-09-12", # YYYY-MM-DD
	"moneda"=>"MXN", 
	"usocfd"=>"G03", 
	"metodo_pago"=>"03", 
	"nomina"=>array(
		"ymtm7"=>array(
			"p"=> array(
				"001"=> array(
					"exento"=>0, 
					"gravado"=>1100, 
					"total"=>1100
				) 
			),  
			"d"=>array(
				"D002"=>array(
					"exento"=>0, 
					"gravado"=>100, 
					"total"=>100
				) 
			) 
		)
	)
);
*/

/**
* nomina - listar
*/
#$path= 'nominas/deploy/list';
#$data= array();

/**
* nomina - buscar
*/
#$path= 'nominas/deploy/search';
#$data= array("nombre"=>"038");

/**
* nomina - estado de la nomina
*/
#$path= 'nominas/deploy/status';
#$data= array( "id_nomina"=>"56vm6pby");

/**
* nomina - obtener ultima nomina
*/
#$path= 'nominas/deploy/getlast';
#$data= array(); # por defecto 1 (valida)
#$data= array("estado"=>"2"); # semi-validada
#$data= array("estado"=>"0"); # sin validar

/**
* nomina - generar formalmente ante SAT
*/
#$path= 'nominas/deploy/gen';
#$data= array("id_nomina"=>"56vm6pby");

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) {
	echo "\n[Error] ". $mbox->getError(). "\n\n";
	print_r($mbox->getHeaderResponse());
}
else {
	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
?>
