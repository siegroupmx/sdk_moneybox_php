#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'chevrongpdsv';
$pass= 'Ch3c40n1.';
$idCliente= 'vf4';

/**
* factura - lista
*/
$path= 'factura/list';
$data= array("id_cliente"=>$idCliente, "tipo_factura"=>"03", "limite"=>"60", "pagina"=>1);
// $data= array(); # vacio

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) {
	echo "\n[Error] ". $mbox->getError();

	if( is_object($mbox->getErrorDetails()) ) {
		echo "\nDetalles:\n";
		print_r($mbox->getErrorDetails());
		echo "\n";
	}
}
else {
	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";

	$rF= $mbox->getRespuesta();
	$uuid= array();

	foreach( $rF->result as $k=>$v ) {
		if( !count($uuid) && !count($uuidDb[$v->timbre_fiscal]) && $v->timbre_fiscal ) {
			$uuid[]= array("uuid"=>$v->timbre_fiscal);
			$uuidDb[$v->timbre_fiscal]= 1;

			// echo "\n\n=== Target Cliente Factura Original..\n";
			// print_r($v);
			// echo "\n\n";
		}
	}

	echo "\n\n=== UUIDs ===\n";
	print_r($uuid);
}

echo "\n\nFin programa...\n\n";
exit(0);
?>
