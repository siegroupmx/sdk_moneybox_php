#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

// $user= 'urquinadavid';
// $pass= 'RositaDavid';

$user= 'cubillosco';
$pass= 'MariaCarlos';

// $user= 'merkayed';
// $pass= 'TatianaYesid';

// $user= 'pyrsas';
// $pass= 'DianaJesus';

$path= 'factura/list';
$totalMaxPaginas= 20;

for($i=0; $i<($totalMaxPaginas+1); $i++ ) {
	$data= array("tipo"=>"factura", "orden_por"=>"FECHA", "orden"=>"DESC", "orden_busqueda"=>array("timbre_fiscal"=>""), "pagina"=>($i+1), "limite"=>"50"); # vacio
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) {
		echo '[Error] '. $mbox->getError();
	}
	else {
		// /* Headers */
		// echo "\n\nHeaders Request:\n";
		// print_r($mbox->getHeaderRequest());
		// echo "\n\nHeaders Response:\n";
		$r= $mbox->getRespuesta();
		// print_r($r);

		echo "\nTotal documentos: ". $r->result->total;
		echo "\nPagina: ". $r->result->pagina;
		echo "\nQuery Rank: ". $r->result->rank_query;
		echo "\n\n------ Documentos -----";

		foreach( $r->result->documentos as $k=>$v ) {
			echo "\n\n-- Elemento ". ($k+1). " / ". $r->result->total;
			echo "\nID: ". $v->id;
			echo "\nCliente: ". $v->id_cliente;
			echo "\nFolio: [". $v->id_folio. "] ". ($v->serie ? $v->serie.'-':'').$v->folio;
			echo "\nFecha: ". date( "Y/m/d, H:i", $v->fecha);
			echo "\nUUID: ". ($v->timbre_fiscal ? $v->timbre_fiscal:'--');
			echo "\nValidacion: ". ($v->validacion ? $v->validacion:'--');
	        // echo "\nPulsa ENTER para continuar....";
	        // $rl= readline();

			$pathR= 'factura/regen';
			$dataR= array("id"=>$v->id); # vacio
			$mboxR= new moneyBox($user, $pass, $pathR, $dataR);

			if( $mboxR->getError() ) {
				echo "\nRegeneracion ERROR -- ". $mboxR->getError();
		        echo "\nPulsa ENTER para continuar....";
		        $rl= readline();
			}
			else {
				$rR= $mboxR->getRespuesta();
				echo "\n";
				// print_r($rR);

				if( !$rR->result->timbre_fiscal ) {
					echo "\nRegeneracion con fallos :(...";
					echo "\nCabecera...\n";
					print_r($mboxR->getHeaderResponse());
					$rRestore= $mboxR->getRespuesta();
					print_r($rRestore->result);
					echo "\nUUID: ". $rRestore->result["timbre_fiscal"];
			        echo "\nPulsa ENTER para continuar....";
			        $rl= readline();
				}
				else {
					echo "\nRegeneracion con exito...";
					echo "\nUUID: ". $rR->result->timbre_fiscal;
					echo "\nXML: ". $rR->result->url_xml;
					echo "\nPDF: ". $rR->result->url_pdf;
				}
				unset($rR);
			}
			unset($dataR, $mboxR);
		}
		unset($r);
	}
	unset($mbox, $data);
}

echo "\n\n";
echo "\n\nFin del programa...\n\n";
?>
