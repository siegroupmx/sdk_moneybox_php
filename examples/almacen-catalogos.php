#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'demo';
$pass= 'gratis123';

/**
* Catalogos
* consulta todos los catalogos
*/
#$path= 'almacen/catalogo/list';
#$data= array(); # vacio

/**
* Ejemplo de Respuesta
*/
/*
printr($mbox->getRespuesta());
stdClass Object
(
    [result] => Array
        (
            [0] => stdClass Object
                (
                    [id] => j5arvmye
                    [nombre] => Electronica
                    [url_nombre] => electronica
                    [sub_count] => 2
                    [sub_catalogo] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [id] => pk87wa352g
                                    [id_catalogo] => j5arvmye
                                    [nombre] => Cubo Rubyk
                                    [url_nombre] => cubo_rubyk
                                )

                            [1] => stdClass Object
                                (
                                    [id] => 9zn3w6md7n
                                    [id_catalogo] => j5arvmye
                                    [nombre] => Mouse Gamer
                                    [url_nombre] => mouse_gamer
                                )

                        )

                )

        )

    [error] => 
    [error_code] => 0
)

$r= $mbox->getRespuesta();
foreach( $r->result[0] as $key=>$val ) {
	echo "\n==> ". $key;
}
*/


/**
* Obtener un Catalogo especifico
* consulta de un catalogo especifico
*/
#$path= 'almacen/catalogo/get';
#$data= array(
#	"db"=>"catalogo", 
#	"url_nombre"=>"electronica" // busqueda por el nombre URL
	# "id"=>"j5arvmye" // opcional busqueda por ID
#);


/**
* Obtener un SubCatalogo especifico
* consulta de un Subcatalogo especifico
*/
#$path= 'almacen/catalogo/get';
#$data= array(
#	"db"=>"subcatalogo", 
#	"url_nombre"=>"cubo_rubyk" # busqueda por el nombre URL
	# "id"=>"pk87wa352g" // opcional busqueda por ID
#);

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	/* Headers */
	#echo "\n\nHeaders Request:\n";
	#print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
?>
