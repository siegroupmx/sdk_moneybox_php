#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'motopartsco';
$pass= 'NataliaAlexis';

/**
* departamento laboral - agregar
*/
/*
$path= 'nominas/departamentos/add';
$data= array("nombre"=>"Gerencia Administrativa");
*/

/**
* departamento laboral - listar
*/
#$path= 'nominas/departamentos/list';
#$data= array();

/**
* departamento laboral - actualizar
*/
/*
$path= 'nominas/departamentos/update';
$data= array(
	"id_departamento"=>"6pduas", 
	"nombre"=>"Gerencia Administrativa Mod"
);
*/

/**
* departamento laboral - eliminar
*/
#$path= 'nominas/departamentos/del';
#$data=array("id_departamento"=>"6pduas");

/**
* departamento laboral - buscar
*/
#$path= 'nominas/departamentos/search';
#$data=array("id_departamento"=>"ungy"); # busqueda por id
#$data=array("nombre"=>"Gerencia"); # busqueda por nombre


/**
* empleado - agregar
*/
/*
$path= 'nominas/empleados/create';
$data= array(
	"id_control"=>"123456", 			# numero de empleado
	"nombre"=>"Angel",					# nombre del empleado
	"apellidoP"=>"Cantu", 				# apellido paterno
	"apellidoM"=>"Jauregui", 			# apellido materno
	"email"=>"siegroupmx@gmai.com", 	# email del empleado
	"estado_civil"=>"3", 				# codigo de estado civil
	"sexo"=>"1", 						# codigo de sexualidad
	"telefono"=>"8998711722", 			# numero de telefono
	"direccion"=>"20 de noviembre", 	# direccion completa
	"cp"=>"88780", 						# direccion codigo postal
	"ciudad"=>"05001",					# codigo de ciudad: 05001=Medellin
	"estado"=>"05", 					# codigo de estado: 05=Antioquia
	"pais"=>"47", 						# codigo de pais
	"fecha_ingreso"=>"2020-01-01", 		# fecha que inicio relacion laboral
	"fecha_nacimiento"=>"1984-12-14", 	# fecha nacimiento
	# "imagen"=>"_imagen_base64_",		# imagen en base64, solo JPG
	"nit"=>"700200857", 				# NIT (cedula, pasaporte o nit) Completo
	"dv"=>"2", 							# Digito Verificador
	"banco_cuenta"=>"11223999", 		# numero de cuenta
	"banco"=>"07", 						# numero de banco (lista DIAN)
	"tipo_trabajador"=>"01", 			# codigo de Tipo de Trabajador
	"subtipo_trabajador"=>"00",			# codigo de Subtipo de Trabajador
	"jornada"=>"1", 					# codigo de jornada laboral
	"contrato"=>"1", 					# codigo tipo de contrato
	"empresa_departamento"=>"5ydu3g8", 	# codigo de departamento laboral
	"empresa_puesto"=>"Jefaso", 		# codigo de puesto laboral
	"salario_base"=>"1100", 			# salario base
	"total_devengados"=>"777777.70",	# total devengos/percepciones
	"total_deducciones"=>"555555.50",	# tota deducciones
	"conceptos"=>array(					# devengos, deducciones e incapacidades -- OPCIONAL
		"devengos"=>array(
			"1"=>"[0|P001|2222|20|10|0|0]", 					# transporte
			"2"=>"[0|P002|444|1|0|0|0]", 						# hed
			"3"=>"[0|P009|767676|1|1631106000|1631109600|0]", 	# vacaciones
			"4"=>"[0|P012|1880|89800.80|0|0|0]", 				# cesantia
			"5"=>"[0|P017|10|20|0|0|0]", 						# bonificacion
			"6"=>"[0|P018|130|250|0|0|0]" 						# auxilio
		), 
		"deducciones"=>array(
			"1"=>"[0|D001|120.50|20.30|0|0|0]", 	# salud
			"2"=>"[0|D004|440.00|16.50|0|0|0]", 	# sindicatos
			"3"=>"[0|D016|55634.19|0|0|0|0]"		# educacion
		)
	)
); # vacio
*/

/**
* empleado - actualizar
*/
/*
$path= 'nominas/empleados/update';
$data= array(
	"id_empleado"=>"rtd4it91", 		# id registro moneyBox
	"id_control"=>"202020", 			# numero de empleado
	"nombre"=>"Angel",					# nombre del empleado
	"apellidoP"=>"Cantu", 				# apellido paterno
	"apellidoM"=>"Jauregui", 			# apellido materno
	# "estatus"=>"2", 					# 1=activo (pordefecto), 2=baja, 3=incapacitado, 4=suspendido
	"email"=>"siegroupmx@gmai.com", 	# email del empleado
	"estado_civil"=>"3", 				# codigo de estado civil
	"sexo"=>"1", 						# codigo de sexualidad
	"telefono"=>"8998711722", 			# numero de telefono
	"direccion"=>"20 de noviembre", 	# direccion completa
	"cp"=>"88780", 						# direccion codigo postal
	"ciudad"=>"05001",					# codigo de ciudad: 05001=Medellin
	"estado"=>"05", 					# codigo de estado: 05=Antioquia
	"pais"=>"47", 						# codigo de pais
	"fecha_ingreso"=>"2020-01-01", 		# fecha que inicio relacion laboral
	"fecha_nacimiento"=>"1984-12-14", 	# fecha nacimiento
	# "imagen"=>"_imagen_base64_",		# imagen en base64, solo JPG
	"nit"=>"700200857", 				# NIT (cedula, pasaporte o nit) Completo
	"dv"=>"2", 							# Digito Verificador
	"banco_cuenta"=>"11223999", 		# numero de cuenta
	"banco"=>"07", 						# numero de banco (lista DIAN)
	"tipo_trabajador"=>"01", 			# codigo de Tipo de Trabajador
	"subtipo_trabajador"=>"00",			# codigo de Subtipo de Trabajador
	"jornada"=>"1", 					# codigo de jornada laboral
	"contrato"=>"1", 					# codigo tipo de contrato
	"empresa_departamento"=>"5ydu3g8", 	# codigo de departamento laboral
	"empresa_puesto"=>"Jefaso", 		# codigo de puesto laboral
	"salario_base"=>"1100", 			# salario base
	"total_devengados"=>"777777.70",	# total devengos/percepciones
	"total_deducciones"=>"555555.50",	# tota deducciones
	"conceptos"=>array(					# devengos, deducciones e incapacidades -- OPCIONAL
		"devengos"=>array(
			"1"=>"[0|P001|2222|20|10|0|0]" 					# transporte
		), 
		"deducciones"=>array(
			"1"=>"[0|D001|120.50|1|0|0|0]", 	# salud
			"2"=>"[0|D002|440.00|1|0|0|0]" 		# fondopension
		)
	)
); # vacio
*/
/**
* empleado - listar
*/
#$path= 'nominas/empleados/list';
#$data= array(); # vacio

/**
* empleado - obtener
*/
#$path= 'nominas/empleados/search';
#$data= array( "id_empleado"=>"vthmv2ui" ); # apartir del id de moneybox
#$data= array( "id_control"=>"2020202" ); # apartir del id de moneybox
#$data= array( "rfc"=>"121212" ); # apartir del rfc (mexico)
#$data= array( "nit"=>"121212" ); # apartir del nit (colombia)

/**
* empleado - eliminar
*/
#$path= 'nominas/empleados/delete';
#$data= array( "id_empleado"=>"vthmv2ui" ); # apartir del id de moneybox
#$data= array( "id_control"=>"123456" ); # apartir del id de moneybox
#$data= array( "rfc"=>"121212" ); # apartir del rfc (mexico)
#$data= array( "nit"=>"121212" ); # apartir del nit (colombia)

/**
* nomina - guardar (prenomina)
*/
/*
$path= 'nominas/deploy/save';
$data= array(
	"titulo"=>"Septiembre", 
	"fecha_corte_inicio"=>"2021-09-06",  # YYYY-MM-DD
	"fecha_corte_fin"=>"2021-10-03", # YYYY-MM-DD
	"moneda"=>"COP", 
	"moneda_vcambio"=>"1", 
	"metodo_pago"=>"46", 			# 46=transferencia, 10=efectivo
	"nomina"=>array(
		"rtd4it91"=>array(
			"dv"=>array(
				"1"=>"[0|P001|2222|20|10|0|0]"		# transporte
			), 
			"d"=>array(
				"1"=>"[0|D001|120.50|1|0|0|0]", 	# salud
				"2"=>"[0|D002|440.00|1|0|0|0]" 		# fondopension
			), 
			"total_devengos"=>"6652", 
			"total_deducciones"=>"560.50", 
			"total"=>"6091.50"
		)
	)
);
*/

/**
* nomina - listar
*/
$path= 'nominas/deploy/list';
$data= array();

/**
* nomina - buscar
*/
#$path= 'nominas/deploy/search';
#$data= array("nombre"=>"Septiembre");

/**
* nomina - estado de la nomina
*/
#$path= 'nominas/deploy/status';
#$data= array( "id_nomina"=>"s194hf");

/**
* nomina - obtener ultima nomina
*/
#$path= 'nominas/deploy/getlast';
#$data= array(); # por defecto 1 (valida)
#$data= array("estado"=>"2"); # semi-validada
#$data= array("estado"=>"0"); # sin validar

/**
* nomina - generar formalmente ante DIAN
*/
#$path= 'nominas/deploy/gen';
#$data= array("id_nomina"=>"gyjcr1cz4");

/**
* nomina - eliminar recibo
*/
// $path= 'nominas/deploy/del';
// $data= array("id_recibo"=>"gyjcr1cz4");
// $data= array("id_nomina"=>"gyjcr1cz4");

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) {
	echo '[Error] '. $mbox->getError();
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
else {
	/* Headers */
	// echo "\n\nHeaders Request:\n";
	// print_r($mbox->getHeaderRequest());

	// echo "\n\nHeaders Response:\n";
	// print_r($mbox->getHeaderResponse());
	// echo "\n\n";

	$r= $mbox->getRespuesta();
	
	foreach( $r->result as $k=>$v ) {
		foreach( $v->nomina as $k2=>$v2 ) {
			if( $v2->estado!=4 ) { // sin cancelar
				$recibo[]= $v2->id_recibo;
			}
		}
	}

	print_r($recibo);
}
?>
