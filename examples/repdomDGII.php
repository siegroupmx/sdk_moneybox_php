#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

function setCeros($a=NULL, $pos=NULL) {
	if( !$pos )
		return 0;
	else {
		if( strlen($a)==$pos ) { # misma cantidad de posiciones
			return $a; # devolvemos
		}
		else if( strlen($a)>$pos ) { # supera las posiciones
			return $a; # devolvemos
		}
		else {
			$need= ($pos-(strlen($a))); # calculamos los 0(ceros) que necesitamos
			$zero='';

			for($i=0; $i<$need; $i++ ) {
				$zero .= '0';
			}
			unset($need);

			return $zero.$a;
		}
	}
}
function cleanString($a=NULL, $cmd=NULL) {
	if( $a & $cmd ) {
		if( !strcmp($cmd, "end_linea") ) {
			$buscar= array("\n", "\0");
			$reempl= array("", "");
			return trim(str_replace($buscar, $reempl, $a));
		}
		else if( !strcmp($cmd, "all_spaces") ) {
			$buscar= array(" ");
			$reempl= array("");
			return trim(str_replace($buscar, $reempl, $a));
		}
	}
}

$user= 'demord';
$pass= 'gratis123';
$productList= 'lista.txt';

if( !file_exists($productList) ) {
	echo "\n[ERROR] El archivo ". $productList. " no existe..";
}
else {
	$receptorNrc= '130379-7-77'; // normal
	$receptorNrcExtr= '1111111111'; // extranjero
	$receptorNrcGob= '401506254'; // gobierno
	$path= 'cuenta/firmas/list';
	$data= array();
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] FIRM-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idFirma= $r->result[0]->id;
		$firmaData= $r->result[0];
	}

	$path= 'cuenta/formulas/list';
	$data= array(
	);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] FORMULA-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idFormula= $r->result[0]->id;
		$formulaData= $r->result[0];
	}

	$path= 'clientes/get';
	$data= array( "nit"=>$receptorNrcExtr);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] CLIENT-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idClienteExtr=$r->result[0]->id;
		$clienteDataExtr= $r->result[0];
	}

	$path= 'clientes/get';
	$data= array( "nit"=>$receptorNrcGob);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] CLIENT-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idClienteGob=$r->result[0]->id;
		$clienteDataGob= $r->result[0];
	}

	$path= 'clientes/list';
	$data= array( "nit"=>$receptorNrc);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] CLIENT-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idCliente=$r->result[0]->id;
		$clienteData= $r->result[0];
	}

	if( !$idFirma ) {
		echo "\n[ERROR] No se a detectado la firma electronica...";
	}
	else if( !$idFormula ) {
		echo "\n[ERROR] No se a detectado la Formula para calculo de impuestos...";
	}
	else if( !$idCliente ) {
		echo "\n[ERROR] No se a detectado el cliente receptor...";
	}
	else {
		echo "\n\n=== Clientes obtenidos...";

		// echo "\nID Cliente Gobierno: ". $idClienteGob;
		// echo "\nData Cliente:\n";
		// print_r($clienteDataGob);

		// echo "\n\nID Cliente Extranjero: ". $idClienteExtr;
		// echo "\nData Cliente:\n";
		// print_r($clienteDataExtr);

		// echo "\n\nID Cliente Regular: ". $idCliente;
		// echo "\nData Cliente:\n";
		// print_r($clienteData);
		
		$path= 'factura/save';

		echo "\n[*] Cliente: ". $idCliente. " - RNC: ". $clienteData->rfc. ", Nombre: ".$clienteData->nombre;
		echo "\n[*] Cliente Extranjero: ". $idClienteExtr. " - RNC: ". $clienteDataExtr->rfc. ", Nombre: ".$clienteDataExtr->nombre;
		echo "\n[*] Cliente Extranjero: ". $idClienteGob. " - RNC: ". $clienteDataGob->rfc. ", Nombre: ".$clienteDataGob->nombre;
		echo "\n[*] Firma: ". $idFirma. " - ". $formulaData->nombre;
		echo "\n[*] Formula: ". $idFormula. " - ". $firmaData->nombre;

		$cont=0;
		$errorLog=false;
		$startFolio= '0000001016';
		$doneFact=array();
		$totalFcf= 5; // 31 - FCF
		$totalFce= 2; // 32 - FCE
		$totalNd= 1; // 33 - ND
		$totalNc= 1; // 34 - NC
		$totalCec= 1; // 41 - CEC
		$totalCegm= 1; // 43 - Cegm
		$totalCere= 1; // 44 - Cere
		$totalCeg= 1; // 45 - Ceg
		$totalCee= 1; // 46 - Cee
		$totalCepe= 1; // 47 - Cepe
		$fp= fopen($productList, "r");

		$i=0;
		$tipoDoc=array( 
			array("tipo_doc"=>31, "envios"=>5, "name"=>"Factura de Cr&eacute;dito Fiscal Electr&oacute;nica"), 
			array("tipo_doc"=>32, "envios"=>2, "name"=>"Factura de Consumo Electr&oacute;nica"), 
			array("tipo_doc"=>33, "envios"=>1, "name"=>"Nota de D&eacute;bito Electr&oacute;nica"),
			array("tipo_doc"=>34, "envios"=>1, "name"=>"Nota de Cr&eacute;dito Electr&oacute;nica"),
			array("tipo_doc"=>41, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico de Compras"),
			array("tipo_doc"=>43, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico para Gastos Menores"),
			array("tipo_doc"=>44, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico para Reg&iacute;menes Especiales"),
			array("tipo_doc"=>45, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico Gubernamental"),
			array("tipo_doc"=>46, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico para Exportaciones"), 
			array("tipo_doc"=>47, "envios"=>1, "name"=>"Comprobante Electr&oacute;nico para Pagos al Exterior")
		);

		foreach( $tipoDoc as $k=>$v ) {
			$tipoDoc= $v["tipo_doc"];
			$totalDocs= $v["envios"];
			$i=0;

			while( (($buf=fgets($fp, (1024*10)))!==FALSE) && $i<$totalDocs && !$errorLog ) {
				if( !strcmp($tipoDoc, "34") ) { // NC
					fseek($fp, 0); // volvemos al inicio
					$buf=fgets($fp, (1024*10)); // leemos primer dato de nuevo
				}

				$cont++;
				$i++;
				$itbis= '18';
				$itbis2= '16';
				$itbis3= '0';				
				$conceptos=array();
				$x= explode("\t", cleanString($buf, "end_linea"));
				$titulo= cleanString($x[1], "end_linea");
				$precio= number_format(cleanString($x[0], "all_spaces"), 2, '.', '');
				$impuesto=0;
				echo "\n[E". $tipoDoc.($startFolio+$cont). "]". $titulo. " -- ". $precio;

				#
				# extra impuestos
				#
				$extrasImp=array();
				$indicadorNc=0;
				$contenedor="";
				$ref="";
				$ajustedoc=0;
				$retIva=0;
				$retIsr=0;

				if( !strcmp($tipoDoc, "32") ) {
					$formaPago=1;
					$cant= 1500;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"4", 
						"tasa"=>"E", 
						"importe"=>($precio*$cant), 
						"impuesto"=>0
					);
					$impuesto= 0;
					$indicadorMontoGrav="";
					$otros= "0,4,1,0,0,0,0,0,0,0,0,0";
				}
				else if( !strcmp($tipoDoc, "33") ) {
					$formaPago=1;
					$cant= 1500;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"4", 
						"tasa"=>"E", 
						"importe"=>($precio*$cant), 
						"impuesto"=>0
					);
					$impuesto= 0;
					$indicadorMontoGrav="";
					$otros= "0,4,1,0,0,0,0,0,0,0,0,0";
					$contenedor=8019289;
					$ref=1447;
					$ajustedoc=array(
						"ncf"=>$doneFact[count($doneFact)-1]["serie"].$doneFact[count($doneFact)-1]["tipo_doc"].setCeros($doneFact[count($doneFact)-1]["folio"], 10), 
						"fecha"=>$doneFact[count($doneFact)-1]["fecha"], 
						"codigo"=>3
					);
				}
				else if( !strcmp($tipoDoc, "34") ) { // NC
					$precio=0;
					$formaPago=0;
					$cant= rand(1,6);
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"1", 
						"tasa"=>$itbis, 
						"importe"=>($precio*$cant), 
						"impuesto"=>(($precio*$cant)*($itbis/100))
					);
					$impuesto= (($precio*$cant)*($itbis/100));
					$indicadorMontoGrav=0;
					$otros= "0,1,1,0,0,0,0,0,0,0,0,0";
					$ajustedoc=array(
						"ncf"=>$doneFact[0]["serie"].$doneFact[0]["tipo_doc"].setCeros($doneFact[0]["folio"], 10), 
						"fecha"=>$doneFact[0]["fecha"], 
						"codigo"=>2, 
						"razon"=>"Error en datos"
					);
				}
				else if( !strcmp($tipoDoc, "41") ) { // NC
					$formaPago=1;
					$cant= rand(1,6);
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"1", 
						"tasa"=>$itbis, 
						"importe"=>($precio*$cant), 
						"impuesto"=>(($precio*$cant)*($itbis/100))
					);
					$impuesto= (($precio*$cant)*($itbis/100));
					$indicadorMontoGrav=0;
					$agenteRete=urlencode('{"agente":"1","itbis":"'.$impuesto.'","isr":"'.($precio*$cant).'"}');
					$otros= "0,1,1,0,0,0,0,0,0,0,0,".$agenteRete;
					$retIva=$impuesto;
					$retIsr=($precio*$cant);
				}
				else if( !strcmp($tipoDoc, "43") ) {
					$formaPago=1;
					$cant= 1500;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"4", 
						"tasa"=>"E", 
						"importe"=>($precio*$cant), 
						"impuesto"=>0
					);
					$impuesto= 0;
					$indicadorMontoGrav="";
					$otros= "0,4,1,0,0,0,0,0,0,0,0,0";
				}
				else if( !strcmp($tipoDoc, "44") ) {
					$formaPago=1;
					$cant= rand(1,6);
					$precio=200000;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"4", 
						"tasa"=>"E", 
						"importe"=>($precio*$cant), 
						"impuesto"=>0
					);
					$impuesto= 0;
					$indicadorMontoGrav="";
					$otros= "0,4,1,0,0,0,0,0,0,0,0,0";
				}
				else if( !strcmp($tipoDoc, "45") ) { 
					echo "\n[*] Modelo 45 detectado..";
					echo "\n[*] Cambiando Receptor: Antes: ". $idCliente;
					unset($idCliente);
					$idCliente= $idClienteGob; // colocamos cliente extranjero
					echo "\n[*] Cambiando Receptor Ahora: ". $idCliente;
					$formaPago=1;
					$cant= rand(1,6);
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"1", 
						"tasa"=>$itbis, 
						"importe"=>($precio*$cant), 
						"impuesto"=>(($precio*$cant)*($itbis/100))
					);
					$impuesto= (($precio*$cant)*($itbis/100));
					$indicadorMontoGrav=0;
					$otros= "0,1,1,0,0,0,0,0,0,0,0,0";
				}	
				else if( !strcmp($tipoDoc, "46") ) { 
					$formaPago=1;
					$cant= rand(1,6);
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"3", 
						"tasa"=>$itbis3, 
						"importe"=>($precio*$cant), 
						"impuesto"=>(($precio*$cant)*($itbis3/100))
					);
					$impuesto= (($precio*$cant)*($itbis3/100));
					$indicadorMontoGrav="";
					$otros= "0,3,1,0,0,0,0,0,0,0,0,0";
				}
				else if( !strcmp($tipoDoc, "47") ) {
					echo "\n[*] Modelo 47 detectado..";
					echo "\n[*] Cambiando Receptor: Antes: ". $idCliente;
					unset($idCliente);
					$idCliente= $idClienteExtr; // colocamos cliente extranjero
					echo "\n[*] Cambiando Receptor Ahora: ". $idCliente;
					$cant= rand(1,6);
					$formaPago=1;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"4", 
						"tasa"=>"E", 
						"importe"=>($precio*$cant), 
						"impuesto"=>0
					);
					$impuesto= 0;
					$indicadorMontoGrav="";
					$agenteRete=urlencode('{"agente":"1","isr":"'.($precio*$cant).'"}');
					$otros= "0,4,1,0,0,0,0,0,0,0,0,".$agenteRete;
					$retIsr=($precio*$cant);
				}
				else { // 31
					$formaPago=1;
					$cant= 1;
					$precio=700;
					$extrasImp[]= array(
						"nombre"=>"ITBIS", 
						"tipo"=>"1", 
						"tasa"=>$itbis, 
						"importe"=>($precio*$cant), 
						"impuesto"=>(($precio*$cant)*($itbis/100))
					);
					$impuesto= (($precio*$cant)*($itbis/100));
					$indicadorMontoGrav=0;
					$otros= "0,1,1,0,0,0,0,0,0,0,0,0";
				}
				$cps= 0;
				$ni= 'A-1234';

				#
				# impuestos locales
				#
				$impLocales=array();
				$tmpImpLoc= array(); # temporal
				foreach( $extrasImp as $key=>$val ) {
					if( !count($tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]) ) # inicializamos
						$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]=array( "tasa"=>$val["tasa"], "importe"=>0, "impuesto"=>0, "clave"=>$val["tipo"] );

					$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["importe"] += $val["importe"];
					$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["impuesto"] += $val["impuesto"];
				}
				if( count($tmpImpLoc) ) {
					foreach( $tmpImpLoc as $key=>$val ) {
						$impLocales[]= $val; # agregamos
					}
				}

				$fechaEntrega= strtotime(date("Y", time())."-".(date("m", time())+1)."-".date("d", time())."T00:00:00");

				#
				# conceptos
				#
				$conceptos[]= array(
					"cantidad"=>$cant, # cantidad de unidades
					"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
					"concepto"=>substr($titulo, 0, 50), 
					"pu"=>$precio,  # precio unitario, NO USAR COMAS ","
					"ni"=>$ni, # numero de identificacion del producto, puede indicar 0 si no desea usarlo
					"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
					"cps"=>$cps, # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
					"impuesto"=>$impuesto, # 1=SiCalcular, 0=NoCancular
					"importe"=>($precio*$cant), 
					"extra_impuestos"=>$extrasImp, 
					"cps"=>$cps, 
					"otros"=>$otros
				);


				$subtotal=0;
				$impuestos=0;
				foreach( $conceptos as $key=>$val ) {
					$subtotal += $val["importe"];
					$impuestos += $val["impuesto"];
				}
				$total= ($impuestos+$subtotal); # sumamos subtotal e impuestos

				// extras
				$extras="[indicadormontogravado|".$indicadorMontoGrav."][numerofacturainterna|0][zonaventa|0][fechaentrega|".$fechaEntrega."][fechaordencompra|][numerocontenedor|". $contenedor."][numeroreferencia|".$ref."][montoexento|0][montoexentootramoneda|0][valorpagar|0][indicadornc|". $indicadorNc."][montopago|".$total."][descuentosorecargos|0][ajustedoc|".urlencode(json_encode($ajustedoc))."][datosbancarios|0][comercializadora|0][adicionaldata|0]";

				$data= array(
					"folio"=>array(
						"serie"=>"E", 
						"folio"=>($startFolio+$cont)
					),
					"tipo"=>"factura", # factura, credito o debito
					"id_cliente"=>$idCliente, # identificador del cliente
					"id_firma"=>$idFirma, # identificador de la firma electronica
					"id_formula"=>$idFormula, # identificador de la formula
					"formato"=>"normal",
					"metodo_pago"=>1, 
					"forma_pago"=>$formaPago, 
					"moneda"=>"DOP", # 1=PesosMexicanos, verificar la ayuda
					"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
					"impuestos_locales"=>$impLocales, 
					"conceptos"=>$conceptos, 
					"predial"=>0, # Predial del arrendador
					"subtotal"=>number_format($subtotal, 2, '.', ''), 
					"impuestos"=>$impuestos, 
					"total"=>number_format($total, 2, '.', ''), 
					"descuentos"=>0, # indica como se procesaran los descuentos de los conceptos en caso que existan: 1=despues de impuestos, 2=antes de impuestos
					"ret_iva"=>$retIva, 
					"ret_isr"=>$retIsr, 
					"auto_calc"=>0, 
					"tipo_operacion"=>1, 
					"tipo_factura"=>$tipoDoc, 
					"fecha_emision"=>date("d-m-Y", time()), 
					"fecha_vencimiento"=>(date("Y", time())+1)."-12-31T00:00:00", 
					"autofolio"=>2,
					"autogenfolios"=>2,
					"ambiente"=>2, 
					"extras"=>$extras, 
					"mailsend"=>0
				);

				// echo "\n\n";
				// print_r($data);
				// echo "\n\n";

				echo "\n[*] [".$i."/".$totalDocs."] [Tipo: ".$tipoDoc."] Emitiendo: E-". ($startFolio+$cont). "\t";

				$mbox= new moneyBox($user, $pass, $path, $data);
				if( $mbox->getError() ) {
					$r= $mbox->getRespuesta();

					if( !strcmp($r->error_code, "92") ) {
						echo "\n[Pend-Script] -- EN PROCESO [ID: ".$r->result->id."]...";
				        echo "\nPulsa ENTER para continuar....";
				        $rl= readline();
					}
					else {
						$errorLog=true;
						echo "\n[Error-Script] -- ERROR!";
					 	echo "\n[Error] ". $mbox->getError();
						echo "\nErrorDetails:\n";
						print_r($mbox->getErrorDetails());
						echo "\n";
					}
				}
				else {
					$r= $mbox->getRespuesta();

					if( isset($r->result->timbre_fiscal) && isset($r->result->sello) ) { # no hay resultado, peor si se genero transaccion
						echo "\n[Done-Script]-- OK!";
						$doneFact[]= array(
							"id"=>$r->result->id, 
							"uuid"=>$r->result->timbre_fiscal, 
							"serie"=>$r->result->serie, 
							"folio"=>$r->result->folio, 
							"fecha"=>$r->result->fecha, 
							"tipo_doc"=>$r->result->tipo_factura,
							"url_xml"=>$r->result->url_xml,
							"url_pdf"=>$r->result->url_pdf
						);
					}
					else {
						$errorLog=true;
						echo "\n[Error-Script] -- ERROR-2!";
					}
				}

		        // echo "\nPulsa ENTER para continuar....";
		        // $rl= readline();
				unset($conceptos,$cant,$titulo,$precio,$ni,$impuesto,$extrasImp,$cps,$otros,$subtotal,$impuestos,$total,$data, $mbox);
			}
		}

		if( count($doneFact) ) {
			echo "\n\n==== ENVIOS ===\n";
			print_r($doneFact);
			echo "\n==== FIN-ENVIOS ===\n";

			$pathZip= 'repdom_pdfs/';
			$tm= time();
			$fileZip= 'repdom_pdfs.zip';
			$fileTxt= 'repdom_pdfs.txt';
			if( !is_dir($pathZip) ) { // no exite
				mkdir($pathZip); // creamos
			}

			// creamos detalle o log de los PDFs vs Folios
			echo "\n[*] Creando archivo ".$pathZip.$fileTxt."\t";
			$fpTxt= fopen($pathZip.$fileTxt, "w");
			fwrite($fpTxt, print_r($doneFact, true));
			fclose($fpTxt);
			unset($fpTxt);
			if( file_exists($pathZip.$fileTxt) ) {
				echo "OK";
			}
			else {
				echo "ERROR!!!!";
			}

			foreach( $doneFact as $kZip=>$vZip ) {
				$data= file_get_contents($vZip["url_pdf"]);
				file_put_contents($pathZip.$vZip["serie"].'_'.$vZip["folio"].".pdf", $data);
				unset($data);
			}

			echo "\n[*] Creando archivo ".$fileZip."\t";
			$zip= new ZipArchive();
			$zip->open($fileZip,  ZipArchive::CREATE);
			$files= scandir($pathZip);
			unset($files[0],$files[1]);

			foreach ($files as $file) {
			    $zip->addFile($pathZip.$file, $file);    
			}
			$zip->close();

			if( file_exists($fileZip) ) {
				echo "OK";
			}
			else {
				echo "ERROR!!!!";
			}
		}

		fclose($fp);
		unset($fp);
	}
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>
