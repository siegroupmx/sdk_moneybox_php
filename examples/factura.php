#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'democo';
$pass= 'gratis123';

/**
* ayuda
*/
#$path= 'clientes/help';
#$data= array(); # vacio

/**
* factura - lista
*/
#$path= 'factura/list';
#$data= array(); # vacio

/**
* factura - buscar
*/
#$path= 'factura/get';
#$data= array( "q"=>"jz8vgif81b" ); # debe ser el Identificador de: Factura, Cliente o Folio

/**
* factura - generar nueva
*/
#$path= 'factura/save';
#$conceptos= array(
#	0=>array(
#		"cantidad"=>1, # cantidad de unidades
#		"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
#		"concepto"=>urlencode("Teclado Gamer"), 
#		"pu"=>"123879.85",  # precio unitario, NO USAR COMAS ","
#		"ni"=>urlencode("KBR-4567"), # numero de identificacion del producto, puede indicar 0 si no desea usarlo
#		"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
#		"cps"=>"ifyn4sh9", # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
#		"impuesto"=>1, # 1=SiCalcular, 0=NoCancular
#		), 
#	1=>array(
#		"cantidad"=>1, # cantidad de unidades
#		"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
#		"concepto"=>urlencode("Pantalla para Computadora"), 
#		"pu"=>"1238790.85",  # precio unitario, NO USAR COMAS ","
#		"ni"=>urlencode("SNY-50223"), # numero de identificacion del producto, puede indicar 0 si no desea usarlo
#		"desc"=>0, # valor del Descuento - Puedes omitirlo y el sistema lo toma como 0 (cero)
#		"cps"=>"ifyn4sh9", # Clave de Producto o Servicio, puede indicar 0 si no desea usarlo
#		"impuesto"=>1, # 1=SiCalcular, 0=NoCancular
#		)
#	);
#$subtotal=0;
#$impuestos=0;
#$miTazaImpuesto= 0.16;
#foreach( $conceptos as $key=>$val ) {
#	$subtotal += ($val["cantidad"]*$val["pu"]); # vamos sumando para tomar el subtotal
#	$impuestos += (($val["impuesto"]==1) ? (($val["cantidad"]*$val["pu"])*$miTazaImpuesto):0); # sacamos impuestos individuales
#}
#$total= ($impuestos+$subtotal); # sumamos subtotal e impuestos
#$data= array(
#	"tipo"=>"factura", # factura, credito o debito
#	"id_cliente"=>"3y2r3wby", # identificador del cliente
#	"id_firma"=>"yl5m", # identificador de la firma electronica
#	"id_formula"=>"qkckyuyfbo", # identificador de la formula
#	"formato"=>"normal", # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
#	"metodo_pago"=>"10", # 10=Efectivo 
#	# "metodo_pago_digitos"=>"1234", # aplicable solo para pagos distintos a Efectivo y NoIdentificado, debe establecerse los digitos de cuenta emisor, cheque o referencia bancaria
#	"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
#	"moneda"=>1, # 1=PesosMexicanos, verificar la ayuda
#	"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
#	"conceptos"=>$conceptos, 
#	"predial"=>0, # Predial del arrendador
#	"subtotal"=>$subtotal, 
#	"impuestos"=>$impuestos, 
#	"total"=>$total, 
#	"descuentos"=>1, # indica como se procesaran los descuentos de los conceptos en caso que existan: 1=despues de impuestos, 2=antes de impuestos
#	# "incoterm "=>"id_del_incoterm", # identificador, verificar la ayuda
#	# "aduana"=>"Numero_De_Aduana", # cadena que indique el Numero de la Aduana
#	# "oc"=>"OdenDecompra", # valor textual deseado
#	# "poliza"=>"NumeroDePoliza", # valor textual deseado
#	# "referencia"=>"DatoDeReferencia", # valor textual deseado
#	# "condicionpago"=>"IndicacionDeLaCondicionDePago", # valor textual deseado
#	# "tiporelacion"=>"id_tipo_relacion_documento", # identificardor del tipo de relacion del documento, verificar ayuda
#	# "uso"=>"identificador_uso", # identificador para el tipo de Uso del documento, verificar la ayuda
#	); # vacio

/**
* factura - regenerar factura
*/
#$path= 'factura/regen';
#$data= array( "id"=>"2tsd" ); # debe ser el Identificador de: Factura

/**
* Generar Nota de Credito
*/
#$path= 'factura/list';
#$data= array();
#$error=array();
#$exito=array();

#$mbox= new moneyBox($user, $pass, $path, $data);
#if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
#else {
#	$r= $mbox->getRespuesta();
#	$debito=array();
#
#	foreach( $r->result as $key=>$val ) {
#		if( isset($val->timbre_fiscal) && isset($val->id_cliente) && isset($val->id) )
#			$debito[]= array( "id"=>$val->id, "timbre_fiscal"=>$val->timbre_fiscal);
#	}
#
#	echo "\nIniciando Proceso...\n\n";
#	$i=0;
#	foreach( $debito as $key=>$val ) {
#		if( $i==0 ) {
#			echo "\nEnviando Nota de Credito UUID/CUFE: ". $val["timbre_fiscal"]. " ---> ";
#			$path= 'factura/save';
#			$data= array(
#				"id"=>array(0=>$val["id"]), # agrega o relaciona en la nota de credito de 1 a muchas facturas
#				"tipo"=>"credito", # factura, credito o debito
#				"id_firma"=>"yl5m", # identificador de la firma electronica
#				"id_formula"=>"qkckyuyfbo", # identificador de la formula
#				"tipo_relacion"=>"3", # 3=devolucion de mercancias, verificar la ayuda
#				"metodo_pago"=>"10", # 10=Efectivo 
#				"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
#				"moneda"=>5, # 1=PesosMexicanos, verificar la ayuda
#				"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
#				);
#
#			$mbox= new moneyBox($user, $pass, $path, $data);
#			if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
#			else {
#				$r= $mbox->getRespuesta();
#
#				if( isset($r->result->timbre_fiscal) )	$exito[$r->result->id]= $r->result->timbre_fiscal;
#				else 	$error[$r->result->id]= 0;
#
#				echo (isset($r->result->timbre_fiscal) ? "OK":"ERROR..");
#			}
#		}
#		$i++;
#	}
#}

/**
* Regenerar Nota de Credito
*/
#$path= 'factura/regen';
#$data= array( "id"=>"96ze", "tipo"=>"credito" );
#$mbox= new moneyBox($user, $pass, $path, $data);

/**
* Listar Notas de Credito
*/
#$path= 'factura/list';
#$data= array("tipo"=>"credito"); # vacio


$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
?>
