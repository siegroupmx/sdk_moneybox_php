#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'democo';
$pass= 'gratis123';
$fileZip= 'egreso.zip';

// Eventos RADIAN
// 030	Acuse de recibo de Factura Electrónica de Venta, emite R (receptor)
// 031	Reclamo de la Factura Electrónica de Venta, emite R
// 032	Recibo del bien o prestación del servicio, emite R
// 033	Aceptación expresa, emite R
// 034	Aceptación Tácita, emite E (emisor)

/**
* enviar estado a radian sobre un documento electronico
*/
$path= 'compras/save';
$data= array( 
	"access"=>"partner", 
	// "from"=>"data", 
	"from"=>"file", 
	"zip"=>base64_encode(file_get_contents($fileZip)), # desde un XML embebido en base64, usa from "file" 
	"emisor"=>array( # emisor y firmante de los eventos
		"nit"=>"900704341", 
		"mail"=>"siegroupmx@gmail.com"
	), 
	// las variables de aqui hacia abajo solo se usan para "from"=>"data"
	// "receptor"=>array(
	// 	"id"=>"", 
	//	"nombre"=>""
	//	"nit"=>"", 
	//	"dv"=>"", 
	// )
);

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
// else {
// 	echo "\nExito:";
// 	echo "\n\nData en Array:\n";
// 	print_r($mbox->getRespuesta());
// 	echo "\n\nData en JSON:\n";
// 	print_r($mbox->getRespuesta("json"));
// }

/* Headers */
echo "\n\nHeaders Request:\n";
print_r($mbox->getHeaderRequest());

echo "\n\nHeaders Response:\n";
print_r($mbox->getHeaderResponse());
echo "\n\n";
?>
