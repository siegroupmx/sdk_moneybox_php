#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'redco';
$pass= '62h6f3agk9';

$path= 'cuenta/firmas/list';
$data= array(
);
$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 
 	echo '[Error] '. $mbox->getError();
else {
	$r= $mbox->getRespuesta();
	$idFirma= $r->result[0]->id;
	echo "\n\nFirma: ". $idFirma. "\n";
}

$path= 'cuenta/formulas/list';
$data= array(
);
$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 
 	echo '[Error] '. $mbox->getError();
else {
	$r= $mbox->getRespuesta();
	$idFormula= $r->result[0]->id;
	echo "\n\nFormula: ". $idFormula. "\n";
}

$path= 'clientes/list';
$data= array(
);
$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 
 	echo '[Error] '. $mbox->getError();
else {
	$r= $mbox->getRespuesta();
	$idCliente=$r->result[0]->id;
	echo "\n\Clientes:". $idCliente. "\n";
}

/**
* Envio de 200 facturas para ambiente de pruebas
*/

$archivo= 'lista.txt'; # lista de articulos
$path= 'factura/save';
$error=array();
$exito=array();

if( !file_exists($archivo) )
	echo "\nNo existe la lista de productos..";
else {
	$fp= fopen($archivo, "r");
	$i=0;
	while(($buf=fgets($fp, (5*1024)))!==FALSE) {
		if( $i<1 ) {
			$cant= rand(1,6);
			$conceptos=array();
			$x= explode("\t", $buf);
			$precio= number_format( ($x[0]*20), 2, '.', '');
			#$titulo= mb_convert_encoding(html_entity_decode($x[1], ENT_QUOTES), "utf-8", "HTML_ENTITIES");
			$titulo= htmlentities($x[1], ENT_QUOTES);

			echo "\n[". ($i+1). "]". $titulo. " -- ". $precio;

			#
			# extra impuestos
			#
			$extrasImp=array();
			$extrasImp[]= array(
				"nombre"=>"IVA", 
				"tipo"=>"01", 
				"tasa"=>"19.00", 
				"importe"=>($precio*$cant), 
				"impuesto"=>(($precio*$cant)*(0.19))
			);
			$cps= array(
				"clave"=>"010", 
				"tipo"=>"UNSPSC", 
				"codigo"=>"1234567892"
			);
			$ni= '1234567892';

			#
			# impuestos locales
			#
			$impLocales=array();
			$tmpImpLoc= array(); # temporal
			foreach( $extrasImp as $key=>$val ) {
				if( !count($tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]) ) # inicializamos
					$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]=array( "tasa"=>$val["tasa"], "importe"=>0, "impuesto"=>0, "clave"=>$val["tipo"] );

				$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["importe"] += $val["importe"];
				$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["impuesto"] += $val["impuesto"];
			}
			if( count($tmpImpLoc) ) {
				foreach( $tmpImpLoc as $key=>$val ) {
					$impLocales[]= $val; # agregamos
				}
			}

			#
			# conceptos
			#
			$conceptos[]= array(
				"cantidad"=>$cant, # cantidad de unidades
				"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
				"concepto"=>urlencode($titulo), 
				"pu"=>$precio,  # precio unitario, NO USAR COMAS ","
				"ni"=>$ni, # numero de identificacion del producto, puede indicar 0 si no desea usarlo
				"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
				"cps"=>"ifyn4sh9", # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
				"impuesto"=>(($precio*$cant)*(0.19)), # 1=SiCalcular, 0=NoCancular
				"importe"=>($precio*$cant), 
				"extra_impuestos"=>$extrasImp, 
				"cps"=>$cps
			);

			$subtotal=0;
			$impuestos=0;
			$miTazaImpuesto= 0.19;
			foreach( $conceptos as $key=>$val ) {
				$subtotal += $val["importe"];
				$impuestos += $val["impuesto"];
			}
			$total= ($impuestos+$subtotal); # sumamos subtotal e impuestos

			$data= array(
				"tipo"=>"factura", # factura, credito o debito
				"id_cliente"=>$idCliente, # identificador del cliente
				"id_firma"=>$idFirma, # identificador de la firma electronica
				"id_formula"=>$idFormula, # identificador de la formula
				"formato"=>"normal", # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
				"metodo_pago"=>"10", # 10=Efectivo 
				"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
				"moneda"=>5, # 1=PesosMexicanos, verificar la ayuda
				"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
				"impuestos_locales"=>$impLocales, 
				"conceptos"=>$conceptos, 
				"predial"=>0, # Predial del arrendador
				"subtotal"=>$subtotal, 
				"impuestos"=>$impuestos, 
				"total"=>$total, 
				"descuentos"=>0, # indica como se procesaran los descuentos de los conceptos en caso que existan: 1=despues de impuestos, 2=antes de impuestos
				"auto_calc"=>0, 
				);

			echo "\n\n";
			print_r($data);
			echo "\n\n";

			$mbox= new moneyBox($user, $pass, $path, $data);
			if( $mbox->getError() ) 
			 	echo '[Error] '. $mbox->getError();
			else {
				$r= $mbox->getRespuesta();

				if( !isset($r->result->timbre_fiscal) && isset($r->result->id) ) { # no hay resultado, peor si se genero transaccion
					$error[$r->result->id]= 0;
				}

				echo " ==> ". ($r->error_code ? "ERROR..":((isset($r->result->timbre_fiscal) && $r->result->timbre_fiscal) ? "OK":"ERROR.."));
				echo "\n\nFull Response:\n";
				print_r($r);
			}

			unset($conceptos, $precio, $titulo);
		}
		$i++;
	}
}

/*
$path= 'factura/regenall';
$data= array( "tipo"=>"factura" );

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	$r= $mbox->getRespuesta();
	$i=0;

	foreach( $r->result as $key=>$val ) {
		echo "\n". ($i+1). ") Factura ". (isset($val->id) ? $val->id:"NOID"). " -- CUFE ". ((isset($val->timbre_fiscal) && $val->timbre_fiscal) ? "OK -- ". $val->timbre_fiscal:"ERROR [". $val->validacion. "]");
		$i++;
	}
}
*/

/**
* Envio de 200 facturas de Credito, apartir de las 200 existentes...
*/

$path= 'factura/list';
$data= array();
$error=array();
$exito=array();

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	$r= $mbox->getRespuesta();
	$debito=array();

	foreach( $r->result as $key=>$val ) {
		if( $val->timbre_fiscal && isset($val->id_cliente) && isset($val->id) ) {
			$debito[]= array( 
				"id"=>$val->id, "timbre_fiscal"=>$val->timbre_fiscal, 
				"subtotal"=>$val->subtotal, 
				"impuesto"=>$val->impuesto, 
				"total"=>$val->total
			);
		}
	}

	echo "\n#### Enviando Notas de Credito, iniciando proceso...\n\n";
	$i=0;

	if( !count($debito) )
		echo "\n.. No se encontraron notas de credito...\n\n";
	else {
		foreach( $debito as $key=>$val ) {
			if($i<1 && $val["timbre_fiscal"] ) {
				echo "\n";
				echo "\nDebito....\n";
				print_r($val);
				echo "\n\n";

				echo "\n[". ($i+1). "] Enviando UUID/CUFE: ". $val["timbre_fiscal"]. " ---> ";
				$path= 'factura/save';
				$titulo= 'Devolucion Factura CUFE: '. $val["timbre_fiscal"];

				#
				# extra impuestos
				#
				$extrasImp=array();
				$extrasImp[]= array(
					"nombre"=>"IVA", 
					"tipo"=>"01", 
					"tasa"=>"19.00", 
					"importe"=>$val["subtotal"], 
					"impuesto"=>$val["impuesto"]
				);
				$cps= array(
					"clave"=>"010", 
					"tipo"=>"UNSPSC", 
					"codigo"=>"1234567892"
				);
				$ni= '1234567892';

				#
				# impuestos locales
				#
				$impLocales=array();
				$tmpImpLoc= array(); # temporal
				foreach( $extrasImp as $key2=>$val2 ) {
					if( !count($tmpImpLoc[$val2["tipo"]. '/'. $val2["tasa"]]) ) # inicializamos
						$tmpImpLoc[$val2["tipo"]. '/'. $val2["tasa"]]=array( "tasa"=>$val2["tasa"], "importe"=>0, "impuesto"=>0, "clave"=>$val2["tipo"] );

					$tmpImpLoc[$val2["tipo"]. '/'. $val2["tasa"]]["importe"] += $val2["importe"];
					$tmpImpLoc[$val2["tipo"]. '/'. $val2["tasa"]]["impuesto"] += $val2["impuesto"];
				}
				if( count($tmpImpLoc) ) {
					foreach( $tmpImpLoc as $key2=>$val2 ) {
						$impLocales[]= $val2; # agregamos
					}
				}

				$conceptos[]= array(
					"cantidad"=>1, # cantidad de unidades
					"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
					"concepto"=>urlencode($titulo), 
					"pu"=>$val["subtotal"],  # precio unitario, NO USAR COMAS ","
					"ni"=>0, # numero de identificacion del producto, puede indicar 0 si no desea usarlo
					"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
					"cps"=>"ifyn4sh9", # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
					"impuesto"=>$val["impuesto"], # 1=SiCalcular, 0=NoCancular
					"tasa"=>19,
					"importe"=>$val["total"], 
					"extra_impuestos"=>$extrasImp, 
					"cps"=>$cps
				);
				$uuid= array(
					0=>array(
						"uuid"=>$val["timbre_fiscal"], 
						"total"=>$val["total"], 
						"impuesto"=>$val["impuesto"]
					)
				);
				$data= array(
					"tipo"=>"credito", # factura, credito o debito
					"id_firma"=>$idFirma, # identificador de la firma electronica
					"id_formula"=>$idFormula, # identificador de la formula
					"id_cliente"=>$idCliente, # identificador del cliente
					"tipo_relacion"=>"3", # 3=devolucion de mercancias, verificar la ayuda
					"metodo_pago"=>"10", # 10=Efectivo 
					"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
					"moneda"=>5, # 1=PesosMexicanos, verificar la ayuda
					"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
					"impuestos_locales"=>$impLocales, 
					"conceptos"=>$conceptos, 
					"subtotal"=>$val["subtotal"], 
					"impuestos"=>$val["impuesto"], 
					"auto_calc"=>0, 
					"total"=>$val["total"],
					"tipo_operacion"=>"22", 
					"uuid"=>$uuid # agrega o relaciona en la nota de credito de 1 a muchas facturas
					);

				print_r($data);

				$mbox= new moneyBox($user, $pass, $path, $data);
				if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
				else {
					$r= $mbox->getRespuesta();

					if( isset($r->error_code) ) { # si no hay error
						if( isset($r->result->timbre_fiscal) )	$exito[$r->result->id]= $r->result->timbre_fiscal;
						else 	$error[$r->result->id]= 0;
					}
					else {
						if( isset($r->error_code) ) {
							if( !strcmp($r->error_code, "254") ) # resguardado por DIAN, se debe reintentar requerir acuse de validacion
								$error[$r->result->id]= 0;
						}
					}

					echo (isset($r->error_code) ? "ERROR..":(isset($r->result->timbre_fiscal) ? "OK":"ERROR.."));
				}
			sleep(5);
			unset($titulo, $conceptos);
			}
			$i++;
		}
	}
	unset($debito);
}


/**
* Generar Nota de Debito
*/

$archivo= 'lista.txt'; # lista de articulos

if( !file_exists($archivo) )
	echo "\nNo existe la lista de productos..";
else {
	$path= 'factura/list';
	$data= array();

	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$debito=array();

		foreach( $r->result as $key=>$val ) {
			if( isset($val->timbre_fiscal) ) {
				if( $val->timbre_fiscal && $val->doctosrel_facturas==0 )
					$debito[]= array( "id"=>$val->id, "timbre_fiscal"=>$val->timbre_fiscal);
			}
		}
	}

	$path= 'factura/save';
	$error=array();
	$exito=array();

	$fp= fopen($archivo, "r");
	$i=0;
	while(($buf=fgets($fp, (5*1024)))!==FALSE) {
		if( $i<1 ) {
			$cant= rand(1,6);
			$conceptos=array();
			$x= explode("\t", $buf);
			$precio= number_format( ($x[0]*20), 2, '.', '');
			#$titulo= mb_convert_encoding(html_entity_decode($x[1], ENT_QUOTES), "utf-8", "HTML_ENTITIES");
			$titulo= htmlentities($x[1], ENT_QUOTES);

			$cps= array(
				"clave"=>"010", 
				"tipo"=>"UNSPSC", 
				"codigo"=>"1234567892"
			);
			$ni= '1234567892';

			#
			# extra impuestos
			#
			$extrasImp=array();
			$extrasImp[]= array(
				"nombre"=>"IVA", 
				"tipo"=>"01", 
				"tasa"=>"19.00", 
				"importe"=>($precio*$cant), 
				"impuesto"=>(($precio*$cant)*(0.19))
			);

			#
			# impuestos locales
			#
			$impLocales=array();
			$tmpImpLoc= array(); # temporal
			foreach( $extrasImp as $key=>$val ) {
				if( !count($tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]) ) # inicializamos
					$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]=array( "tasa"=>$val["tasa"], "importe"=>0, "impuesto"=>0, "clave"=>$val["tipo"] );

				$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["importe"] += $val["importe"];
				$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["impuesto"] += $val["impuesto"];
			}
			if( count($tmpImpLoc) ) {
				foreach( $tmpImpLoc as $key=>$val ) {
					$impLocales[]= $val; # agregamos
				}
			}

			echo "\n[". ($i+1). "]". $titulo. " -- ". $precio;

			$conceptos[]= array(
				"cantidad"=>$cant, # cantidad de unidades
				"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
				"concepto"=>urlencode($titulo), 
				"pu"=>$precio,  # precio unitario, NO USAR COMAS ","
				"ni"=>$ni, # numero de identificacion del producto, puede indicar 0 si no desea usarlo
				"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
				"cps"=>$cps, # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
				"extra_impuestos"=>$extrasImp, 
				"impuesto"=>(($precio*$cant)*(0.19)),  
				"importe"=>($precio*$cant)
			);

			$subtotal=0;
			$impuestos=0;
			$miTazaImpuesto= 0.19;
			foreach( $conceptos as $key=>$val ) {
				$subtotal += $val["importe"];
				$impuestos += $val["impuesto"];
			}
			$total= ($impuestos+$subtotal); # sumamos subtotal e impuestos

			$data= array(
				"id"=>$debito[$i]["id"], 
				"tipo"=>"debito", # factura, credito o debito
				"id_cliente"=>$idCliente, # identificador del cliente
				"id_firma"=>$idFirma, # identificador de la firma electronica
				"id_formula"=>$idFormula, # identificador de la formula
				"formato"=>"normal", # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
				"metodo_pago"=>"10", # 10=Efectivo 
				"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
				"moneda"=>5, # 1=PesosMexicanos, verificar la ayuda
				"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
				"conceptos"=>$conceptos, 
				"predial"=>0, # Predial del arrendador
				"subtotal"=>$subtotal, 
				"impuestos"=>$impuestos, 
				"impuestos_locales"=>$impLocales, 
				"auto_calc"=>0,
				"total"=>$total, 
				"descuentos"=>1, # indica como se procesaran los descuentos de los conceptos en caso que existan: 1=despues de impuestos, 2=antes de impuestos
				);

			$mbox= new moneyBox($user, $pass, $path, $data);
			if( $mbox->getError() ) 
			 	echo '[Error] '. $mbox->getError();
			else {
				$r= $mbox->getRespuesta();

				if( !isset($r->result->timbre_fiscal) && isset($r->result->id) ) { # no hay resultado, peor si se genero transaccion
					$error[$r->result->id]= 0;
				}

				echo " ==> ". ($r->error_code ? "ERROR..":((isset($r->result->timbre_fiscal) && $r->result->timbre_fiscal) ? "OK":"ERROR.."));
				echo "\n\nFull Response:\n";
				print_r($r);

	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());
	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
			}

			unset($conceptos, $precio, $titulo);
		}
		$i++;
	}
}


echo "\n\nFin del programa...\n\n";
?>
