#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'democo';
$pass= 'gratis123';

/**
* ayuda
*/
#$path= 'clientes/help';
#$data= array(); # vacio

// $path= 'cuenta/help/ciudades';
// $data= array(); # vacio

/**
* status servers
*/
// $entidadName= 'mh_send'; // mh_send, mh_consulta, mh_contingencia, mh_cancel
// // $entidadName= 'mh_send_sandbox'; // mh_send_sandbox, mh_consulta_sandbox, mh_contingencia_sandbox, mh_cancel_sandbox
// $path= 'cuenta/status/server/'.$entidadName;
// $data= array(); # vacio

/**
* lista de clientes
*/
$path= 'clientes/list';
$data= array(); # vacio

/**
* agregar cliente
*/
#$path= 'clientes/add';
#$data= array(
#	"empresa"=>"AGROINDUSTRIA PRUEBAS",
#	"nombre"=>"AGROINDUSTRIA PRUEBAS", 
#	"nit"=>"700123123-2", # identificacion tributaria para colombia
#	"rfc"=>NULL, # identificacion tributaria para mexico
#	"calle"=>"CL 4", 
#	#"colonia"=>"Zipaquira", 
#	"cp"=>"80000", 
#	"num_ext"=>"13 Ap. 5", 
#	"ciudad"=>"11001", # 11001=Bogota, catalogo DIAN
#	"estado"=>"25", # 25=Cundinamarca, catalogo DIAN
#	"pais"=>"47", # 47=Colombia, consultar la ayuda
#	"email"=>"agroindustria@gmail.com", 
#	"regimen_fiscal"=>"1", # 1=Juridico, consultar la ayuda 
#	"telefono"=>"571 000 0000", 
#	"tipo_cliente"=>"yrlht9jj", # yrlht9jj=ClienteNacional, consultar ayuda
#	"perfil"=>"1", # 1=Simplificado, consultar ayuda
#	);

/**
* eliminar cliente
*/
#$path= 'clientes/del';
#$data= array( "id"=>"ujjq4v" );

/**
* actualizar cliente
*/
#$path= 'clientes/update';
#$data= array(
#	"id"=>"6rpubsy255", 
#	"calle"=>"CL 4", 
#	);

/**
* direcciones de embarque -- agregar
*/
#$path= 'clientes/direcciones/add';
#$data= array( 
#	"id"=>"8oy6l2", # id del Cliente
#	"calle"=>"la principal", 
#	"numext"=>"300 Local 5", 
#	"colonia"=>"la secundaria", 
#	"cp"=>"88790", 
#	"ciudad"=>"CiudadInventada",  # texto libre
#	"estado"=>"7", # 7=Coahuila, consultar ayuda 
#	"pais"=>"151", # 151=Mexico, consultar ayuda
#	"telefono"=>"899 000 0000", 
#	"email"=>"miotrocorreo@gmail.com", 
#	"contacto"=>"Peter Parker", 
#	"referencias"=>"Frente a la fuente de Agua Roja"
#	);

/**
* direcciones de embarque -- eliminar
*/
#$path= 'clientes/direcciones/del';
#$data= array( 
#	"id"=>"dz2et8mdyk", # id de la Direccion
#	"id_cliente"=>"8oy6l2", # id del Cliente
#	);

/**
* direcciones de embarque -- actualizar
*/
#$path= 'clientes/direcciones/update';
#$data= array( 
#	"id"=>"yvbmw5", # id de la Direccion Embarque
#	"id_cliente"=>"8oy6l2", # id del Cliente
#	"referencias"=>"Frente a un Restaurante azul"
#	);

/**
* busquedas - clientes con direcciones
*
* podemos buscar por: id, nombre, empresa o rfc/nit
*/
#$path= 'clientes/get';
#$data= array( 
#	"id"=>"yvbmw5"
#	"empresa"=>"publi"
#	"nombre"=>"nombre_de_la_empresa"
#	"rfc"=>"rfc_de_la_empresa"
#	"nit"=>"nit_de_la_empresa"
#	);

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	echo "\nExito:";
	echo "\n\nData en Array:\n";
	print_r($mbox->getRespuesta());
	// $r= $mbox->getRespuesta();
	// echo "\nEstado: ". $r->result->tipo;

	echo "\n\nData en JSON:\n";
	print_r($mbox->getRespuesta("json"));
}

/* Headers */
echo "\n\nHeaders Request:\n";
print_r($mbox->getHeaderRequest());

#echo "\n\nHeaders Response:\n";
#print_r($mbox->getHeaderResponse());
echo "\n\n";

$abc= $mbox->getRespuesta();
print_r($abc->result->ciudades);
?>
