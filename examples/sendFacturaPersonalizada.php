#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'marthappco';
$pass= 'ClaveSegura';
$fileJson= '009.json';

if( !file_exists($fileJson) ) {
	echo "\n[ERROR] El archivo ". $fileJson. " no existe..";
}
else {
	$json= json_decode(file_get_contents($fileJson), true);
	$path= 'cuenta/firmas/list';
	$data= array();
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] FIRM-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idFirma= $r->result[0]->id;
	}

	$path= 'cuenta/formulas/list';
	$data= array(
	);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] FORMULA-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idFormula= $r->result[0]->id;
	}

	$path= 'clientes/get';
	$data= array( "nit"=>$json["receptor"]["nit"]);
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) 
	 	echo '[Error] CLIENT-'. $mbox->getError();
	else {
		$r= $mbox->getRespuesta();
		$idCliente=$r->result[0]->id;
	}

	if( !$idFirma ) {
		echo "\n[ERROR] No se a detectado la firma electronica...";
	}
	else if( !$idFormula ) {
		echo "\n[ERROR] No se a detectado la Formula para calculo de impuestos...";
	}
	else if( !$idCliente ) {
		echo "\n[ERROR] No se a detectado el cliente receptor...";
	}
	else {
		$path= 'factura/save';

		$json["tipo"]= "factura"; # factura, credito o debito
		$json["formato"]= "normal"; # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
		$json["id_cliente"]= $idCliente; # identificador del cliente
		$json["id_firma"]= $idFirma; # identificador de la firma electronica
		$json["id_formula"]= $idFormula; # identificador de la formula
		// $json["tipo_factura"]= "01";

		$mbox= new moneyBox($user, $pass, $path, $json);

		echo "\n\nHeaders Response:\n";
		print_r($mbox->getHeaderResponse());
		echo "\n\n";

		if( $mbox->getError() ) {
			echo '[Error] FACT-'. $mbox->getError();
			echo "\nDetalles del error:\n";
			print_r($mbox->getErrorDetails());
		}
		else {
			/* Headers */
			echo "\n\nHeaders Request:\n";
			print_r($mbox->getHeaderRequest());

			echo "\n\nHeaders Response:\n";
			print_r($mbox->getHeaderResponse());
			echo "\n\n";
		}
	}
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>
