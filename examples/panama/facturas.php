#!/usr/bin/php
<?php
include( "../../src/cMoneyBox.php" );

$user= 'demopa';
$pass= 'gratis123';

/**
* factura - lista
*/
// $path= 'factura/list';
// $data= array(); # vacio

/**
* factura - buscar
*/
#$path= 'factura/get';
#$data= array( "q"=>"jz8vgif81b" ); # debe ser el Identificador de: Factura, Cliente o Folio

/**
* factura - generar nueva
*/
$path= 'factura/save';
$conceptos= array(
	0=>array(
		"cantidad"=>5, # cantidad de unidades
		"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
		"concepto"=>urlencode("entrada de cine"), 
		"pu"=>200,  # precio unitario, NO USAR COMAS ","
		"ni"=>"C-1", # numero de identificacion del producto, puede indicar 0 si no desea usarlo
		"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
		"cps"=>"4110", # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
		"impuesto"=>1, # 1=SiCalcular, 0=NoCancular
		"extra_impuestos"=>array()
	), 
	// 0=>array(
	// 	"cantidad"=>2, # cantidad de unidades
	// 	"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
	// 	"concepto"=>urlencode("Termo de agua"), 
	// 	"pu"=>"90.00",  # precio unitario, NO USAR COMAS ","
	// 	"ni"=>urlencode("TR-290"), # numero de identificacion del producto, puede indicar 0 si no desea usarlo
	// 	"desc"=>0, # valor del Descuento - puede indicar 0 si no desea usarlo
	// 	"cps"=>"4110", # Clave de Producto o Servici, puede indicar 0 si no desea usarlo 
	// 	"impuesto"=>1, # 1=SiCalcular, 0=NoCancular
	// 	"extra_impuestos"=>array()
	// ), 
	// 1=>array(
	// 	"cantidad"=>2, # cantidad de unidades
	// 	"unidad"=>1, # unidades de medida, 1=NoAplica, verificar la ayuda 
	// 	"concepto"=>urlencode("Esfera del Dragon"), 
	// 	"pu"=>"20.00",  # precio unitario, NO USAR COMAS ","
	// 	"ni"=>urlencode("DBZ-123"), # numero de identificacion del producto, puede indicar 0 si no desea usarlo
	// 	"desc"=>0, # valor del Descuento - Puedes omitirlo y el sistema lo toma como 0 (cero)
	// 	"cps"=>"4110", # Clave de Producto o Servicio, puede indicar 0 si no desea usarlo
	// 	"impuesto"=>1, # 1=SiCalcular, 0=NoCancular
	// 	"extra_impuestos"=>array()
	// )
);
$impuestos=0;
$total=0;
$subtotal=0;
$miTazaImpuesto= 0.07;
$impLocales=array();
$impTotales=0;
$extra_impuestos=array();
foreach( $conceptos as $key=>$val ) {
	$importe=0;
	$impuestos=0;
	$importe += ($val["cantidad"]*$val["pu"]); # vamos sumando para tomar el importe
	$impuestos += (($val["impuesto"]==1) ? ($importe*$miTazaImpuesto):0); # sacamos impuestos individuales
	$total += ($impuestos+$importe); # sumamos subtotal e impuestos
	$subtotal += $importe;

	$extrasImp=array();
	$extrasImp[]= array(
		"nombre"=>"ITBMS", 
		"tipo"=>"01", 
		"tasa"=>"7.00", 
		"importe"=>($importe), 
		"impuesto"=>number_format((($importe)*(0.07)), 2, '.', '')
	);
	$conceptos[$key]["extra_impuestos"]= $extrasImp;
	$conceptos[$key]["importe"]= $importe;

	$tmpImpLoc= array(); # temporal
	foreach( $extrasImp as $key=>$val ) {
		if( !count($tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]) ) # inicializamos
			$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]=array( "tasa"=>$val["tasa"], "importe"=>0, "impuesto"=>0, "clave"=>$val["tipo"] );

		$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["importe"] += $val["importe"];
		$tmpImpLoc[$val["tipo"]. '/'. $val["tasa"]]["impuesto"] += $val["impuesto"];
	}
	if( count($tmpImpLoc) ) {
		foreach( $tmpImpLoc as $key=>$val )
			$impLocales[]= $val; # agregamos
	}

	$impTotales += $impuestos;
}

$data= array(
	"folio"=>array(
		"serie"=>"", 
		"folio"=>"352216568"
	), 
	"tipo"=>"factura", # factura, credito o debito
	"id_cliente"=>"5383", # identificador del cliente
	"id_firma"=>"yl5m", # identificador de la firma electronica
	"id_formula"=>"jvrhqe", # identificador de la formula
	"formato"=>"normal", # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
	"metodo_pago"=>"01", # 01:credito, 02:efectivo, 03:T.Cred, 04:T.Debit, 08:transferencia, 09:cheque
	# "metodo_pago_digitos"=>"1234", # aplicable solo para pagos distintos a Efectivo y NoIdentificado, debe establecerse los digitos de cuenta emisor, cheque o referencia bancaria
	"forma_pago"=>"1", # 1=contado, 2=credito, 3=mixto
	"moneda"=>1, # 1=PesosMexicanos, verificar la ayuda
	"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
	"conceptos"=>$conceptos, 
	"predial"=>0, # Predial del arrendador
	"subtotal"=>$subtotal, 
	"impuestos"=>number_format($impTotales, 2, '.', ''), 
	"impuestos_locales"=>$impLocales, 
	"total"=>$total, 
	"descuentos"=>0, # indica como se procesaran los descuentos de los conceptos en caso que existan: 1=despues de impuestos, 2=antes de impuestos
	"fecha_vencimiento"=>"2022-10-21", 
	"mailsend"=>0, # 0= no mandar correo, 1=mandar correo al receptor de la factura
	"tipo_operacion"=>"11", # 01=venta, 11=devolucion 
	# "oc"=>"OdenDecompra", # valor textual deseado
	# "poliza"=>"NumeroDePoliza", # valor textual deseado
	# "referencia"=>"DatoDeReferencia", # valor textual deseado
	# "condicionpago"=>"IndicacionDeLaCondicionDePago", # valor textual deseado
); # vacio

/**
* factura - regenerar factura
*/
#$path= 'factura/regen';
#$data= array( "id"=>"2tsd" ); # debe ser el Identificador de: Factura

/**
* Generar Nota de Credito
*/
#$path= 'factura/list';
#$data= array();
#$error=array();
#$exito=array();

#$mbox= new moneyBox($user, $pass, $path, $data);
#if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
#else {
#	$r= $mbox->getRespuesta();
#	$debito=array();
#
#	foreach( $r->result as $key=>$val ) {
#		if( isset($val->timbre_fiscal) && isset($val->id_cliente) && isset($val->id) )
#			$debito[]= array( "id"=>$val->id, "timbre_fiscal"=>$val->timbre_fiscal);
#	}
#
#	echo "\nIniciando Proceso...\n\n";
#	$i=0;
#	foreach( $debito as $key=>$val ) {
#		if( $i==0 ) {
#			echo "\nEnviando Nota de Credito UUID/CUFE: ". $val["timbre_fiscal"]. " ---> ";
#			$path= 'factura/save';
#			$data= array(
#				"id"=>array(0=>$val["id"]), # agrega o relaciona en la nota de credito de 1 a muchas facturas
#				"tipo"=>"credito", # factura, credito o debito
#				"id_firma"=>"yl5m", # identificador de la firma electronica
#				"id_formula"=>"qkckyuyfbo", # identificador de la formula
#				"tipo_relacion"=>"3", # 3=devolucion de mercancias, verificar la ayuda
#				"metodo_pago"=>"10", # 10=Efectivo 
#				"forma_pago"=>"PUE", # PUE=Pago Unica Exhibicion
#				"moneda"=>5, # 1=PesosMexicanos, verificar la ayuda
#				"moneda_vcambio"=>1, # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
#				);
#
#			$mbox= new moneyBox($user, $pass, $path, $data);
#			if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
#			else {
#				$r= $mbox->getRespuesta();
#
#				if( isset($r->result->timbre_fiscal) )	$exito[$r->result->id]= $r->result->timbre_fiscal;
#				else 	$error[$r->result->id]= 0;
#
#				echo (isset($r->result->timbre_fiscal) ? "OK":"ERROR..");
#			}
#		}
#		$i++;
#	}
#}

/**
* Regenerar Nota de Credito
*/
#$path= 'factura/regen';
#$data= array( "id"=>"96ze", "tipo"=>"credito" );
#$mbox= new moneyBox($user, $pass, $path, $data);

/**
* Listar Notas de Credito
*/
#$path= 'factura/list';
#$data= array("tipo"=>"credito"); # vacio


$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() )  {
	echo "\n[Error] ". $mbox->getError();
	echo "\n\n";

	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
else {
	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}

echo "\n\n";
exit(0);
?>
