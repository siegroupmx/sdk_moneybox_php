#!/usr/bin/php
<?php
include( "../../src/cMoneyBox.php" );

$user= 'demopa';
$pass= 'gratis123';

/**
* consultando informacion del perfil
*/
// $path= 'cuenta/info';
// $data= array(); # vacio

/**
* consultar estado de la cuenta
*/
// $path= 'cuenta/status';
// $data= array(); # vacio

/**
* consultar modulos de servicio activos
*/
#$path= 'cuenta/modulos';
#$data= array(); # vacio

/**
* actualizar informacion
*/
#$path= 'cuenta/update';
#$data= array(
#		"nombre"=>"nuevo nombre SAS", 
#		"nit"=>"123456789-2", 
#		"telefono"=>"000 000 0000", 
#		"empresa"=>"nuevo nombre SAS", 
#		"email"=>"nuevocorreo@gmail.com", 
#		"ciudad"=>"id_de_la_ciudad", 
#		"estado"=>"id_del_estado", 
#		"colonia"=>"nombre de la colonia", 
#		"calle"=>"nombre de la calle", 
#		"num_ext"=>"numero exterior", 
#		"cp"=>"123456"
#		"pais"=>"id_del_pais", 
#		"regimen_fiscal"=>"id_del_regimen_fiscal", 
#		"imagen"=>"base64_de_la_imagen", 
#		"web"=>"https://www.miempresa.com", 
#		"slogan"=>"nombre comercial o para el publico", 
#		"moneda"=>"id_de_la_moneda_o_divisa_local"
#	);

/**
* webservice - agregar
*/
#$path= 'cuenta/webservice/add';
#$data= array(
#		"nombre"=>"700200857moneybox", 
#		"webservice_id"=>"d80990f3-e5e5-41d0-a632-aa45298ed0da", 
#		"webservice_pin"=>"Abc1234", 
#		"webservice_usuario"=>"ACANTUJ", 
#		"webservice_clave"=>"Abc2345678", 
#		"webservice_url"=>urlencode("https://facturaelectronica.dian.gov.co/operacion/B2BIntegrationEngine/FacturaElectronica/facturaElectronica.wsdl"), 
#		"webservice_url_pruebas"=>urlencode("https://facturaelectronica.dian.gov.co/habilitacion/B2BIntegrationEngine/FacturaElectronica/facturaElectronica.wsdl"), 
#		"estatus"=>2 # 1=produccion, 2=pruebas
#	);

/**
* webservice - listar
*/
#$path= 'cuenta/webservice/list';
#$data= array(); # vacio

/**
* webservice - eliminar
*/
#$path= 'cuenta/webservice/del';
#$data= array( "id"=>"hu36" ); # vacio

/**
* webservice - actualizar
*/
#$path= 'cuenta/webservice/update';
#$data= array( "id"=>"hu36", "nombre"=>"prueba123" ); # vacio

/**
* folios -- listar
*/
// $path= 'cuenta/folios/list';
// $data= array(); # vacio

/**
* folios -- crear
*/
// $path= 'cuenta/folios/add';
// $data= array(
// 	"folio_serie"=>"TPAN", 
// 	"folio_inicio"=>"1", 
// 	"folio_fin"=>"200", 
// 	"tipo"=>"1" # 1=factura, 2=nomina, 3=documentosoporte o retenciones, 4=eventos
// 	);

/**
* folios -- eliminar
*/
#$path= 'cuenta/folios/del';
#$data= array( "id"=>"2b3d" ); # vacio

/**
* formulas - listar
*/
// $path= 'cuenta/formulas/list';
// $data= array(); # vacio

/**
* formulas - agregar
*/
// $path= 'cuenta/formulas/add';
// $data= array(
// 	"nombre"=>"ITBMS 7%", 
// 	"operacion"=>1, # 1=suma,2=resta,3=multiplicacion,4=division
// 	"porcentaje"=>"7%"
// 	);

/**
* formulas - eliminar
*/
#$path= 'cuenta/formulas/del';
#$data= array("id"=>"cyfy3l");

/**
* formulas - actualizar
*/
#$path= 'cuenta/formulas/update';
#$data= array(
#	"id"=>"fd7q", 
#	"nombre"=>"IVA 16%", 
#	"operacion"=>1, # 1=suma,2=resta,3=multiplicacion,4=division
#	"porcentaje"=>"16%"
#	);

/**
* firmas - listar
*/
#$path= 'cuenta/firmas/list';
#$data= array();

/**
* firmas - agregar
*/
// $certificado= base64_encode(file_get_contents('certificados/certificado_kit.p12'));
// $path= 'cuenta/firmas/add';
// $data= array(
// 	"nombre"=>"MiFirma", 
// 	"clave"=>"A1s4r1l2n", 
// 	"key_file"=>base64_encode(file_get_contents($certificado)), 
// 	"cert_file"=>base64_encode(file_get_contents($certificado))
// 	);

/**
* firmas - eliminar
*/
#$path= 'cuenta/firmas/del';
#$data= array( "id"=>"ffvzz532t2" );


$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	echo "\nExito:";
	echo "\n\nData en Array:\n";
	print_r($mbox->getRespuesta());

	echo "\n\nData en JSON:\n";
	print_r($mbox->getRespuesta("json"));

	echo "\n\nEstados....\n";
	$abc= $mbox->getRespuesta();
	print_r($abc->result->estados);
	foreach( $abc->result->estados as $key=>$val ) {
		echo "\n[". $key. "] ". $val;
	}
	echo "\n";
}

/* Headers */
echo "\n\nHeaders Request:\n";
print_r($mbox->getHeaderRequest());

echo "\n\nHeaders Response:\n";
print_r($mbox->getHeaderResponse());
echo "\n\n";
?>
