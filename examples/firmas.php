#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'democo';
$pass= 'gratis123';

/**
* requerimiento de CSR
*/
/*
$path= 'cuenta/firmas/gen_csr';
$data= array(
	"pais"=>"CO", 
	#"estado"=>"Cundinamarca", 
	"departamento"=>"Cundinamarca", 
	"ciudad"=>"Zipaquira", 
	"nombre"=>"Mi Empresa SAS", 
	"mail"=>"micorreo@gmail.com", 
	"algo"=>"sha512", 
	"algo_bits"=>2048
);
*/

/**
* geenrar PFX
*/
$path= 'cuenta/firmas/gen_pfx';
$data= array(
	"nombre"=>"SYNERGIAN ESTRATEGIAS SOSTENIBLES SAS", 
	"pass"=>"Syn3r423Se", 
	"cert"=>base64_encode(file_get_contents("3152705484.crt")), 
	"firma"=>base64_encode(file_get_contents("firma.key")), 
	"extra_pri"=>base64_encode(file_get_contents("Sub001.cer")), 
	"extra_sec"=>base64_encode(file_get_contents("Sub001.cer"))
);

print_r($data);

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($mbox->getHeaderRequest());
	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";

	$r= $mbox->getRespuesta();
	print_r($r);

	# generar CSR
	#$fp= fopen(getcwd()."/cert.csr", "w");
	#fwrite($fp, base64_decode($r->result->csr));
	#fclose($fp);
	#$fp= fopen(getcwd()."/private.key", "w");
	#fwrite($fp, base64_decode($r->result->private_key));
	#fclose($fp);
	#$fp= fopen(getcwd()."/public.key", "w");
	#fwrite($fp, base64_decode($r->result->public_key));
	#fclose($fp);

	# generar PFX
	$fp= fopen(getcwd()."/salida.pfx", "w");
	fwrite($fp, base64_decode($r->result->pfx));
	fclose($fp);
}
echo "\n\n";
?>
