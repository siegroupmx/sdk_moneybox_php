#!/usr/bin/php
<?php
$cert= file_get_contents(getcwd()."/3152705484.crt");
$certTmp= getcwd()."/3152705484.crt";
$private_key= file_get_contents(getcwd()."/firma.key");
$cert2= file_get_contents(getcwd()."/root.cer");
$cert3= file_get_contents(getcwd()."/Sub001.cer");
$pass= '123456';
$args= array(
	"extracerts"=>array($cert2, $cert3), 
	"friendly_name"=>"Hola que tal"
);

# DER to PEM
$certPem= "-----BEGIN CERTIFICATE-----\n";
$certPem .= chunk_split(base64_encode($cert), 64, "\n");
$certPem .= "-----END CERTIFICATE-----\n";
$fp= fopen(getcwd()."/salida.pem", "w");
fwrite($fp, $certPem);
fclose($fp);

# openssl x509 -inform DER -in 3152705484.crt -out salida.pem -outform PEM
# openssl_x509_export($cert, $outCert);
#$outCert= openssl_x509_parse($cert);

openssl_pkcs12_export($certPem, $out, $private_key, $pass, $args);

#echo "\n\nDataCert:\n";
#print_r($cert);
echo "\n\nCert:\n";
$fp= fopen("salida.pfx", "w");
fwrite($fp, $out);
fclose($fp);

echo "\n\n";
?>
