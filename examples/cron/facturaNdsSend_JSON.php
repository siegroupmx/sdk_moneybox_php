<?php
/**
* envio de factura en formato JBOX
*/
function sendFacturaNds_JBOX_byConfig($fileName=false, $configFile=NULL) {
	if( !count($configFile) ) 	return 0;
	else {
		#if( !file_exists(RESOURCES.$configFile["ConfigData"]) ) # configuracion del Mapeo
		#	return 0;
		#else if( !file_exists(RESOURCES.$configFile["Template"]) ) # factura JBOX que se manda a moneyBox
		#	return 0;
		if( strcmp($configFile["InputFile"], "jbox") ) # distinto de TXT la entrada de la "factura original"
			return 0;
		else if( strcmp($configFile["DataFile"], "xml") ) # contenido del Mapeo distinto a XML
			return 0;
		else {
			# print_r($configFile);
			# echo "\nBuffer de Lectura: ". $configFile["InputFile"];
			# echo "\nData Content: ". $configFile["DataFile"];
			# echo "\nConfiguracion: ". RESOURCES.$configFile["ConfigData"]. "\n\n";
			# echo "\nTemplate: ". RESOURCES.$configFile["Template"];

			$fileContent= file_get_contents($fileName);

			if( !$fileContent ) 		return 0;
			else {
				$facturaJson= json_decode($fileContent, true);
				$facturaJson["tipo"]= "credito";
				$facturaJson["formato"]= "normal";
				$hashMd5= md5(json_encode($facturaJson));
				$facturaJson["huella"]= $hashMd5;

				$fecha=time();
				$newname= SDK.'tmp/'.$fecha; # temporal donde generamos JSON final
				$file= fopen($newname, "w");
				$outJson= json_encode($facturaJson);
				fputs($file, $outJson, strlen($outJson));
				# fputs($file, $outArray, strlen($outJson));
				fclose($file);
				unset($file);

				$r= sendToMoneyBox($facturaJson);
				# print_r($facturaJson);
			}
		return $r;
		}
	}
}

/**
* envio de factura en formato JBOX
*/
function sendFacturaNds_JBOX($fileName=false) {
	echo "\n---> Enviando Nota DocumentoSoporte JBOX";
}
?>
