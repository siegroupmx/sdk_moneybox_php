<?php
$nuevos= $tmpSecret.'nuevos.txt'; # archivos temporal de nuevos

if( !file_exists($nuevos) ) {
	echo "> No se encontraron documentos electronicos a subir..";
}
else {
	$fp= fopen($nuevos, "r");
	$archivo= array();
	while( ($files=fgets($fp, (10*1024)))!==FALSE ) {
		$x= explode("\t", $files);
		if( file_exists($x[0]) ) # prevencion existencia
			$archivo[]= $x[0];
		unset($x);
	}
	fclose($fp);

	echo "\nSe encontraron: ". count($archivo). " documento". (count($archivo)==1 ? "":"s"). "\n";

	foreach( $archivo as $key ) {
		$x= explode("/", $key);
		$estado= false;
		echo "\nEnviando ". $x[count($x)-1]. " --> ";
		$ext= false;

		foreach( $extSoportados as $key2 ) {
			if( strstr($x[count($x)-1], $key2) )
				$ext= $key2;
		}

		if( $ext ) {
			if( !strcmp($argv[2], "factura") ) # enviando factura
				$estado= sendFactura($key, $ext); # enviamos al orquestador
			else if( !strcmp($argv[2], "nc") ) # enviando nota de credito
				$estado= sendFacturaNc($key, $ext); # enviamos al orquestador
			else if( !strcmp($argv[2], "nd") ) # enviando nota de debito
				$estado= sendFacturaNd($key, $ext); # enviamos al orquestador
			else if( !strcmp($argv[2], "ds") ) # enviando documento soporte
				$estado= sendFacturaDs($key, $ext); # enviamos al orquestador
			else if( !strcmp($argv[2], "nds") ) # enviando nota ajuste documento soporte
				$estado= sendFacturaNds($key, $ext); # enviamos al orquestador
		}

		echo ($estado ? "Exito":"Fallo");
	}
	unlink($nuevos);
}
unset($nuevos);
?>