<?php
if( !strcmp($argv[1], "-xml2mbox") ) { # limpia temporales de busqueda
	if( !is_array($GLOBALS['dirFacturas']) || !count($GLOBALS['dirFacturas']) )
		echo "\n\nXML2MBOX: Problemas para encontrar los directorios fuente y destino..";
	else {
		echo "\n\n### XML2MBOX\n";

		/**
		* patron para identificar una factura de multiples archivos, especialmente cuando todos los documentos de factura, nc y nd estan en 
		* un mismo directorio y deseamos separarlos en sus carpetas respectivas (ordenar)
		* descomentar el arreglo en caso que necesites usarlo
		*/
		$patronFormatos= array(
			"factura"=>array("muestra"=>"7-", "de"=>0, "a"=>2), 
			"nc"=>array("muestra"=>"8-", "de"=>0, "a"=>2), 
			"nd"=>array("muestra"=>"9-", "de"=>0, "a"=>2), 
			"ds"=>array("muestra"=>"5-", "de"=>0, "a"=>2), 
			"nds"=>array("muestra"=>"6-", "de"=>0, "a"=>2)
		);

		foreach( $GLOBALS['dirFacturas'] as $key=>$val ) {
			echo "\n[". ucfirst($key). "]";
			echo "\n\tCarpeta Fuenta: ". $val["in"]. "\t". ((file_exists($val["in"]) && is_dir($val["in"])) ? "Correcto":"ERROR");
			echo "\n\tCarpeta Destino: ". $val["out"]. "\t". ((file_exists($val["out"]) && is_dir($val["out"])) ? "Correcto":"ERROR");
		}

		echo "\n\n### XML2MBOX: Convirtiendo XML a MBOX format\n";
		foreach( $GLOBALS['dirFacturas'] as $key=>$val ) {
			if( file_exists($val["in"]) && is_dir($val["in"]) ) {
				$files2Mbox= readDirectory($val["in"], true);
				# print_r($files2Mbox);

				foreach( $files2Mbox as $key2=>$val2 ) { # leemos la carpeta "in" (donde estan los XMLs)
					if( !strcmp(substr(strtolower($val2["name"]), -4), ".xml") ) { # si es XML
						if( !file_exists($val["out"].substr($val2["name"], 0, -4).".mbox") ) { # si no existe como MBOX, aun no se ha validado ante moneyBox
							$newname= strtolower(substr($val2["name"], 0, -4)).'.mbox'; # nombre con extencion .mbox
							$fileFrom= $val2["path"].$val2["name"]; # ruta al archivo original XML
							$fileTo= $val["out"].$newname; # nueva ruta donde se copiara con extension .mbox

							if( isset($patronFormatos) && count($patronFormatos[$key]) ) { # por patron coincidencia del nombre del XML original
								# extraemos la "muestra" usando "de" y "a", si es igual a al muestra pasa
								if( !strcmp( substr($val2["name"], $patronFormatos[$key]["de"], $patronFormatos[$key]["a"]), $patronFormatos[$key]["muestra"]) ) { 
									echo "\n* Convirtiendo ". $fileFrom. " a formato .MBOX\tOK";
									echo "\n* Validando Muestra: ". $patronFormatos[$key]["muestra"];
									copy($fileFrom, $fileTo);
								}
							}
							else {
								echo "\n* Convirtiendo ". $fileFrom. " a formato .MBOX\tOK";
								copy($fileFrom, $fileTo);
							}
							unset($fileFrom, $fileTo);
						}
					}
				}
			}
		}
	}
}
?>