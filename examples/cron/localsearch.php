<?php
$target=array();
if( !strcmp($argv[2], "--auto") ) { # si es "auto", porque leeremos de un directorio distinto a donde se guardaran
	$target= $GLOBALS['dirFacturas']; # arreglo donde estan las rutas "in" y "out" de cada tipo de documento electronico
}
else 
	$target= $argv[2]; # directorio # directorio unico

if( is_array($target) && count($target) ) { # arreglo de directorios
	foreach( $target as $key=>$val ) {
		if( !file_exists($val["in"]) && !is_dir($val["in"]) ) {
			echo "\n\n Error - Directorio de [". $key. "] no existe: ". $val["in"];
		}
		else {
			$data= readDirectory(
				$val["in"], 
				$extSoportados, # necesito estos
				$extDone # excluir estos
			);

			if( is_array($data) && count($data) ) {
				if( !makeHiddenFile("nuevos.txt", $data) )
					echo "\n\n### ERROR !!! - No se logor establecer los archivos encontrados de [". $key. "]..";
				else {
					echo "\n\nNuevos documentos encontrados de [". $key. "]:\n";
					
					foreach( $data as $key2=>$val2 ) {
						echo "\n". $val2["name"]."\t". $val2["hash"];
					}
				}
			}
			else
				echo "\n\n> No se encontraron nuevos Documentos Electronicos en [". $key. "]..";
		}
	}
}
else {
	$data= readDirectory(
		$target, 
		$extSoportados, # necesito estos
		$extDone # excluir estos
	);

	if( is_array($data) && count($data) ) {
		if( !makeHiddenFile("nuevos.txt", $data) )
			echo "\n\n### ERROR !!! - No se logor establecer los archivos encontrados..";
		else {
			echo "\n\nNuevos documentos encontrados:\n";
			
			foreach( $data as $key=>$val ) {
				echo "\n". $val["name"]."\t". $val["hash"];
			}
		}
	}
	else
		echo "\n\n> No se encontraron nuevos Documentos Electronicos..";
}
?>