<?php
/**
* envio de factura en formato JBOX
*/
function sendFactura_JBOX_byConfig($fileName=false, $configFile=NULL) {
	if( !count($configFile) ) 	return 0;
	else {
		#if( !file_exists(RESOURCES.$configFile["ConfigData"]) ) # configuracion del Mapeo
		#	return 0;
		#if( !file_exists(RESOURCES.$configFile["Template"]) ) # factura JBOX que se manda a moneyBox
		#	return 0;
		if( strcmp($configFile["InputFile"], "jbox") ) # distinto de TXT la entrada de la "factura original"
			return 0;
		else if( strcmp($configFile["DataFile"], "xml") ) # contenido del Mapeo distinto a XML
			return 0;
		else {
			#print_r($configFile);
			#echo "\nBuffer de Lectura: ". $configFile["InputFile"];
			#echo "\nData Content: ". $configFile["DataFile"];
			#echo "\nConfiguracion: ". $configFile["ConfigData"];
			#echo "\nTemplate: ". RESOURCES.$configFile["Template"];
			#echo "\nArchivo: ". $fileName. "\n\n";

			$fileContent= file_get_contents($fileName);

			#print_r($fileContent);
			#echo "\n\n";

			if( !$fileContent ) 		return 0;
			else {
				$facturaJson= json_decode($fileContent, true);
				#echo "\n\nConten JSON:\n";
				#print_r($facturaJson);
				#echo "\n\n";
				$facturaJson["tipo"]= "factura";
				$facturaJson["formato"]= "normal";
				$hashMd5= md5(json_encode($facturaJson));
				$facturaJson["huella"]= $hashMd5;
				$facturaJson["mailsend"]= 1; // 1=envia mail de la factura, 2=no lo envia al mail
				$facturaJson["autogenfolios"]= 2; // 1=si dispara el autoampliador de folios, 2=no dispara el ampliados, general el folio al vuelo

				$fecha=time();
				$newname= SDK.'tmp/'.$fecha; # temporal donde generamos JSON final
				$file= fopen($newname, "w");
				$outJson= json_encode($facturaJson);
				fputs($file, $outJson, strlen($outJson));
				# fputs($file, $outArray, strlen($outJson));
				fclose($file);
				unset($file);
				
				$r= sendToMoneyBox($facturaJson);
			}
		return $r;
		}
	}
}

/**
* envio de factura en formato JBOX
*/
function sendFactura_JBOX($fileName=false) {
	echo "\n---> Enviando JBOX";
}
?>