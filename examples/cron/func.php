<?php
/**
* previene caracteres indeceados
*
* @param string $a la cadena de texto
* @return string la nueva cadena
*/
function fixTextPrev($a=NULL) {
	if( !$a ) 	return 0;
	$q= array("\t", "\r", "%", "\"", "$", "'", "+", "\\", "@", "?", "*", "°");
	$res= array(" ", " ", "&#37;", "&#34;", "&#36;", "&#39;", "&#43;", "&#92;", "&#64;", "&#63;", "&#42;", "&#176;");
	$a= sprintf(str_replace($q, $res, $a));
	$aux="";
	$r="";
	$fl=false;

	for($i=(strlen($a)-1); $i>=0; $i-- ) {
        if( !$fl && !strcmp($a[$i], " ") )
                $fl= false;
        else {
                $r .= $a[$i];
                $fl= true;
        }
	}

	for( $i=(strlen($r)-1); $i>=0; $i-- ) {
        $aux .= $r[$i];
	}
	return sprintf($aux);
}

/**
* previene el uso de palabras acentuadas para busquedas entre el Webservice y la muestra del cliente
*/
function quitarCentos($a=NULL) {
	if( !$a ) 	return 0;
	else {
		$a= str_replace("&sup3;", "", $a);
		$a= strtolower($a);
		$a= str_replace("tilde;", "acute;", $a);
		$acentos= array("&eacute;", "&aacute;", "&iacute;", "&oacute;", "&uacute;", "&nacute;", "&uuml;");
		$reemplazo= array("e", "a", "i", "o", "u", "n", "u");
		$tmpA= str_replace( $acentos, $reemplazo, $a );
		$tmpA= str_replace( array(" ", ".", ","), array("","",""), $tmpA );
		return $tmpA;
	}
}

/**
* reduce los espacios a solo uno por palabra
*
* @param string $a la cadena de texto
* @return string la nueva palabra
*/
function reduceSpaces($a=NULL) {
	if( !$a ) 	return 0;
	else {
		$fl= false;
		$r='';
		foreach( $a as $key=>$val ) {
			if( !strcmp($val, " ") && !$fl ) {
				$fl= true;
				$r .= $val;
			}
			else {
				$fl= false;
				$r .= $val;
			}
		}
		return $r;
	}
}

/**
* lee directorio
*
* @param string $a ruta al directorio que se pretende leer
* @param 
*
* @return array arreglo de datos de la informacion
*/
function readDirectory($a=false, $need=false, $exclude=false) {
	if( !$a || is_array($a) ) 	return false;
	else if( !$need ) 	return false;
	else {
		$dir= opendir($a);
		$r=array();

		echo "\n\nLeyendo: ". $a;

		while( ($buf=readdir($dir))!==FALSE ) {
			if( strcmp($buf, ".") && strcmp($buf, "..") ) {
				if( is_file($a.$buf) && !is_dir($a.$buf) ) {
					if( $exclude ) { # si hay para excluir
						$fl=true;
						if( is_array($exclude) ) { # en arreglo
							$x= explode(".", $buf); # extraemos la extension
							$ext= $x[(count($x)-1)]; # la extension
							$theFile= substr($buf, 0, -(strlen($ext)+1)); # nombre del archivo sin extension

							foreach( $exclude as $key ) {
								if( strstr($buf, $key) )
									$fl= false;
								else if( file_exists($a.$theFile.$key) ) # si existe el hermano a excluir
									$fl= false; # activamos prevencion
							}
						}
						else { # un solo elemento
							if( strstr($buf, $exclude) )
								$fl= false;
						}

						if( $fl ) # si no tuvo hermanos falsos
							$r[]= array("name"=>$buf, "path"=>$a, "hash"=>md5_file($a.$buf));
					}
					else {
						$fl=true;
						/**
						* por seguridad verificamos si el archivo no tiene "hermanos" que seran parte de los excluidos
						*/
						if( $exclude ) { # si hay para excluir
							$x= explode(".", $buf); # extraemos la extension
							$ext= $x[(count($x)-1)]; # la extension
							$theFile= substr($buf, 0, -(strlen($ext)+1)); # nombre del archivo sin extension

							if( is_array($exclude) ) { # en arreglo
								foreach( $exclude as $key ) {
									if( file_exists($a.$theFile.$key) ) # si existe el hermano a excluir
										$fl= false; # activamos prevencion
								}
							}
							else { # un solo elemento
								if( file_exists($a.$theFile.$exclude) ) # si existe el hermano a excluir
									$fl= false; # activamos prevencion
							}
						}

						if( $fl ) # si no tiene hermanos falsos
							$r[]= array( "name"=>$buf, "path"=>$a, "hash"=>md5_file($a.$buf)); # agregamos
					}
				}
			}
		}
		return $r;
	}
}

/**
* crea archivo oculto con los nuevos archivos a interactuar mas adelante
*
* @return boolean confirma fallo o exito de creacion del archivo
*/
function makeHiddenFile($fileName=false, $data=false) {
	if( !$fileName ) 	return 0;
	else if( !is_array($data) || !count($data) ) 	return 0;
	else {
		$dir= $GLOBALS['tmpSecret'];
		$buf="";

		foreach( $data as $key=>$val ) {
			$buf .= $val["path"].$val["name"]. "\t". $val["hash"]."\n";
		}

		$fp= fopen($dir.$fileName, "a+");
		fwrite($fp, $buf);
		fclose($fp);
		return 1;
	}
}

/**
* elimina archivo oculto de temporal
*
* @return boolean confirma fallo o exito de eliminacion del archivo
*/
function cleanHiddenFile($fileName=false) {
	if( !$fileName ) 	return 0;
	else {
		$dir= $GLOBALS['tmpSecret'];

		if( !file_exists($dir.$fileName) ) # archivo no existe
			return 0;
		else { # archivo encontrado
			unlink($dir.$fileName); # eliminamos
			return 1;
		}
	}
}

/**
* carga el archivo de configuraion
*
* @param string ruta al archivo de configuracion
*
* @return array arreglo de comandos
*/
function readScript( $file ) {
	$i=0; # contador 
	$arr= array(); # arreglo
	$fp= fopen($file, "r");
	while(!feof($fp) )
		{
		$buf= fgets( $fp, 2048); # leemos
		# si es distinto de comentario, vacio o salto de linea, leemos
		if( strcmp($buf[0], "#") && strcmp($buf[0], "") && strcmp($buf[0], "\n") )
			{
			$x= explode( "=", $buf ); # explotando
			$arr[$x[0]]= $x[1]; # metemos al arreglo
			$i++; # incremento 
			}
		}
	fclose($fp);
	unset($x);
	return $arr; # retornamos 
	}

/**
* obtiene las ciudades del ws moneyBox
*
* @return array configuracion sesgada por 3 arreglos "factura", "nc", y "nd"
*/
function isConfigFile() {
	$fileConfig= RESOURCES.'config.cfg'; # archivo de configuracion

	if( !file_exists($fileConfig) ) 	return 0;
	else { # exite
		$rFile= readScript($fileConfig);
		
		if( !count($rFile) ) 	return 0;
		else {
			$r= array( "factura"=>array(), "nc"=>array(), "nd"=>array(), "ds"=>array(), "nds"=>array() );

			foreach( $rFile as $key=>$val ) {
				foreach( $r as $key2=>$val2 ) {
					if( strstr($key, $key2) ) {
						$r[$key2][substr($key, strlen($key2), strlen($key))]= substr($val, 0, -1);
					}
				}
			}

			return $r;
		}
	}
}

/**
* Orquestador de formatos para envio de facturas al WS moneyBox
*
* @param string $fileName ruta completa al archivo factura
* @param string $format extension del archivo
* @return boolean estado del proceso de envio
*/
function sendFactura($fileName=false, $format=false) {
	$fl=false;
	$r= false;
	$c= true;
	$prev=0;
	$dormir= 30; # dormir 30 segundos

	foreach( $GLOBALS['extSoportados'] as $key ) { # verificacion de seguridad
		if( !strcmp($key, $format) )
			$fl= true;
	}

	$cfgPresent= isConfigFile();
	#print_r($cfgPresent);

	if( $fl ) { # si es formato reconocido
		#echo "\n\nFormato: \n";
		#print_r($format);
		#echo "\n\n--->";

		#do {
			$prev++; # prevenir loop

			if( $prev>1 )
				sleep($dormir);

			if( !strcmp($format, ".csv") ) # formato CSV
				$r= (!count($cfgPresent["factura"]) ? sendFactura_CSV($fileName):sendFactura_CSV_byConfig($fileName, $cfgPresent["factura"]));
			else if( !strcmp($format, ".xls") || !strcmp($format, ".xlsx") ) # formato XLS o XLSX
				$r= (!count($cfgPresent["factura"]) ? sendFactura_XLS($fileName):sendFactura_XLS_byConfig($fileName, $cfgPresent["factura"]));
			else if( !strcmp($format, ".txt") ) # formato TXT
				$r= (!count($cfgPresent["factura"]) ? sendFactura_TXT($fileName):sendFactura_TXT_byConfig($fileName, $cfgPresent["factura"]));
			else if( !strcmp($format, ".mbox") ) # formato MBOX
				$r= (!count($cfgPresent["factura"]) ? sendFactura_MBOX($fileName):sendFactura_MBOX_byConfig($fileName, $cfgPresent["factura"]));
			else if( !strcmp($format, ".jbox") ) # formato JBOX
				$r= (!count($cfgPresent["factura"]) ? sendFactura_JBOX($fileName):sendFactura_JBOX_byConfig($fileName, $cfgPresent["factura"]));

			$path= getFileInfo($fileName, "path");
			$nameFile= getFileInfo($fileName, "name");

			if(isset($r->result->timbre_fiscal)) { # si obtuvimos exitoso UUID
				if( file_exists($path.$nameFile.".error") ) {
					unlink($path.$nameFile.".error");
				}
				
				$zip= @file_get_contents($r->result->url_zip); # obtenemos zip
				$fp= @fopen($path.$nameFile.".zip", "w");
				@fwrite($fp, $zip);
				@fclose($fp);
				unset($fp);

				$xml= @file_get_contents($r->result->url_xml); # obtenemos xml
				$fp= @fopen($path.$nameFile.".xml", "w");
				@fwrite($fp, $xml);
				@fclose($fp);
				unset($fp);

				$pdf= @file_get_contents($r->result->url_pdf); # obtenemos pdf
				$fp= @fopen($path.$nameFile.".pdf", "w");
				@fwrite($fp, $pdf);
				@fclose($fp);
				unset($fp);

				$qrcode= @file_get_contents($r->result->url_qrcode); # obtenemos xml
				$fp= @fopen($path.$nameFile."_qrcode.jpg", "w");
				@fwrite($fp, $qrcode);
				@fclose($fp);
				unset($fp);

				$r= true;
				$c= false; # ya no continuar lanzando

				#echo "\n\nExito recibi Timbre fiscal: ". $r->result->timbre_fiscal;
				#echo "\nR= ". ($r ? "true":"falso");
				#echo "\nC= ". ($c ? "true":"falso");
			}
			else if( $r->error_code && $r->error ) {
				$error= (isset($r->error_code) ? $r->error_code:''). ': '. (isset($r->error) ? $r->error:''); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}
			else { // sin respuesta
				$error= print_r($r, true); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}

			# echo "\n\nPulsa Enter.....\n\n";
			# $rl= readline();
		#} while($c && $prev<3 );
	}

	return $r;
}

/**
* Orquestador de formatos para envio de facturas Nota Credito al WS moneyBox
*
* @param string $fileName ruta completa al archivo factura
* @param string $format extension del archivo
* @return boolean estado del proceso de envio
*/
function sendFacturaNc($fileName=false, $format=false) {
	$fl=false;
	$r= false;
	$prev=0;
	$dormir= 30; # dormir 30 segundos

	foreach( $GLOBALS['extSoportados'] as $key ) { # verificacion de seguridad
		if( !strcmp($key, $format) )
			$fl= true;
	}

	$cfgPresent= isConfigFile();

	if( $fl ) { # si es formato reconocido
		do {
			$prev++;

			if( $prev>1 )
				sleep($dormir);

			if( !strcmp($format, ".csv") ) # formato CSV
				$r= (!count($cfgPresent["nc"]) ? sendFacturaNc_CSV($fileName):sendFacturaNc_CSV_byConfig($fileName, $cfgPresent["nc"]));
			else if( !strcmp($format, ".xls") || !strcmp($format, ".xlsx") ) # formato XLS o XLSX
				$r= (!count($cfgPresent["nc"]) ? sendFacturaNc_XLS($fileName):sendFacturaNc_XLS_byConfig($fileName, $cfgPresent["nc"]));
			else if( !strcmp($format, ".txt") ) # formato TXT
				$r= (!count($cfgPresent["nc"]) ? sendFacturaNc_TXT($fileName):sendFacturaNc_TXT_byConfig($fileName, $cfgPresent["nc"]));
			else if( !strcmp($format, ".mbox") ) # formato MBOX
				$r= (!count($cfgPresent["nc"]) ? sendFacturaNc_MBOX($fileName):sendFacturaNc_MBOX_byConfig($fileName, $cfgPresent["nc"]));
			else if( !strcmp($format, ".jbox") ) # formato JBOX
				$r= (!count($cfgPresent["nc"]) ? sendFacturaNc_JBOX($fileName):sendFacturaNc_JBOX_byConfig($fileName, $cfgPresent["nc"]));

			$path= getFileInfo($fileName, "path");
			$nameFile= getFileInfo($fileName, "name");

			if(isset($r->result->timbre_fiscal)) { # si obtuvimos exitoso UUID
				if( file_exists($path.$nameFile.".error") ) {
					unlink($path.$nameFile.".error");
				}

				$zip= file_get_contents($r->result->url_zip); # obtenemos zip
				$fp= fopen($path.$nameFile.".zip", "w");
				fwrite($fp, $zip);
				fclose($fp);
				unset($fp);

				$xml= file_get_contents($r->result->url_xml); # obtenemos xml
				$fp= fopen($path.$nameFile.".xml", "w");
				fwrite($fp, $xml);
				fclose($fp);
				unset($fp);

				$qrcode= @file_get_contents($r->result->url_qrcode); # obtenemos xml
				$fp= @fopen($path.$nameFile."_qrcode.jpg", "w");
				@fwrite($fp, $qrcode);
				@fclose($fp);
				unset($fp);

				$pdf= file_get_contents($r->result->url_pdf); # obtenemos pdf
				$fp= fopen($path.$nameFile.".pdf", "w");
				fwrite($fp, $pdf);
				fclose($fp);
				unset($fp);
				$r= true;
			}
			else if( $r->error_code && $r->error ) {
				$error= $r->error_code. ': '. $r->error; # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r=false;
			}
			else { // sin respuesta
				$error= print_r($r, true); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}
		} while($c && $prev<3 );
	}

	return $r;
}

/**
* Orquestador de formatos para envio de facturas Nota Debito al WS moneyBox
*
* @param string $fileName ruta completa al archivo factura
* @param string $format extension del archivo
* @return boolean estado del proceso de envio
*/
function sendFacturaNd($fileName=false, $format=false) {
	$fl=false;
	$r= false;
	$prev=0;
	$dormir= 30; # dormir 30 segundos

	foreach( $GLOBALS['extSoportados'] as $key ) { # verificacion de seguridad
		if( !strcmp($key, $format) )
			$fl= true;
	}

	$cfgPresent= isConfigFile();

	if( $fl ) { # si es formato reconocido
		do {
			$prev++;

			if( $prev>1 )
				sleep($dormir);

			if( !strcmp($format, ".csv") ) # formato CSV
				$r= (!count($cfgPresent["nd"]) ? sendFacturaNd_CSV($fileName):sendFacturaNd_CSV_byConfig($fileName, $cfgPresent["nd"]));
			else if( !strcmp($format, ".xls") || !strcmp($format, ".xlsx") ) # formato XLS o XLSX
				$r= (!count($cfgPresent["nd"]) ? sendFacturaNd_XLS($fileName):sendFacturaNd_XLS_byConfig($fileName, $cfgPresent["nd"]));
			else if( !strcmp($format, ".txt") ) # formato TXT
				$r= (!count($cfgPresent["nd"]) ? sendFacturaNd_TXT($fileName):sendFacturaNd_TXT_byConfig($fileName, $cfgPresent["nd"]));
			else if( !strcmp($format, ".mbox") ) # formato MBOX
				$r= (!count($cfgPresent["nd"]) ? sendFacturaNd_MBOX($fileName):sendFacturaNd_MBOX_byConfig($fileName, $cfgPresent["nd"]));
			else if( !strcmp($format, ".jbox") ) # formato JBOX
				$r= (!count($cfgPresent["nd"]) ? sendFacturaNd_JBOX($fileName):sendFacturaNd_JBOX_byConfig($fileName, $cfgPresent["nd"]));

			$path= getFileInfo($fileName, "path");
			$nameFile= getFileInfo($fileName, "name");

			if(isset($r->result->timbre_fiscal)) { # si obtuvimos exitoso UUID
				if( file_exists($path.$nameFile.".error") ) {
					unlink($path.$nameFile.".error");
				}

				$zip= file_get_contents($r->result->url_zip); # obtenemos zip
				$fp= fopen($path.$nameFile.".zip", "w");
				fwrite($fp, $zip);
				fclose($fp);
				unset($fp);

				$xml= file_get_contents($r->result->url_xml); # obtenemos xml
				$fp= fopen($path.$nameFile.".xml", "w");
				fwrite($fp, $xml);
				fclose($fp);
				unset($fp);

				$qrcode= @file_get_contents($r->result->url_qrcode); # obtenemos xml
				$fp= @fopen($path.$nameFile."_qrcode.jpg", "w");
				@fwrite($fp, $qrcode);
				@fclose($fp);
				unset($fp);

				$pdf= file_get_contents($r->result->url_pdf); # obtenemos pdf
				$fp= fopen($path.$nameFile.".pdf", "w");
				fwrite($fp, $pdf);
				fclose($fp);
				unset($fp);
				$r= true;
			}
			else if( $r->error_code && $r->error ) {
				$error= $r->error_code. ': '. $r->error; # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r=false;
			}
			else { // sin respuesta
				$error= print_r($r, true); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}
		} while($c && $prev<3 );
	}

	return $r;
}

/**
* Orquestador de formatos para envio de documento soporte al WS moneyBox
*
* @param string $fileName ruta completa al archivo documento soporte
* @param string $format extension del archivo
* @return boolean estado del proceso de envio
*/
function sendFacturaDs($fileName=false, $format=false) {
	$fl=false;
	$r= false;
	$c= true;
	$prev=0;
	$dormir= 30; # dormir 30 segundos

	foreach( $GLOBALS['extSoportados'] as $key ) { # verificacion de seguridad
		if( !strcmp($key, $format) )
			$fl= true;
	}

	$cfgPresent= isConfigFile();
	// print_r($cfgPresent);

	if( $fl ) { # si es formato reconocido
		#echo "\n\nFormato: \n";
		#print_r($format);
		#echo "\n\n--->";

		#do {
			$prev++; # prevenir loop

			if( $prev>1 )
				sleep($dormir);

			if( !strcmp($format, ".csv") ) # formato CSV
				$r= (!count($cfgPresent["ds"]) ? sendFacturaDs_CSV($fileName):sendFacturaDs_CSV_byConfig($fileName, $cfgPresent["ds"]));
			else if( !strcmp($format, ".xls") || !strcmp($format, ".xlsx") ) # formato XLS o XLSX
				$r= (!count($cfgPresent["ds"]) ? sendFacturaDs_XLS($fileName):sendFacturaDs_XLS_byConfig($fileName, $cfgPresent["ds"]));
			else if( !strcmp($format, ".txt") ) # formato TXT
				$r= (!count($cfgPresent["ds"]) ? sendFacturaDs_TXT($fileName):sendFacturaDs_TXT_byConfig($fileName, $cfgPresent["ds"]));
			else if( !strcmp($format, ".mbox") ) # formato MBOX
				$r= (!count($cfgPresent["ds"]) ? sendFacturaDs_MBOX($fileName):sendFacturaDs_MBOX_byConfig($fileName, $cfgPresent["ds"]));
			else if( !strcmp($format, ".jbox") ) # formato JBOX
				$r= (!count($cfgPresent["ds"]) ? sendFacturaDs_JBOX($fileName):sendFacturaDs_JBOX_byConfig($fileName, $cfgPresent["ds"]));

			$path= getFileInfo($fileName, "path");
			$nameFile= getFileInfo($fileName, "name");

			if(isset($r->result->timbre_fiscal)) { # si obtuvimos exitoso UUID
				if( file_exists($path.$nameFile.".error") ) {
					unlink($path.$nameFile.".error");
				}
				
				$zip= @file_get_contents($r->result->url_zip); # obtenemos zip
				$fp= @fopen($path.$nameFile.".zip", "w");
				@fwrite($fp, $zip);
				@fclose($fp);
				unset($fp);

				$xml= @file_get_contents($r->result->url_xml); # obtenemos xml
				$fp= @fopen($path.$nameFile.".xml", "w");
				@fwrite($fp, $xml);
				@fclose($fp);
				unset($fp);

				$qrcode= @file_get_contents($r->result->url_qrcode); # obtenemos xml
				$fp= @fopen($path.$nameFile."_qrcode.jpg", "w");
				@fwrite($fp, $qrcode);
				@fclose($fp);
				unset($fp);

				$pdf= @file_get_contents($r->result->url_pdf); # obtenemos pdf
				$fp= @fopen($path.$nameFile.".pdf", "w");
				@fwrite($fp, $pdf);
				@fclose($fp);
				unset($fp);
				$r= true;
				$c= false; # ya no continuar lanzando

				#echo "\n\nExito recibi Timbre fiscal: ". $r->result->timbre_fiscal;
				#echo "\nR= ". ($r ? "true":"falso");
				#echo "\nC= ". ($c ? "true":"falso");
			}
			else if( $r->error_code && $r->error ) {
				$error= (isset($r->error_code) ? $r->error_code:''). ': '. (isset($r->error) ? $r->error:''); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}
			else { // sin respuesta
				$error= print_r($r, true); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}

			# echo "\n\nPulsa Enter.....\n\n";
			# $rl= readline();
		#} while($c && $prev<3 );
	}

	return $r;
}

/**
* Orquestador de formatos para envio de nota ajuste documento soporte al WS moneyBox
*
* @param string $fileName ruta completa al archivo nota ajuste documento soporte
* @param string $format extension del archivo
* @return boolean estado del proceso de envio
*/
function sendFacturaNds($fileName=false, $format=false) {
	$fl=false;
	$r= false;
	$c= true;
	$prev=0;
	$dormir= 30; # dormir 30 segundos

	foreach( $GLOBALS['extSoportados'] as $key ) { # verificacion de seguridad
		if( !strcmp($key, $format) )
			$fl= true;
	}

	$cfgPresent= isConfigFile();
	#print_r($cfgPresent);

	if( $fl ) { # si es formato reconocido
		#echo "\n\nFormato: \n";
		#print_r($format);
		#echo "\n\n--->";

		#do {
			$prev++; # prevenir loop

			if( $prev>1 )
				sleep($dormir);

			if( !strcmp($format, ".csv") ) # formato CSV
				$r= (!count($cfgPresent["nds"]) ? sendFacturaNds_CSV($fileName):sendFacturaNds_CSV_byConfig($fileName, $cfgPresent["nds"]));
			else if( !strcmp($format, ".xls") || !strcmp($format, ".xlsx") ) # formato XLS o XLSX
				$r= (!count($cfgPresent["nds"]) ? sendFacturaNds_XLS($fileName):sendFacturaNds_XLS_byConfig($fileName, $cfgPresent["nds"]));
			else if( !strcmp($format, ".txt") ) # formato TXT
				$r= (!count($cfgPresent["nds"]) ? sendFacturaNds_TXT($fileName):sendFacturaNds_TXT_byConfig($fileName, $cfgPresent["nds"]));
			else if( !strcmp($format, ".mbox") ) # formato MBOX
				$r= (!count($cfgPresent["nds"]) ? sendFacturaNds_MBOX($fileName):sendFacturaNds_MBOX_byConfig($fileName, $cfgPresent["nds"]));
			else if( !strcmp($format, ".jbox") ) # formato JBOX
				$r= (!count($cfgPresent["nds"]) ? sendFacturaNds_JBOX($fileName):sendFacturaNds_JBOX_byConfig($fileName, $cfgPresent["nds"]));

			$path= getFileInfo($fileName, "path");
			$nameFile= getFileInfo($fileName, "name");

			if(isset($r->result->timbre_fiscal)) { # si obtuvimos exitoso UUID
				if( file_exists($path.$nameFile.".error") ) {
					unlink($path.$nameFile.".error");
				}
				
				$zip= @file_get_contents($r->result->url_zip); # obtenemos zip
				$fp= @fopen($path.$nameFile.".zip", "w");
				@fwrite($fp, $zip);
				@fclose($fp);
				unset($fp);

				$xml= @file_get_contents($r->result->url_xml); # obtenemos xml
				$fp= @fopen($path.$nameFile.".xml", "w");
				@fwrite($fp, $xml);
				@fclose($fp);
				unset($fp);

				$qrcode= @file_get_contents($r->result->url_qrcode); # obtenemos xml
				$fp= @fopen($path.$nameFile."_qrcode.jpg", "w");
				@fwrite($fp, $qrcode);
				@fclose($fp);
				unset($fp);

				$pdf= @file_get_contents($r->result->url_pdf); # obtenemos pdf
				$fp= @fopen($path.$nameFile.".pdf", "w");
				@fwrite($fp, $pdf);
				@fclose($fp);
				unset($fp);
				$r= true;
				$c= false; # ya no continuar lanzando

				#echo "\n\nExito recibi Timbre fiscal: ". $r->result->timbre_fiscal;
				#echo "\nR= ". ($r ? "true":"falso");
				#echo "\nC= ". ($c ? "true":"falso");
			}
			else if( $r->error_code && $r->error ) {
				$error= (isset($r->error_code) ? $r->error_code:''). ': '. (isset($r->error) ? $r->error:''); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}
			else { // sin respuesta
				$error= print_r($r, true); # obtenemos xml
				$fp= fopen($path.$nameFile.".error", "w");
				fwrite($fp, $error);
				fclose($fp);
				unset($fp, $error);
				$r= false;
				$c= false; # ya no continuar lanzando
			}

			# echo "\n\nPulsa Enter.....\n\n";
			# $rl= readline();
		#} while($c && $prev<3 );
	}

	return $r;
}

/**
* obtiene la informacion del archivo
*
* @param string $fileName ruta al archivo
* @param string $op opcion de lo que deseas obtener
*
* @return string dato a extraer, ya sea nombre o ruta antes del nombre
*/
function getFileInfo($fileName, $op) {
	if( !$fileName ) 	return flse;
	else {
		$x= explode("/", $fileName);
	}

	if( !strcmp($op, "namefull") ) { # nombre completo
		return $x[(count($x)-1)];
	}
	else if( !strcmp($op, "name") ) { # nombre sin extension
		$name= explode(".", $x[(count($x)-1)]);
		return $name[0];
	}
	else if( !strcmp($op, "path") ) { # ruta antes del nombre
		return substr($fileName, 0, -(strlen($x[(count($x)-1)])));
	}
	return false;
}

/**
* busca las ciudades del ws moneyBox
*
* @param string $a texto o codigo de ciudad
* @param string $b texto o codigo de estado/depto
* @return string Identificador de ciudad
*/
function searchCiudad($a=NULL, $b=NULL) {
	$r=0;
	if( !$a )	return $r;
	else {
		if( is_numeric($a) )
			$r= $a;
		else {
			$qTemp= getCiudades($b);

			foreach( $qTemp as $key=>$val ) {
				$tmp= htmlentities($a, ENT_QUOTES);
				if( !strcmp(strtolower($val->nombre), strtolower($tmp)) || !strcmp(quitarCentos($tmp), quitarCentos($val->nombre)) ) {
					$r= $val->id;
				}
				unset($tmp);
			}

			# cuando el nombre no fue exacto, trataremos de buscar por coincidencia
			if( !$r ) {
				foreach( $qTemp as $key=>$val ) {
					$tmp= htmlentities($a, ENT_QUOTES);
					if( strstr(strtolower($val->nombre), strtolower($tmp)) || strstr(quitarCentos($val->nombre), quitarCentos($tmp)) ) {
						$r= $val->id;
					}
					unset($tmp);
				}
			}
		unset($qTemp);
		}
	}
	return $r;
}

/**
* busca el estado/depto del ws moneyBox
*
* @return string Identificador de estado/depto
*/
function searchEstado($a=NULL) {
	$r= 0;
	if( !$a )	return $r;
	else {
		if( is_numeric($a) )
			$r= $a;
		else {
			$qTemp= getEstados();

			foreach( $qTemp as $key=>$val ) {
				$tmp= htmlentities($a, ENT_QUOTES);
				if( !strcmp(strtolower($val), strtolower($tmp)) || !strcmp(quitarCentos($tmp), quitarCentos($val)) ) {
					$r= $key;
				}
				unset($tmp);
			}
			unset($qTemp);
		}
	}
	return $r;
}

/**
* obtiene las ciudades del ws moneyBox
*
* @param sting $a estado/depto asociado
* @return object objeto json de ciudades
*/
function getCiudades($a=NULL) {
	$user= 'democo';
	$pass= 'gratis123';

	$path= 'cuenta/help/ciudades';
	$data= array("estado"=>($a ? $a:0));
	$idFormula= false;
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) {
		echo $mbox->getError();
		return false;
	}
	else {
		$abc= $mbox->getRespuesta();
		return $abc->result->ciudades;
		unset($abc);
	}
}

/**
* obtiene las estados del ws moneyBox
*
* @return object objeto json de estados
*/
function getEstados() {
	$user= 'democo';
	$pass= 'gratis123';

	$path= 'cuenta/help/estados';
	$data= array(); # vacio
	$idFormula= false;
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) {
		echo $mbox->getError();
		return false;
	}
	else {
		$abc= $mbox->getRespuesta();
		return $abc->result->estados;
		unset($abc);
	}
}

/**
* Envio al WS moneyBox
*
* @param array $factura arreglo de factura
*/
function sendToMoneyBox($factura, $devPartner=false) {
	$user= 'democo';
	$pass= 'gratis123';

	if( $devPartner ) { # no es partner
		/**
		* factura - generar nueva Partner
		*/
		$path= 'factura/save';
		$data= $factura; # vacio
	}
	else {
		/**
		* buscando al cliente
		*/
		$path= 'clientes/get';
		$data= array("nit"=>$factura["receptor"]["nit"]);
		$idCliente= false;
		$mbox= new moneyBox($user, $pass, $path, $data);
		if( $mbox->getError() ) {
			if( !strstr($mbox->getError(), "102") ) { # error distinto a 102 (no existe cliente)
				echo $mbox->getError();
				echo "\n===> Registrando ERROR 102 -- CLIENTE";
				return $mbox->getRespuesta();
			}
			else { # no existe cliente
				$estado= searchEstado($factura["receptor"]["estado"]);
				$ciudad= searchCiudad($factura["receptor"]["ciudad"], $estado);
				
				/**
				* agregar cliente
				*/
				$path2= 'clientes/add';
				$data2= array(
					"empresa"=>$factura["receptor"]["nombre"],
					"nombre"=>$factura["receptor"]["nombre"], 
					"nit"=>$factura["receptor"]["nit"], # identificacion tributaria para colombia
					"dv"=>$factura["receptor"]["dv"], # identificacion tributaria para colombia
					"rfc"=>NULL, # identificacion tributaria para mexico
					"calle"=>$factura["receptor"]["calle"], 
					"cp"=>$factura["receptor"]["cp"], 
					"ciudad"=>$ciudad, # 45buqqiq=Bogota, consultar la ayuda 
					"estado"=>$estado, # 9nvyhrrh6v=Cundinamarca, consultar la ayuda 
					"pais"=>"47", # 47=Colombia, consultar la ayuda
					"email"=>str_replace(array('&#64;', '&amp;#64;'), array('@','@'),$factura["receptor"]["email"]),
					"regimen_fiscal"=>"1", # 1=Juridico, consultar la ayuda 
					"telefono"=>$factura["receptor"]["telefono"], 
					"tipo_cliente"=>"yrlht9jj", # yrlht9jj=ClienteNacional, consultar ayuda
					"perfil"=>"1", # 1=Simplificado, consultar ayuda
					"responsabilidad"=>$factura["receptor"]["responsabilidad"], 
					"tipo_documento"=>$factura["receptor"]["tipo_documento"]
				);

				// echo "\n\nagregando cliente...\n";
				// print_r($data2);

				$mbox2= new moneyBox($user, $pass, $path2, $data2);
				if( $mbox2->getError() ) {
					#echo '[Error] '. $mbox2->getError();
					return $mbox2->getRespuesta();
				}
				else {
					echo "\n===> Agregado cliente...";
					$abc= $mbox2->getRespuesta();
					$idCliente= $abc->result->id;
					print_r($abc);
					unset($abc);
				}
			}
			# return $mbox->getRespuesta();
		}
		else {
			$abc= $mbox->getRespuesta();
			$idCliente= fixTextPrev($abc->result[0]->id);
			unset($abc);

			if( CLIENT_AUTOUPDATE ) { # actualizar en automatico siempre
				echo "\n\n!!!!===> ACTUALIZANDO CLIENTE =====\n";
				
				$estado= searchEstado($factura["receptor"]["estado"]);
				$ciudad= searchCiudad($factura["receptor"]["ciudad"], $estado);
				/**
				* actualizar cliente
				*/
				$path2= 'clientes/update';
				$data2= array(
					"id"=>$idCliente, 
					"empresa"=>$factura["receptor"]["nombre"],
					"nombre"=>$factura["receptor"]["nombre"], 
					"nit"=>$factura["receptor"]["nit"], # identificacion tributaria para colombia
					"dv"=>$factura["receptor"]["dv"], # identificacion tributaria para colombia
					"rfc"=>NULL, # identificacion tributaria para mexico
					"calle"=>$factura["receptor"]["calle"], 
					"cp"=>$factura["receptor"]["cp"], 
					"ciudad"=>$ciudad, 
					"estado"=>$estado, 
					"pais"=>"47", # 47=Colombia, consultar la ayuda
					"email"=>str_replace(array('&#64;', '&amp;#64;'), array('@','@'),$factura["receptor"]["email"]),
					"regimen_fiscal"=>"1", # 1=Juridico, consultar la ayuda 
					"telefono"=>$factura["receptor"]["telefono"], 
					"tipo_cliente"=>"yrlht9jj", # yrlht9jj=ClienteNacional, consultar ayuda
					"perfil"=>"1", # 1=Simplificado, consultar ayuda
					"responsabilidad"=>$factura["receptor"]["responsabilidad"], 
					"tipo_documento"=>$factura["receptor"]["tipo_documento"]
				);

				// echo "\n\nActualizando cliente...";
				// print_r($data2);

				$mbox2= new moneyBox($user, $pass, $path2, $data2);
				if( $mbox2->getError() ) {
					echo "\n## AUTOUPDATE: [Error] ". $mbox2->getError();
				}
				else {
					echo "\n## AUTOUPDATE: [Exito] ";
					// print_r($mbox2->getRespuesta());
				}
			}
		}

		/**
		* buscando formula
		*/
		$path= 'cuenta/formulas/list';
		$data= array(); # vacio
		$idFormula= false;
		$mbox= new moneyBox($user, $pass, $path, $data);
		if( $mbox->getError() ) {
			echo $mbox->getError();
			return $mbox->getRespuesta();
		}
		else {
			$abc= $mbox->getRespuesta();
			$idFormula= $abc->result[0]->id;
			unset($abc);
		}

		/**
		* firmas - listar
		*/
		$path= 'cuenta/firmas/list';
		$data= array();
		$idFirma= false;
		$mbox= new moneyBox($user, $pass, $path, $data);
		if( $mbox->getError() ) {
			echo $mbox->getError();
			return $mbox->getRespuesta();
		}
		else {
			$abc= $mbox->getRespuesta();
			$idFirma= $abc->result[0]->id;
			unset($abc);
		}

		/**
		* factura - generar nueva
		*/
		$path= 'factura/save';
		$conceptos= $factura["conceptos"];
		$subtotal=0;
		$impuestos=0;
		$miTazaImpuesto= 0.16;
		foreach( $conceptos as $key=>$val ) {
			$subtotal += ($val["cantidad"]*$val["pu"]); # vamos sumando para tomar el subtotal
			$impuestos += (($val["impuesto"]==1) ? (($val["cantidad"]*$val["pu"])*$miTazaImpuesto):0); # sacamos impuestos individuales
		}
		$total= ($impuestos+$subtotal); # sumamos subtotal e impuestos
		$data= array(
			"id_cliente"=>$idCliente, # identificador del cliente
			"id_firma"=>$idFirma, # identificador de la firma electronica
			"id_formula"=>$idFormula, # identificador de la formula
			"folio"=>$factura["folio"],  # folios personalizados
			"tipo"=>$factura["tipo"],
			"formato"=>"normal", # normal, arrenda, honorarios, transporte, aduana_gasto, aduana_comer
			"metodo_pago"=>$factura["metodo_pago"], # 10=Efectivo 
			"autogenfolios"=>$factura["autogenfolios"], # autogenfolios 
			"forma_pago"=>$factura["forma_pago"], # PUE=Pago Unica Exhibicion
			"fecha_vencimiento"=>$factura["fecha_vencimiento"], # vencimiento de la factura
			"condicionpago"=>$factura["condicionpago"], # condicion de pago
			"referencia"=>$factura["referencia"], # referencia
			"moneda"=>$factura["moneda"], # 1=PesosMexicanos, verificar la ayuda
			"moneda_vcambio"=>$factura["moneda_vcambio"], # el valor de cambio de moneda, si no es necesario indique 1 o Puedes omitirlo!
			"conceptos"=>$conceptos, 
			"retenciones"=>($factura["retenciones"] ? $factura["retenciones"]:0), 
			"uuid"=>($factura["uuid"] ? $factura["uuid"]:0), # uuid lista, para: Credito o Debito.
			"tipo_relacion"=>($factura["tipo_relacion"] ? $factura["tipo_relacion"]:0), # 3=devolucion de mercancias, verificar la ayuda
			"tipo_operacion"=>$factura["tipo_operacion"], 
			"tipo_factura"=>$factura["tipo_factura"], 
			"predial"=>0, # Predial del arrendador
			"subtotal"=>$factura["subtotal"], 
			"impuestos"=>number_format($factura["impuestos"], 2, '.', ''), 
			"impuestos_locales"=>$factura["impuestos_locales"], 
			"redondeos"=>$factura["redondeos"], 
			"total"=>$factura["total"], 
			"leyenda_fiscal"=>$factura["leyenda_fiscal"], 
			"descuentos"=>($factura["descuentos"] ? $factura["descuentos"]:0), # descuentos: 1=despues de impuestos, 2=antes de impuestos
			"mailsend"=>($factura["mailsend"] ? 1:0)
			); # vacio

		# print_r($factura);
		print_r($data);
	}

	# print_r($data);

	/**
	* consultando informacion del perfil
	*/
	$mbox= new moneyBox($user, $pass, $path, $data);
	if( $mbox->getError() ) {
		echo "\n\nERROR: ". $mbox->getError();
		echo "\n\n";
		print_r($mbox->getHeaderResponse());
		return $mbox->getRespuesta();
	}
	else {
		echo "\n===> Exito:";
		echo "\n\nData en Array:\n";
		echo "\n\n";
		print_r($mbox->getRespuesta());
		#echo "\n\nData en JSON:\n";
		#echo "\n\n";
		#print_r($mbox->getHeaderResponse());
		#print_r($mbox->getRespuesta("json"));
		
		return $mbox->getRespuesta();
	}
}

?>