#!/usr/bin/php
<?php
/**
* ruta donde esta el deamon de archivo o scripts cron
*/
define('CWD', "/home/angel/public_html/sdk_moneybox_php/examples/cron/");

/*
* carpeta recursos
*/
define('RESOURCES', "/home/angel/public_html/sdk_moneybox_php/resources/");

/**
* ruta al SDK moneyBox
*/
define('SDK', "/home/angel/public_html/sdk_moneybox_php/");

/**
* actualizar cliente siempre que se envie un documento electronico
*/
define('CLIENT_AUTOUPDATE', true);

/**
* rutas de I/O de documentos electronicos
*/
$dirFacturas= array(
	"factura"=>array(
		"in"=>"/home/angel/Desktop/miempresa/facturas_inxml/", 
		"out"=>"/home/angel/Desktop/miempresa/facturas/"
	), 
	"nc"=>array(
		"in"=>"/home/angel/Desktop/miempresa/notascredito_inxml/", 
		"out"=>"/home/angel/Desktop/miempresa/notascredito/"
	), 
	"nd"=>array(
		"in"=>"/home/angel/Desktop/miempresa/notasdebito_inxml/", 
		"out"=>"/home/angel/Desktop/miempresa/notasdebito/"
	), 
	"ds"=>array(
		"in"=>"/home/angel/Desktop/miempresa/documentosoporte_inxml/", 
		"out"=>"/home/angel/Desktop/miempresa/documentosoporte/"
	), 
	"nds"=>array(
		"in"=>"/home/angel/Desktop/miempresa/notadocumentosoporte_inxml/", 
		"out"=>"/home/angel/Desktop/miempresa/notadocumentosoporte/"
	)
);

$extSoportados= array(".csv", ".xls", ".xlsx", ".txt", ".mbox", ".jbox"); # formatos soportados nueva factura
$extDone= array(".error", ".xml", ".pdf", ".zip", ".html", ".md5", ".json"); # excluir, causan error o son resultados de consulta

/**
* declaracion del Template de factura JSON que se mandara al WS monyeBox
*/
# $templateFacutraJson= SDK.'resources/factura.json'; # plantilla factura JSON Oficial
$templateFacutraJson= SDK.'resources/factura_edx.json'; # plantilla factura JSON Oficial
$templateNcJson= SDK.'resources/facturaNc_edx.json'; # plantilla factura JSON Oficial
$templateNdJson= SDK.'resources/facturaNd_edx.json'; # plantilla factura JSON Oficial
$templateDsJson= SDK.'resources/facturaDs.json'; # plantilla documento soporte JSON Oficial
$templateNdsJson= SDK.'resources/facturaNds.json'; # plantilla nota ajuste documento soporte JSON Oficial

$ops=array(
	"-send"=>"\tenviar informcion para para nueva creacion", 
	"-localsearch"=>"busca informacion local", 
	"-clean"=>"\tlimpia los temporales de busqueda", 
	"-xml2mbox"=>"\tconvierte un XML a extension MBOX", 
	"-json2jbox"=>"\tconvierte un XML a extension MBOX"
);

$com=array(
	"/ruta/local/"=>"indica ruta al directorio local, solo util para -localsearch [linux,mac,solaris]", 
	"C:\\ruta\\local\\"=>"indica ruta al directorio local, solo util para -localsearch [windows]", 
	"factura"=>"\tenvia una nueva factura", 
	#"factura/update"=>"actualiza factura, nc o nd", 
	#"factura/list"=>"deuvelve lista de facturas, nc o nd", 
	#"cliente"=>"\tcrea nuevo cliente", 
	#"cliente/update"=>"actualiza informacion del cliente", 
	#"cliente/del"=>"eliminar el cliente", 
	#"firma"=>"\tcrea una firma electronica", 
	#"firma/del"=>"eliminar firma electronica", 
	#"firma/update"=>"actualiza firma electronica", 
	#"firma/list"=>"lista firma electronica", 
	#"folios"=>"\tcrea nuevos folios", 
	#"folios/del"=>"eliminar folios", 
	#"folios/list"=>"lista folios", 
	"nc"=>"\tenvia una nueva nota de credito", 
	"nd"=>"\tenvia una nueva nota de debito", 
	"ds"=>"\tenvia un nuevo documento soporte", 
	"nds"=>"\tenvia una nueva nota de ajuste documento soporte"
);

/**
* stuff functions
*/
include(SDK."src/cMoneyBox.php" );
include(CWD."phpexcel/Classes/PHPExcel.php");
include(CWD."facturaSend_TXT.php");
include(CWD."facturaSend_MBOX.php");
include(CWD."facturaSend_CSV.php");
include(CWD."facturaSend_XLS.php");
include(CWD."facturaSend_JSON.php");
include(CWD."facturaNcSend_TXT.php");
include(CWD."facturaNcSend_MBOX.php");
include(CWD."facturaNcSend_JSON.php");
include(CWD."facturaNdSend_TXT.php");
include(CWD."facturaNdSend_MBOX.php");
include(CWD."facturaNdSend_JSON.php");
include(CWD."facturaDsSend_TXT.php");
include(CWD."facturaNdsSend_TXT.php");
include(CWD."facturaDsSend_JSON.php");
include(CWD."facturaNdsSend_JSON.php");
include(CWD."facturaDsSend_MBOX.php");
include(CWD."facturaNdsSend_MBOX.php");

/**
* impresion de banner de ayuda
*/
function help() {
	echo "Usame asi:\n";
	echo "\n# ./run.php -[OPCION] [COMANDO]\n";

	echo "\nOpciones:";
	foreach( $GLOBALS['ops'] as $key=>$val ) {
		echo "\n\t". $key. "\t\t". $val;
	}

	echo "\n\nComandos (algunos...):";
	foreach( $GLOBALS['com'] as $key=>$val ) {
		echo "\n\t". $key. "\t\t". $val;
	}
}

/**
* autentificacion shell
*/
function autentificacion_cmd( $argc, $argv ) {
	if( $argc<3 ) {
		help();
	}
	else {
		if( !strstr($argv[0], "/run.php") ) # para windows quitar el "/", y dejar como "runt.php"
			help();
		else {
			$fl= false;
			foreach( $GLOBALS['ops'] as $key=>$val ) {
				if( !strcmp($key, $argv[1]) )
					$fl= true;
			}

			if( !$fl )
				help();

			if( !strcmp($argv[1], "-clean") ) { # limpia temporales de busqueda
				$fl=true;
			}
			else if( !strcmp($argv[1], "-xml2mbox") ) { # limpia temporales de busqueda
				$fl=true;
			}
			else if( !strcmp($argv[1], "-json2jbox") ) { # limpia temporales de busqueda
				$fl=true;
			}
			else if( !strcmp($argv[1], "-localsearch") ) {
				if( !strcmp($argv[2], "--auto") ) { # busqueda automatico
					$fl= true;
				}
				else
					$fl= (is_dir($argv[2]) ? true:false);
			}
			else {
				$fl= false;
				foreach( $GLOBALS['com'] as $key=>$val ) {
					if( !strcmp($key, $argv[2]) )
						$fl= true;
				}
			}

			if( !$fl ) {
				help();
			}
			else
				return 1;
		}
	}
	return 0;
}

$ban= file_get_contents("ban");

echo $ban;
echo "\n\n";

if( autentificacion_cmd( $argc, $argv ) ) {
	include( "func.php");

	$tmpSecret= getcwd()."/.tmp/";
	if( !file_exists($tmpSecret) )
		mkdir($tmpSecret);

	if( !strcmp($argv[1], "-clean") ) # limpia temporales de busqueda
		include( "clean.php" );
	else if( !strcmp($argv[1], "-xml2mbox") ) # limpia temporales de busqueda
		include( "xml2mbox.php" );
	else if( !strcmp($argv[1], "-json2jbox") ) # limpia temporales de busqueda
		include( "json2jbox.php" );
	else if( !strcmp($argv[1], "-localsearch") ) # busqueda local
		include( "localsearch.php" );
	else
		include( "send.php");
}

echo "\n\nFin del Programa...";
echo "\n\n";
?>