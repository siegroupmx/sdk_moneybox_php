<?php
/**
* envio de factura en formato TXT
*/
function sendFacturaNd_TXT_byConfig($fileName=false, $configFile=NULL) {
	if( !count($configFile) ) 	return 0;
	else {
		if( !file_exists(RESOURCES.$configFile["ConfigData"]) ) # configuracion del Mapeo
			return 0;
		else if( !file_exists(RESOURCES.$configFile["Template"]) ) # factura JSON que se manda a moneyBox
			return 0;
		else if( strcmp($configFile["InputFile"], "txt") ) # distinto de TXT la entrada de la "factura original"
			return 0;
		else if( strcmp($configFile["DataFile"], "xml") ) # contenido del Mapeo distinto a XML
			return 0;
		else {
			# print_r($configFile);
			# echo "\nBuffer de Lectura: ". $configFile["InputFile"];
			# echo "\nData Content: ". $configFile["DataFile"];
			# echo "\nConfiguracion: ". $configFile["ConfigData"];
			# echo "\nTemplate: ". RESOURCES.$configFile["Template"];

			$configXml= simplexml_load_file(RESOURCES.$configFile["ConfigData"]);
			$fileContent= file_get_contents($fileName);

			if( !count($configXml->Sections) ) 		return 0;
			else {
				$fileEnd= array();
				$fileAdiciones= array();
				foreach( $configXml->Sections->Section as $key=>$val ) { # leyendo configuracion
					$tmpData=array();

					/**
					* leyendo los atributos de cada elementos hijo <Section>
					*/
					foreach( $val->attributes() as $atKey=>$atVal ) {
						$tmpData[$atKey]= fixTextPrev($atVal);
					}

					/**
					* extraemos datos en base a InitText (dato inicio) y EndText (dato final), tomado de $fileContent y colocado en $out
					* extraido de cada elemento hijo <Section>
					*/
					preg_match_all('/'. $tmpData["InitText"]. '(.*?)'. $tmpData["EndText"]. '/is', $fileContent, $out);
					$crudeData= ($out[0][0] ? $out[1][0]:0); # datos obtenidos

					$aux=array(); # auxiliar
					foreach( $val->Element as $key2=>$val2 ) { # recorremos los Element
						$spTmp=array();
						if( count($val2->Translations) ) { # si existe Translations
							foreach( $val2->Translations->Translation as $atkey3=>$atval3 ) { # recorremos nodos hijos Translation
								$tmp=array();
								foreach( $atval3->attributes() as $atKey=>$atVal ) { # miramos atributos de Translation
									$tmp[$atKey]= fixTextPrev($atVal); # guardamo los atributos
								}

								/**
								* de los atributos encontraos en <Translation> se solo extraemos ciertos argumentos
								*/
								$spTmp[$tmp["Type"]]= array(
									"from"=>$tmp["Source"], 
									"to"=>$tmp["Target"], 
									"spaces"=>(isset($tmp["Spaces"]) ? $tmp["Spaces"]:0)
								);
								unset($tmp);
							}
						}

						/**
						* continuamos con los <Element>
						*/
						$tmp=array();
						foreach( $val2->attributes() as $atKey=>$atVal ) { # recorremos atributos de <Element>
							$tmp[$atKey]= fixTextPrev($atVal); # guardamos todos los atributos de <Element>
						}

						/**
						* armamos el array auxiliar tomando como base el argumento "TagUBL" declarado en cada <Element>
						* y armando el contenido con elementos de interes como: RowNumber, ColumnNumber, Tag, Length y TagUBL
						* y en "sp" guardamos los argumento encontrados en <Translation> como parte de las ordenes del
						* <Element> especifico
						*/
						#$aux[$tmp["Tag"]]= array( 
						$aux[$tmp["TagUBL"]]= array( 
							"sizeRead"=>$tmp["Length"], 
							"rows"=>($tmp["RowNumber"] ? $tmp["RowNumber"]:0), 
							"startRead"=>$tmp["ColumnNumber"], 
							"onUBL"=>$tmp["TagUBL"], 
							"tag"=>$tmp["Tag"], 
							"sp"=>(count($spTmp) ? $spTmp:0)
						);
						unset($tmp, $spTmp);
					}

					/**
					* armamos arreglo principal que practicamente "transcibe" el XML Templae, a un Array ordenado
					* Colocando como elementos principales el rango de lectura "readFrom" y "readTo", los nodos encontrados <Element> en "nodes"
					* y los datos en crudo leidos del segmento Spool que se guarda en "data"
					* Todo se conjuga en este arreglo fileEnd
					*/
					$fileEnd[]= array(
						"type"=>$tmpData["Type"], 
						"name"=>$tmpData["Name"], 
						"readFrom"=>$tmpData["InitText"], 
						"readTo"=>$tmpData["EndText"], 
						"nodes"=>$aux, 
						"data"=>$crudeData
					);
					unset($aux, $sp, $out);
				}

				# print_r($fileEnd);

				if( !count($fileEnd) ) 	return 0;
				else {
					$factura= array();
					$recurData=0;
					$impCont=0;

					foreach($fileEnd as $key=>$val ) {
						$crudeData= $val["data"]; # los datos en crudo extraidos del spool
						$stripCrude= explode("\n", $crudeData); # dividimos los datos en crudo del spool

						if( !strcmp($val["type"], "Recursive") )  { # busqueda en recursivo - conceptos y uuids
							/**
							* en el modo Recursivo, la busqueda no esperara la aparicion de la variable declarada en la configuracion XML
							* solo indicara las posiciones que correspondan a los Valores asociados a las variables declaradas en la confi
							* guracion del XML
							* Siempre se omitira la primer linea ya que corresponde a la declaracion de variables con satisfaccion Visual
							*/
							# print_r($crudeData);
							# print_r($val);
							$i=(!strcmp(strtolower($val["name"]), "eximpuestos") ? 1:0);

							foreach( $stripCrude as $crudeX=>$crudeY ) {
								if( substr($crudeY, 0, -1) ) {
									if( $i ) {
										$conceptos=array();
										$conceptosAdit=array();
										$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);
										# echo "\n[". $i. "]--->|". fixTextPrev(substr($crudeY, 0, -1)). "|";

										foreach( $val["nodes"] as $key2=>$val2 ) {
											$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);

											if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
												foreach( $val2["sp"] as $com=>$exec ) {
													if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
														$aux= $exec["from"];
													}
													else if( !strcmp(strtolower($com), "replace") ) { # reemplaso
														$aux= str_replace($exec["from"], $exec["to"], $aux);
													}
													else if( !strcmp(strtolower($com), "format") ) { # aplicar formato
														if( !strcmp(strtolower($exec["to"]), "numeric") ) {
															$aux= str_replace(",", "", $aux);
															$aux= number_format($aux, 6, '.', '');
														}
													}
													else if( !strcmp(strtolower($com), "setstring") ) { # colocar cadena
														$aux .= $exec["from"];
													}
													else if( !strcmp(strtolower($com), "concat") ) { # concatenar
														$x= (strstr($exec["from"], ",") ? explode(",", $exec["from"]):$exec["from"]);

														if( !is_array($x) ) # simple
															$aux .= $fileAdiciones[$x]; # tomamos nodo y concatenamos
														else { # si es array
															foreach( $x as $nodX ) {
																if( strstr($nodX, "[str:") ) { # si contiene comando para string de datos por entrada personalizada
																	$spStr= substr($nodX, 5, -1);
																	$aux .= $spStr; # concatenamos especial
																	unset($spStr);
																}
																else
																	$aux .= $fileAdiciones[$nodX]; # tomamos nodo y concatenamos
															}
														}
													}
													else if( !strcmp(strtolower($com), "div") ) { # aplicar formato
														$x= $conceptosAdit[$exec["from"]];
														$aux= number_format( ($aux/$x), 10, '.', '');
														unset($x);
													}

													if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
														if( !strcmp($exec["spaces"], "kill") ) { # quitar
															$aux= str_replace(" ", "", $aux);
														}
														if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
															$aux= reduceSpaces($aux);
														}
													}
												}
											}

											$conceptos[$key2]= fixTextPrev($aux);
											$conceptosAdit[$val2["tag"]]= fixTextPrev($aux);
											# $factura[$key2]= fixTextPrev($aux);
											unset($aux);
										}

										if( !strcmp(strtolower($val["name"]), "eximpdetail") ) { # extra impuestos anidados en el mismo concepto
											$j=0;
											$aux=array();
											$extrasImpConceptos=array();
											foreach( $conceptos as $exImpDetKey=>$exImpDetVal ) {
												$aux[]= $exImpDetVal;

												if( $j==4 ) {
													$extrasImpConceptos[]= array(
														"nombre"=>$aux[4], 
														"tipo"=>$aux[3], 
														"tasa"=>$aux[2], 
														"importe"=>$aux[1], # base
														"impuesto"=>$aux[0]
													);
													unset($aux);
													$j=0;
												}
												else 
													$j++;
											}

											# copiamos a array estandar
											$factura["conceptos"][($i-1)]["extra_impuestos"]= $extrasImpConceptos;
											$fileAdiciones["conceptos"][($i-1)]["extra_impuestos"]= $extrasImpConceptos;
											unset($auxExtraImp, $j, $extrasImpConceptos, $auxExtraImp);

										}
										else if( !strcmp(strtolower($val["name"]), "remesas") ) { # extra impuestos anidados en el mismo concepto
											$j=0;
											$aux=array();
											$remesaConceptos=array();
											foreach( $conceptos as $remKey=>$remVal ) {
												$aux[]= $remVal;

												if( $j==4 ) {
													$remesaConceptos[]= array(
														"servicio"=>$aux[0], // tipo de traslado
														"cantidad"=>$aux[1], // cantidad
														"unidad"=>$aux[2], // unidad de medida
														"codigo"=>$aux[3], // codigo de tipo de remesa
														"valor"=>$aux[4] // valor de remesa
													);
													unset($aux);
													$j=0;
												}
												else 
													$j++;
											}

											# copiamos a array estandar
											$factura["conceptos"][($i-1)]["remesas"]= $remesaConceptos;
											$fileAdiciones["conceptos"][($i-1)]["remesas"]= $remesaConceptos;
											unset($auxExtraImp, $j, $remesaConceptos, $auxExtraImp, $aux);
										}
										else if( !strcmp(strtolower($val["name"]), "exredodetail") ) { # extra redondeos anidados en el mismo concepto
											$j=0;
											$aux=array();
											$extrasImpConceptos=array();
											foreach( $conceptos as $exImpDetKey=>$exImpDetVal ) {
												$aux[]= $exImpDetVal;

												if( $j==1 ) {
													$extrasImpConceptos[]= array( 
														"clave"=>$aux[0], 
														"importe"=>$aux[1]
													);

													unset($aux);
													$j=0;
												}
												else 
													$j++;
											}

											# copiamos a array estandar
											$factura["conceptos"][($i-1)]["extra_redondeos"]= $extrasImpConceptos;
											$fileAdiciones["conceptos"][($i-1)]["extra_redondeos"]= $extrasImpConceptos;
											unset($auxExtraImp, $j, $extrasImpConceptos, $auxExtraImp, $aux);

										}
										else if( !strcmp(strtolower($val["name"]), "eximpuestos") ) {
											$factura["impuestos_locales"][]= $conceptos;
											$fileAdiciones["impuestos_locales"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "docsmercenv") ) {
											$factura["documentos_envio"][]= $conceptos;
											$fileAdiciones["documentos_envio"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "docsmercrec") ) {
											$factura["documentos_recepcion"][]= $conceptos;
											$fileAdiciones["documentos_recepcion"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "anticipos") ) {
											$factura["anticipos"][]= $conceptos;
											$fileAdiciones["anticipos"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "descglobales") ) {
											$factura["descuentos_globales"][]= $conceptos;
											$fileAdiciones["descuentos_globales"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "locredo") ) {
											$factura["redondeos"][]= $conceptos;
											$fileAdiciones["redondeos"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "partes") ) {
											$factura["participantes"][]= $conceptos;
											$fileAdiciones["participantes"][]= $conceptosAdit;
										}
										else if( $recurData==0 ) { # conceptos
											$factura["conceptos"][]= $conceptos;
											$fileAdiciones["conceptos"][]= $conceptosAdit;
										}
										else if( $recurData==1 ) { # uuids
											$factura["uuid"][]= $conceptos;
											$fileAdiciones["uuid"][]= $conceptosAdit;
										}
										else if( !strcmp(strtolower($val["name"]), "uuidrel") ) {
											$factura["uuid"][]= $conceptos;
											$fileAdiciones["uuid"][]= $conceptosAdit;
										}
										unset($conceptos, $conceptosAdit);
									}
									$i++;
								}
							}
							$recurData++;
						}
						else if( !strcmp($val["type"], "Single") )  { # busqueda y armado regular
							/**
							* en el modo Single, debera encontrarse en la linea la invovacion de la variable y sera el accionados para 
							* buscar en la posicion indicada en la configuracion del XML el valor resultante a la variable
							*/
							foreach( $val["nodes"] as $key2=>$val2 ) {
								$aux='';
								foreach( $stripCrude as $crudeX=>$crudeY ) {
									if( strstr($crudeY, $val2["tag"]) && !$aux ) { # nodo a buscar
										$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);
									}
								}

								if( !strcmp(strtolower($val["name"]), "eximpuestos") ) { # impuestos_locales
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["impuestos_locales"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["impuestos_locales"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else if( !strcmp(strtolower($val["name"]), "docsmercenv") ) { # documentos mercantiles de envio
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["documentos_envio"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["documentos_envio"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else if( !strcmp(strtolower($val["name"]), "docsmercrec") ) { # documentos mercantiles de recepcion
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["documentos_recepcion"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["documentos_recepcion"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else if( !strcmp(strtolower($val["name"]), "anticipos") ) { # anticipos
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["anticipos"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["anticipos"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else if( !strcmp(strtolower($val["name"]), "descglobales") ) { # descuentos globales
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["descuentos_globales"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["descuentos_globales"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else if( !strcmp(strtolower($val["name"]), "locredo") ) { # redondeos
									if( count($val2["sp"]) ) {
										foreach( $val2["sp"] as $com=>$exec ) {
											if( !strcmp(strtolower($com), "condicion") ) { # condicion para reemplazo
												$aux= str_replace(" ", "", $aux); # limpiando espacios
												$condOut='';
												$patron='/([a-zA-Z0-9\.\{\}\!=]{1,})/is';
												preg_match_all($patron, $exec["from"], $condOut);
												$auxCond=NULL;
												$spVar=false;

												foreach( $condOut[0] as $keyCond=>$valCond ) {
													if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
														$patron2='/\{([a-zA-Z0-9]{1,})\}/is';
														preg_match_all($patron2, $exec["from"], $condOut2);
														$condStatus= substr($condOut[1][$keyCond], 0, 1); # puede ser { o !

														if( $condOut2[0][0] && (!strcmp($condStatus, "{") || !strcmp($condStatus, "!"))) {
															foreach( $val["nodes"] as $keyCond2=>$valCond2 ) {
																if( !strcmp($valCond2["tag"], $condOut2[1][0]) && !$spVar ) {
																	$auxTmp= substr($crudeY, ($valCond2["startRead"]-1), $valCond2["sizeRead"]);
																	$auxTmp= str_replace(" ", "", $auxTmp); # limpiando espacios
																	$auxTmp= number_format($auxTmp, 0, '', '');
																	$spVar= (((!strcmp($condStatus, "{") && $auxTmp) || (!strcmp($condStatus, "!") && !$auxTmp)) ? true:false);
																}
															}
														}
													}

													if( !$auxCond ) {
														if( strstr($condOut[1][$keyCond], "{") && strstr($condOut[1][$keyCond], "}") ) { # condicion de variable
															$auxCond= ($spVar ? $exec["to"]:NULL);
														}
														else
															$auxCond= (!strcmp($condOut[1][$keyCond], $aux) ? $exec["to"]:NULL);
													}
												}

												if( $auxCond ) # si hizo match
													$aux= $auxCond; # sustituimos
												unset($auxCond, $condOut);
											}

											if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
												if( !strcmp($exec["spaces"], "kill") ) { # quitar
													$aux= str_replace(" ", "", $aux);
												}
												if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
													$aux= reduceSpaces($aux);
												}
											}
										}
									}

									$factura["redondeos"][$impCont][$key2]= fixTextPrev($aux);
									$fileAdiciones["redondeos"][$impCont][$val2["tag"]]= fixTextPrev($aux);
								}
								else { # normal, los
									$factura[$key2]= fixTextPrev($aux);
									$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);
								}

								#$aux='';
								if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
									foreach( $val2["sp"] as $com=>$exec ) {
										if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
											$aux= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "replace") ) { # reemplaso
											$aux= str_replace($exec["from"], $exec["to"], $factura[$key2]);
										}
										else if( !strcmp(strtolower($com), "format") ) { # aplicar formato
											if( !strcmp(strtolower($exec["to"]), "numeric") ) {
												$factura[$key2]= str_replace(",", "", $factura[$key2]);
												$aux= number_format($factura[$key2], 6, '.', '');
											}
										}
										else if( !strcmp(strtolower($com), "setstring") ) { # colocar cadena
											$aux .= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "concat") ) { # concatenar
											$x= (strstr($exec["from"], ",") ? explode(",", $exec["from"]):$exec["from"]);

											if( !is_array($x) ) # simple
												$aux .= $fileAdiciones[$x]; # tomamos nodo y concatenamos
											else { # si es array
												foreach( $x as $nodX ) {
													if( strstr($nodX, "[str:") ) { # si contiene comando para string de datos por entrada personalizada
														$spStr= substr($nodX, 5, -1);
														$aux .= $spStr; # concatenamos especial
														unset($spStr);
													}
													else
														$aux .= $fileAdiciones[$nodX]; # tomamos nodo y concatenamos
												}
											}
										}

										if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
											if( !strcmp($exec["spaces"], "kill") ) { # quitar
												$aux= str_replace(" ", "", $aux);
											}
											if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
												$aux= reduceSpaces($aux);
											}
										}
									}
								#if( $aux ) { # si hubo ejecucion
									$factura[$key2]= fixTextPrev($aux);
									$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);
								#	}
								}
							}
						}
						$impCont++;
					}
				}

				if( is_array($factura) && count($factura) ) {
					# print_r($fileAdiciones);
					$template= file_get_contents($GLOBALS['templateNdJson']);

					if( !$template )
						echo "-[No existe Template..]-";
					else {
						$fecha=time();
						$newname= SDK.'tmp/'.$fecha; # temporal donde generamos JSON final
						$file= fopen($newname, "w");
						$facturaJson= json_decode($template, true);
						$tmpConceptos= $facturaJson["conceptos"]; # elementos declarados en el JSON
						$facturaJson["conceptos"]= array(); # limpiamos

						foreach( $facturaJson as $key=>$val ) { # colocando datos del emisor
							if( strcmp($key, "conceptos") && strcmp($key, "uuid") && strcmp($key, "impuestos_locales") && strcmp($key, "redondeos") && strcmp($key, "participantes") && strcmp($key, "documentos_envio") && strcmp($key, "documentos_recepcion") && strcmp($key, "anticipos") && strcmp($key, "descuentos_globales") ) {
								if( is_array($val) && count($val) ) {
									foreach( $val as $key2=>$val2 )
										$facturaJson[$key][$key2]= fixTextPrev($factura[$val2] ? $factura[$val2]:0); # establecemos el valor
								}
								else
									$facturaJson[$key]= fixTextPrev($factura[$val] ? $factura[$val]:0); # establecemos el valor
							}
						}

						$conceptos=array();
						foreach( $factura["conceptos"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
							}

							if( count($factura["conceptos"][$key]["extra_impuestos"]) ) # impuestos extras
								$aux["extra_impuestos"]= $factura["conceptos"][$key]["extra_impuestos"];
							if( count($factura["conceptos"][$key]["remesas"]) ) # remesas
								$aux["remesas"]= $factura["conceptos"][$key]["remesas"];
							if( count($factura["conceptos"][$key]["extra_redondeos"]) ) # redondeos extras
								$aux["extra_redondeos"]= $factura["conceptos"][$key]["extra_redondeos"];
								
							$conceptos[$key]= $aux;
							unset($aux);
						}

						$facturaJson["conceptos"]= $conceptos;

						# insercion de uuid's
						$tmpConceptos= $facturaJson["uuid"]; # elementos declarados en el JSON
						$facturaJson["uuid"]= array(); # limpiamos
						$conceptos=array();
						foreach( $factura["uuid"] as $key=>$val ) { # recorriendo factura real
							$aux=array();
							# print_r($tmpConceptos);

							if( is_array($tmpConceptos) && count($tmpConceptos) ) {
								foreach( $tmpConceptos[0] as $key2=>$val2 ) {
									# echo "\n\n--> ". $aux[$key2]. " ::: ". fixTextPrev($val[$val2] ? $val[$val2]:0). "\n";
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								}
								$conceptos[$key]= $aux;
								unset($aux);
							}
						}
						$facturaJson["uuid"]= $conceptos;

						/**
						* participantes
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["participantes"]; # elementos declarados en el JSON
						$facturaJson["participantes"]= array();
						$i=0;
						foreach( $factura["participantes"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["participantes"]= $conceptos;

						/**
						* impuestos_locales
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["impuestos_locales"]; # elementos declarados en el JSON
						$facturaJson["impuestos_locales"]= array();
						foreach( $factura["impuestos_locales"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "tasa") )
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$key]= $aux;
							unset($aux);
						}
						$facturaJson["impuestos_locales"]= $conceptos;

						/**
						* redondeos
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["redondeos"]; # elementos declarados en el JSON
						$facturaJson["redondeos"]= array();
						$i=0;
						foreach( $factura["redondeos"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "importe") ) 
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["redondeos"]= $conceptos;

						/**
						* participantes
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["participantes"]; # elementos declarados en el JSON
						$facturaJson["participantes"]= array();
						$i=0;
						foreach( $factura["participantes"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["participantes"]= $conceptos;

						/**
						* mercantiles de envio
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["documentos_envio"]; # elementos declarados en el JSON
						$facturaJson["documentos_envio"]= array();
						$i=0;
						foreach( $factura["documentos_envio"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "importe") ) 
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["documentos_envio"]= $conceptos;

						/**
						* mercantiles de recepcion
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["documentos_recepcion"]; # elementos declarados en el JSON
						$facturaJson["documentos_recepcion"]= array();
						$i=0;
						foreach( $factura["documentos_recepcion"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "importe") ) 
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["documentos_recepcion"]= $conceptos;

						/**
						* anticipos
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["anticipos"]; # elementos declarados en el JSON
						$facturaJson["anticipos"]= array();
						$i=0;
						foreach( $factura["anticipos"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "importe") ) 
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["anticipos"]= $conceptos;

						/**
						* descuentos globales
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["descuentos_globales"]; # elementos declarados en el JSON
						$facturaJson["descuentos_globales"]= array();
						$i=0;
						foreach( $factura["descuentos_globales"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "importe") ) 
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["descuentos_globales"]= $conceptos;

						# adicionales extras
						$aux=array();
						foreach( $fileAdiciones as $key=>$val ) {
							if( strcmp($key, "uuid") ) {
								$aux[$key]= $val;
							}
						}
						$facturaJson["extra"]= $aux;
						unset($aux);

						$hashMd5= md5(json_encode($facturaJson));
						$facturaJson["huella"]= $hashMd5;
						$outArray= print_r($facturaJson, true);
						$outJson= json_encode($facturaJson);

						fputs($file, $outJson, strlen($outJson));
						# fputs($file, $outArray, strlen($outJson));
						fclose($file);
						unset($file);
					}
					$facturaJson["tipo"]= "debito";
					$facturaJson["formato"]= "normal";
					$facturaJson["access"]= "partner";
					$facturaJson["levelService"]= "L1"; # L1=XML, L2=CUFE, L3=FIRMA, L4=DIAN

					# print_r($facturaJson);

					if( count($facturaJson["conceptos"]) ) { # si hay elementos
						$r= sendToMoneyBox($facturaJson, true);
					}
					# print_r($r);
				}
			}
		return $r;
		}
	}
}

/**
* envio de factura en formato TXT
*/
function sendFacturaNd_TXT($fileName=false) {
	echo "\n---> Enviando TXT";
}
?>