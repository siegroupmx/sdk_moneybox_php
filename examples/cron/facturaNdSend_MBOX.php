<?php
/**
* envio de factura en formato MBOX
*/
function sendFacturaNd_MBOX_byConfig($fileName=false, $configFile=NULL) {
	if( !count($configFile) ) 	return 0;
	else {
		if( !file_exists(RESOURCES.$configFile["ConfigData"]) ) # configuracion del Mapeo
			return 0;
		else if( !file_exists(RESOURCES.$configFile["Template"]) ) # factura JSON que se manda a moneyBox
			return 0;
		else if( strcmp($configFile["InputFile"], "mbox") ) # distinto de MBOX la entrada de la "factura original"
			return 0;
		else if( strcmp($configFile["DataFile"], "xml") ) # contenido del Mapeo distinto a XML
			return 0;
		else {
			# print_r($configFile);
			# echo "\nBuffer de Lectura: ". $configFile["InputFile"];
			# echo "\nData Content: ". $configFile["DataFile"];
			# echo "\nConfiguracion: ". $configFile["ConfigData"];
			# echo "\nTemplate: ". RESOURCES.$configFile["Template"];

			$configXml= simplexml_load_file(RESOURCES.$configFile["ConfigData"]);
			$fileContent= file_get_contents($fileName);

			if( !count($configXml->Sections) ) 		return 0;
			else {
				$fileEnd= array();
				$fileAdiciones= array();
				foreach( $configXml->Sections->Section as $key=>$val ) { # leyendo configuracion
					$tmpData=array();

					/**
					* leyendo los atributos de cada elementos hijo <Section>
					*/
					foreach( $val->attributes() as $atKey=>$atVal ) {
						$tmpData[$atKey]= fixTextPrev($atVal);
					}

					/**
					* extraemos datos en base a InitText (dato inicio) y EndText (dato final), tomado de $fileContent y colocado en $out
					* extraido de cada elemento hijo <Section>
					*/
					preg_match_all('/'. $tmpData["InitText"]. '(.*?)'. $tmpData["EndText"]. '/is', $fileContent, $out);
					$crudeData= ($out[0][0] ? $out[1][0]:0); # datos obtenidos

					$aux=array(); # auxiliar
					foreach( $val->Element as $key2=>$val2 ) { # recorremos los Element
						$spTmp=array();
						if( count($val2->Translations) ) { # si existe Translations
							foreach( $val2->Translations->Translation as $atkey3=>$atval3 ) { # recorremos nodos hijos Translation
								$tmp=array();
								foreach( $atval3->attributes() as $atKey=>$atVal ) { # miramos atributos de Translation
									$tmp[$atKey]= fixTextPrev($atVal); # guardamo los atributos
								}

								/**
								* de los atributos encontraos en <Translation> se solo extraemos ciertos argumentos
								*/
								$spTmp[$tmp["Type"]]= array(
									"from"=>$tmp["Source"], 
									"to"=>$tmp["Target"], 
									"spaces"=>(isset($tmp["Spaces"]) ? $tmp["Spaces"]:0)
								);
								unset($tmp);
							}
						}

						/**
						* continuamos con los <Element>
						*/
						$tmp=array();
						foreach( $val2->attributes() as $atKey=>$atVal ) { # recorremos atributos de <Element>
							$tmp[$atKey]= fixTextPrev($atVal); # guardamos todos los atributos de <Element>
						}

						/**
						* armamos el array auxiliar tomando como base el argumento "TagUBL" declarado en cada <Element>
						* y armando el contenido con elementos de interes como: RowNumber, ColumnNumber, Tag, Length y TagUBL
						* y en "sp" guardamos los argumento encontrados en <Translation> como parte de las ordenes del
						* <Element> especifico
						*/
						#$aux[$tmp["Tag"]]= array( 
						$aux[$tmp["TagUBL"]]= array( 
							"sizeRead"=>$tmp["Length"], 
							"rows"=>($tmp["RowNumber"] ? $tmp["RowNumber"]:0), 
							"startRead"=>$tmp["ColumnNumber"], 
							"onUBL"=>$tmp["TagUBL"], 
							"tag"=>$tmp["Tag"], 
							"sp"=>(count($spTmp) ? $spTmp:0)
						);
						unset($tmp, $spTmp);
					}

					/**
					* armamos arreglo principal que practicamente "transcibe" el XML Templae, a un Array ordenado
					* Colocando como elementos principales el rango de lectura "readFrom" y "readTo", los nodos encontrados <Element> en "nodes"
					* y los datos en crudo leidos del segmento Spool que se guarda en "data"
					* Todo se conjuga en este arreglo fileEnd
					*/
					$fileEnd[]= array(
						"type"=>$tmpData["Type"], 
						"name"=>$tmpData["Name"], 
						"readFrom"=>$tmpData["InitText"], 
						"readTo"=>$tmpData["EndText"], 
						"nodes"=>$aux, 
						"data"=>$crudeData
					);
					unset($aux, $sp, $out);
				}

				# print_r($fileEnd);

				if( !count($fileEnd) ) 	return 0;
				else {
					$factura= array();
					$recurData=0;

					foreach($fileEnd as $key=>$val ) {
						$crudeData= $val["data"]; # los datos en crudo extraidos del spool
						$stripCrude= explode("\n", $crudeData); # dividimos los datos en crudo del spool

						if( !strcmp($val["type"], "Recursive") )  { # busqueda en recursivo - conceptos y uuids
							/**
							* en el modo Recursivo, la busqueda no esperara la aparicion de la variable declarada en la configuracion XML
							* solo indicara las posiciones que correspondan a los Valores asociados a las variables declaradas en la confi
							* guracion del XML
							* Siempre se omitira la primer linea ya que corresponde a la declaracion de variables con satisfaccion Visual
							*/
							# print_r($crudeData);
							# print_r($val);
							$i=0;

							foreach( $stripCrude as $crudeX=>$crudeY ) {
								if( substr($crudeY, 0, -1) ) {
									if( $i ) {
										$conceptos=array();
										$conceptosAdit=array();
										$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);
										# echo "\n[". $i. "]--->|". fixTextPrev(substr($crudeY, 0, -1)). "|";

										foreach( $val["nodes"] as $key2=>$val2 ) {
											$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);

											if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
												foreach( $val2["sp"] as $com=>$exec ) {
													if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
														$aux= $exec["from"];
													}
													else if( !strcmp(strtolower($com), "replace") ) { # reemplaso
														$aux= str_replace($exec["from"], $exec["to"], $aux);
													}
													else if( !strcmp(strtolower($com), "format") ) { # aplicar formato
														if( !strcmp(strtolower($exec["to"]), "numeric") ) {
															$aux= str_replace(",", "", $aux);
															$aux= number_format($aux, 2, '.', '');
														}
													}
													else if( !strcmp(strtolower($com), "setstring") ) { # colocar cadena
														$aux .= $exec["from"];
													}
													else if( !strcmp(strtolower($com), "concat") ) { # concatenar
														$x= (strstr($exec["from"], ",") ? explode(",", $exec["from"]):$exec["from"]);
													}

													if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
														if( !strcmp($exec["spaces"], "kill") ) { # quitar
															$aux= str_replace(" ", "", $aux);
														}
														if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
															$aux= reduceSpaces($aux);
														}
													}
												}
											}

											$conceptos[$key2]= $aux;
											$conceptosAdit[$val2["tag"]]= $aux;
											# $factura[$key2]= fixTextPrev($aux);
											unset($aux);
										}

										if( $recurData==0 ) { # conceptos
											$factura["conceptos"][]= $conceptos;
											$fileAdiciones["conceptos"][]= $conceptosAdit;
										}
										else if( $recurData==1 ) { # uuids
											$factura["uuid"][]= $conceptos;
											$fileAdiciones["uuid"][]= $conceptosAdit;
										}
										unset($conceptos, $conceptosAdit);
									}
									$i++;
								}
							}
							$recurData++;
						}
						else if( !strcmp($val["type"], "Single") )  { # busqueda y armado regular
							/**
							* en el modo Single, debera encontrarse en la linea la invovacion de la variable y sera el accionados para 
							* buscar en la posicion indicada en la configuracion del XML el valor resultante a la variable
							*/
							foreach( $val["nodes"] as $key2=>$val2 ) {
								$aux='';
								foreach( $stripCrude as $crudeX=>$crudeY ) {
									if( strstr($crudeY, $val2["tag"]) && !$aux ) { # nodo a buscar
										$aux= substr($crudeY, ($val2["startRead"]-1), $val2["sizeRead"]);
									}
								}
								$factura[$key2]= fixTextPrev($aux);
								$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);

								$aux='';
								if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
									foreach( $val2["sp"] as $com=>$exec ) {
										if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
											$aux= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "replace") ) { # reemplaso
											$aux= str_replace($exec["from"], $exec["to"], $factura[$key2]);
										}
										else if( !strcmp(strtolower($com), "format") ) { # aplicar formato
											if( !strcmp(strtolower($exec["to"]), "numeric") ) {
												$factura[$key2]= str_replace(",", "", $factura[$key2]);
												$aux= number_format($factura[$key2], 2, '.', '');
											}
										}
										else if( !strcmp(strtolower($com), "setstring") ) { # colocar cadena
											$aux .= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "concat") ) { # concatenar
											$x= (strstr($exec["from"], ",") ? explode(",", $exec["from"]):$exec["from"]);

											if( !is_array($x) ) # simple
												$aux .= $fileAdiciones[$x]; # tomamos nodo y concatenamos
											else { # si es array
												foreach( $x as $nodX ) {
													if( strstr($nodX, "[str:") ) { # si contiene comando para string de datos por entrada personalizada
														$spStr= substr($nodX, 5, -1);
														$aux .= $spStr; # concatenamos especial
														unset($spStr);
													}
													else
														$aux .= $fileAdiciones[$nodX]; # tomamos nodo y concatenamos
												}
											}
										}

										if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
											if( !strcmp($exec["spaces"], "kill") ) { # quitar
												$aux= str_replace(" ", "", $aux);
											}
											if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
												$aux= reduceSpaces($aux);
											}
										}
									}
								if( $aux ) { # si hubo ejecucion
									$factura[$key2]= fixTextPrev($aux);
									$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);
									}
								}
							}
						}
					}
				}

				if( is_array($factura) && count($factura) ) {
					# print_r($fileAdiciones);
					$template= file_get_contents($GLOBALS['templateNdJson']);

					if( !$template )
						echo "-[No existe Template..]-";
					else {
						$fecha=time();
						$newname= SDK.'tmp/'.$fecha; # temporal donde generamos JSON final
						$file= fopen($newname, "w");
						$facturaJson= json_decode($template, true);
						$tmpConceptos= $facturaJson["conceptos"]; # elementos declarados en el JSON
						$facturaJson["conceptos"]= array(); # limpiamos

						foreach( $facturaJson as $key=>$val ) { # colocando datos del emisor
							if( strcmp($key, "conceptos") && strcmp($key, "uuid") ) {
								if( is_array($val) && count($val) ) {
									foreach( $val as $key2=>$val2 )
										$facturaJson[$key][$key2]= fixTextPrev($factura[$val2] ? $factura[$val2]:0); # establecemos el valor
								}
								else
									$facturaJson[$key]= fixTextPrev($factura[$val] ? $factura[$val]:0); # establecemos el valor
							}
						}

						$conceptos=array();
						foreach( $factura["conceptos"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
							}
							$conceptos[$key]= $aux;
							unset($aux);
						}

						$facturaJson["conceptos"]= $conceptos;

						# insercion de uuid's
						$tmpConceptos= $facturaJson["uuid"]; # elementos declarados en el JSON
						$facturaJson["uuid"]= array(); # limpiamos
						$conceptos=array();
						foreach( $factura["uuid"] as $key=>$val ) { # recorriendo factura real
							$aux=array();
							# print_r($tmpConceptos);

							if( is_array($tmpConceptos) && count($tmpConceptos) ) {
								foreach( $tmpConceptos[0] as $key2=>$val2 ) {
									$aux[$key2]= (isset($val[$val2]) ? fixTextPrev($val[$val2] ? $val[$val2]:0):0);
									#$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								}
								$conceptos[$key]= $aux;
								unset($aux);
							}
						}

						$facturaJson["uuid"]= $conceptos;

						# adicionales extras
						$aux=array();
						foreach( $fileAdiciones as $key=>$val ) {
							if( strcmp($key, "uuid") ) {
								$aux[$key]= $val;
							}
						}
						$facturaJson["extra"]= $aux;
						unset($aux);

						$hashMd5= md5(json_encode($facturaJson));
						$facturaJson["huella"]= $hashMd5;
						$outArray= print_r($facturaJson, true);
						$outJson= json_encode($facturaJson);

						fputs($file, $outJson, strlen($outJson));
						# fputs($file, $outArray, strlen($outJson));
						fclose($file);
						unset($file);
					}
					$facturaJson["tipo"]= "debito";
					$facturaJson["formato"]= "normal";

					# print_r($facturaJson);

					if( count($facturaJson["conceptos"]) ) { # si hay elementos
						$r= sendToMoneyBox($facturaJson);
					}
					# print_r($r);
				}
			}
		return $r;
		}
	}
}

/**
* envio de factura en formato MBOX
*/
function sendFacturaNd_MBOX($fileName=false) {
	echo "\n---> Enviando TXT";
}
?>