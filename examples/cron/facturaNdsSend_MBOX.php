<?php
/**
* envio de factura en formato MBOX
*/
function sendFacturaNds_MBOX_byConfig($fileName=false, $configFile=NULL) {
	if( !count($configFile) ) 	return 0;
	else {
		if( !file_exists(RESOURCES.$configFile["ConfigData"]) ) # configuracion del Mapeo
			return 0;
		else if( !file_exists(RESOURCES.$configFile["Template"]) ) # factura JSON que se manda a moneyBox
			return 0;
		else if( strcmp($configFile["InputFile"], "mbox") ) # distinto de MBOX la entrada de la "factura original"
			return 0;
		else if( strcmp($configFile["DataFile"], "xml") ) # contenido del Mapeo distinto a XML
			return 0;
		else {
			# print_r($configFile);
			# echo "\nBuffer de Lectura: ". $configFile["InputFile"];
			# echo "\nData Content: ". $configFile["DataFile"];
			# echo "\nConfiguracion: ". $configFile["ConfigData"];
			# echo "\nTemplate: ". RESOURCES.$configFile["Template"];

			$configXml= simplexml_load_file(RESOURCES.$configFile["ConfigData"]);
			$fileContent= file_get_contents($fileName);
			$simpleXml= simplexml_load_string($fileContent);

			if( !count($configXml->Sections) ) 		return 0;
			else {
				$fileEnd= array();
				$fileAdiciones= array();
				foreach( $configXml->Sections->Section as $key=>$val ) { # leyendo configuracion
					$tmpData=array();

					/**
					* leyendo los atributos de cada elementos hijo <Section>
					*/
					foreach( $val->attributes() as $atKey=>$atVal ) {
						$tmpData[$atKey]= fixTextPrev($atVal);
					}

					/**
					* extraemos datos en base a InitText (dato inicio) y EndText (dato final), tomado de $fileContent y colocado en $out
					* extraido de cada elemento hijo <Section>
					*/
					$aux=array(); # auxiliar
					foreach( $val->Element as $key2=>$val2 ) { # recorremos los Element
						$spTmp=array();
						if( count($val2->Translations) ) { # si existe Translations
							foreach( $val2->Translations->Translation as $atkey3=>$atval3 ) { # recorremos nodos hijos Translation
								$tmp=array();
								foreach( $atval3->attributes() as $atKey=>$atVal ) { # miramos atributos de Translation
									$tmp[$atKey]= fixTextPrev($atVal); # guardamo los atributos
								}

								/**
								* de los atributos encontraos en <Translation> se solo extraemos ciertos argumentos
								*/
								$spTmp[$tmp["Type"]]= array(
									"from"=>$tmp["Source"], 
									"to"=>$tmp["Target"], 
									"spaces"=>(isset($tmp["Spaces"]) ? $tmp["Spaces"]:0)
								);
								unset($tmp);
							}
						}

						/**
						* continuamos con los <Element>
						*/
						$tmp=array();
						foreach( $val2->attributes() as $atKey=>$atVal ) { # recorremos atributos de <Element>
							$tmp[$atKey]= fixTextPrev($atVal); # guardamos todos los atributos de <Element>
						}

						/**
						* armamos el array auxiliar tomando como base el argumento "TagUBL" declarado en cada <Element>
						* y armando el contenido con elementos de interes como: RowNumber, ColumnNumber, Tag, Length y TagUBL
						* y en "sp" guardamos los argumento encontrados en <Translation> como parte de las ordenes del
						* <Element> especifico
						*/
						$aux[$tmp["TagUBL"]]= array( 
							"onUBL"=>$tmp["TagUBL"], 
							"tag"=>$tmp["Tag"], 
							"sp"=>(count($spTmp) ? $spTmp:0)
						);
						unset($tmp, $spTmp);
					}

					/**
					* armamos arreglo principal que practicamente "transcibe" el XML Templae, a un Array ordenado
					* Colocando como elementos principales el rango de lectura "readFrom" y "readTo", los nodos encontrados <Element> en "nodes"
					* y los datos en crudo leidos del segmento Spool que se guarda en "data"
					* Todo se conjuga en este arreglo fileEnd
					*/
					$fileEnd[]= array(
						"type"=>$tmpData["Type"], 
						"root"=>((isset($tmpData["Root"]) && $tmpData["Root"]) ? $tmpData["Root"]:""), 
						"name"=>$tmpData["Name"], 
						"nodes"=>$aux
					);
					unset($aux, $sp, $out);
				}

				#print_r($fileEnd);

				if( !count($fileEnd) ) 	return 0;
				else {
					$factura= array();

					foreach($fileEnd as $key=>$val ) {
						if( !strcmp($val["type"], "Recursive") )  { # busqueda en recursivo - conceptos y uuids
							/**
							* en el modo Recursivo, la busqueda no esperara la aparicion de la variable declarada en la configuracion XML
							* solo indicara las posiciones que correspondan a los Valores asociados a las variables declaradas en la confi
							* guracion del XML
							* Siempre se omitira la primer linea ya que corresponde a la declaracion de variables con satisfaccion Visual
							*/
							$i=0;
							#$i=(!strcmp(strtolower($val["name"]), "eximpuestos") ? 1:0);
							$rootRaiz= NULL;
							$auxRoot= NULL;
							$rootStripe= explode("/", $val["root"]);
							foreach( $rootStripe as $rCrudeX ) {
								if( $rCrudeX ) {
									$auxRoot= (!$rootRaiz ? $simpleXml->$rCrudeX:$rootRaiz->$rCrudeX);
									$rootRaiz= $auxRoot;
								}
							}

							foreach( $rootRaiz as $crudeX=>$crudeY ) {
								#if( substr($crudeY, 0, -1) ) {
								#if( $i ) {
									$conceptos=array();
									$conceptosAdit=array();

									foreach( $val["nodes"] as $key2=>$val2 ) {
										$iterNode= explode("/", $val2["tag"]);
										$ooo= NULL;
										$oooTmp= NULL;

										foreach( $iterNode as $iterData ) {
											if( $iterData ) {
												$oooTmp= (!$ooo ? $crudeY->$iterData:$ooo->$iterData);
												$ooo= $oooTmp;
											}
										}

										$aux= fixTextPrev($ooo);

										if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
											foreach( $val2["sp"] as $com=>$exec ) {
												if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
													$aux= $exec["from"];
												}
											}
										}

										$conceptos[$key2]= $aux;
										$conceptosAdit[$val2["tag"]]= $aux;
										unset($aux, $ooo);
									}

									if( !strcmp(strtolower($val["name"]), "eximpdetail") ) { # extra impuestos anidados en el mismo concepto
										$j=0;
										$aux=array();
										$extrasImpConceptos=array();
										foreach( $conceptos as $exImpDetKey=>$exImpDetVal ) {
											$aux[]= $exImpDetVal;

											if( $j==4 ) {
												$extrasImpConceptos[]= array(
													"nombre"=>$aux[4], 
													"tipo"=>$aux[3], 
													"tasa"=>$aux[2], 
													"importe"=>$aux[1], # base
													"impuesto"=>$aux[0]
												);
												unset($aux);
												$j=0;
											}
											else 
												$j++;
										}

										# copiamos a array estandar
										$factura["conceptos"][($i)]["extra_impuestos"]= $extrasImpConceptos;
										$fileAdiciones["conceptos"][($i)]["extra_impuestos"]= $extrasImpConceptos;
										unset($auxExtraImp, $j, $extrasImpConceptos, $auxExtraImp);
									}
									else if( !strcmp(strtolower($val["name"]), "eximpuestos") ) {
										$factura["impuestos_locales"][]= $conceptos;
										$fileAdiciones["impuestos_locales"][]= $conceptosAdit;
									}
									else {
										$factura["conceptos"][]= $conceptos;
										$fileAdiciones["conceptos"][]= $conceptosAdit;
									}
									unset($conceptos, $conceptosAdit);
								#}
								$i++;
							}
						}
						else if( !strcmp($val["type"], "Single") )  { # busqueda y armado regular
							/**
							* en el modo Single, debera encontrarse en la linea la invovacion de la variable y sera el accionados para 
							* buscar en la posicion indicada en la configuracion del XML el valor resultante a la variable
							*/
							foreach( $val["nodes"] as $key2=>$val2 ) {
								$aux=NULL;
								$lastInfo=NULL;
								$stripCrude= explode("/", $val2["tag"]); # explotamos la ruta del XML

								foreach( $stripCrude as $crudeX ) {
									if( $crudeX ) {
										$lastInfo= (!$aux ? $simpleXml->$crudeX:$aux->$crudeX);
										$aux= $lastInfo;
									}
								}

								if( !strcmp(strtolower($val["name"]), "retenciones") ) { # retenciones
									$factura["retenciones"][0][$key2]= fixTextPrev($aux);
									$fileAdiciones["retenciones"][$val2["tag"]]= fixTextPrev($aux);
								}
								else {
									$factura[$key2]= fixTextPrev($aux);
									$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);
								}

								$aux='';
								if( is_array($val2["sp"]) && count($val2["sp"]) ) { # comandos para traducir
									foreach( $val2["sp"] as $com=>$exec ) {
										if( !strcmp(strtolower($com), "set") ) { # establece dato nuevo
											$aux= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "replace") ) { # reemplaso
											$aux= str_replace($exec["from"], $exec["to"], $factura[$key2]);
										}
										else if( !strcmp(strtolower($com), "nitfix") ) { # corregir nit obtenien do el DV
											$aux= substr($factura[$key2], 0, -1). '-'.substr($factura[$key2], -1);
										}
										else if( !strcmp(strtolower($com), "format") ) { # aplicar formato
											if( !strcmp(strtolower($exec["to"]), "numeric") ) {
												$factura[$key2]= str_replace(",", "", $factura[$key2]);
												$aux= number_format($factura[$key2], 2, '.', '');
											}
										}
										else if( !strcmp(strtolower($com), "setstring") ) { # colocar cadena
											$aux .= $exec["from"];
										}
										else if( !strcmp(strtolower($com), "concat") ) { # concatenar
											$x= (strstr($exec["from"], ",") ? explode(",", $exec["from"]):$exec["from"]);

											if( !is_array($x) ) # simple
												$aux .= $fileAdiciones[$x]; # tomamos nodo y concatenamos
											else { # si es array
												foreach( $x as $nodX ) {
													if( strstr($nodX, "[str:") ) { # si contiene comando para string de datos por entrada personalizada
														$spStr= substr($nodX, 5, -1);
														$aux .= $spStr; # concatenamos especial
														unset($spStr);
													}
													else
														$aux .= $fileAdiciones[$nodX]; # tomamos nodo y concatenamos
												}
											}
										}

										if( isset($exec["spaces"]) && !is_array($exec["spaces"]) ) { # comando para espacios
											if( !strcmp($exec["spaces"], "kill") ) { # quitar
												$aux= str_replace(" ", "", $aux);
											}
											if( !strcmp($exec["spaces"], "reduce") ) { # reducir (a uno solo entre palabras)
												$aux= reduceSpaces($aux);
											}
										}
									}
								if( $aux ) { # si hubo ejecucion
									if( !strcmp(strtolower($val["name"]), "retenciones") && $aux ) { # retenciones
										$factura["retenciones"][0][$key2]= fixTextPrev($aux);
										$fileAdiciones["retenciones"][$val2["tag"]]= fixTextPrev($aux);
									}
									else {
										$factura[$key2]= fixTextPrev($aux);
										$fileAdiciones[$val2["tag"]]= fixTextPrev($aux);
									}
								}
							}
						}
					}
				}
			}

				# print_r($factura);

				if( is_array($factura) && count($factura) ) {
					# print_r($fileAdiciones);
					$template= file_get_contents(RESOURCES.$configFile["Template"]);

					if( !$template )
						echo "-[No existe Template..]-";
					else {
						$fecha=time();
						$newname= SDK.'tmp/'.$fecha; # temporal donde generamos JSON final
						$file= fopen($newname, "w");
						$facturaJson= json_decode($template, true);
						$tmpConceptos= $facturaJson["conceptos"]; # elementos declarados en el JSON
						$facturaJson["conceptos"]= array(); # limpiamos

						foreach( $facturaJson as $key=>$val ) { # colocando datos del emisor
							if( strcmp($key, "conceptos") && strcmp($key, "retenciones") && strcmp($key, "impuestos_locales") ) {
								if( is_array($val) && count($val) ) {
									foreach( $val as $key2=>$val2 )
										$facturaJson[$key][$key2]= fixTextPrev($factura[$val2] ? $factura[$val2]:0); # establecemos el valor
								}
								else
									$facturaJson[$key]= fixTextPrev($factura[$val] ? $factura[$val]:0); # establecemos el valor
							}
						}

						$conceptos=array();
						#echo "\n\n===== DEBUG\n";
						#print_r($tmpConceptos);
						#echo "\n======> Factura Conceptos\n";
						#print_r($factura["conceptos"]);
						#echo "\n\n===== END DEBUG\n\n";
						foreach( $factura["conceptos"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								$aux[$key2]= (isset($val[$val2]) ? fixTextPrev($val[$val2] ? $val[$val2]:0):0);
							}

							if( count($factura["conceptos"][$key]["extra_impuestos"]) ) # impuestos extras
								$aux["extra_impuestos"]= $factura["conceptos"][$key]["extra_impuestos"];

							$conceptos[$key]= $aux;
							unset($aux);
						}
						$facturaJson["conceptos"]= $conceptos;

						/**
						* impuestos locales
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["impuestos_locales"]; # elementos declarados en el JSON
						$facturaJson["impuestos_locales"]= array();
						$i=0;
						foreach( $factura["impuestos_locales"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "tasa") )
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$i]= $aux;
							$i++;
							unset($aux);
						}
						unset($i);
						$facturaJson["impuestos_locales"]= $conceptos;

						/**
						* retenciones
						*/
						$conceptos=array();
						$tmpConceptos= $facturaJson["retenciones"]; # elementos declarados en el JSON
						$facturaJson["retenciones"]= array();
						foreach( $factura["retenciones"] as $key=>$val ) { # recorriendo factura real
							$aux=array();

							foreach( $tmpConceptos[0] as $key2=>$val2 ) {
								if( !strcmp($key2, "tasa") )
									$aux[$key2]= fixTextPrev($val[$val2] ? $val[$val2]:0);
								else {
									$aux[$key2]= str_replace(",", "", fixTextPrev($val[$val2] ? $val[$val2]:0));
								}
							}
							$conceptos[$key]= $aux;
							unset($aux);
						}
						$facturaJson["retenciones"]= $conceptos;

						$hashMd5= md5(json_encode($facturaJson));
						$facturaJson["huella"]= $hashMd5;
						$outArray= print_r($facturaJson, true);
						$outJson= json_encode($facturaJson);

						fputs($file, $outJson, strlen($outJson));
						# fputs($file, $outArray, strlen($outJson));
						fclose($file);
						unset($file);
					}

					$facturaJson["tipo"]= "credito";
					$facturaJson["formato"]= "normal";
					# print_r($facturaJson);

					if( count($facturaJson["conceptos"]) ) { # si hay elementos
						$r= sendToMoneyBox($facturaJson);
					}
					# print_r($r);
				}
			}
		return $r;
		}
	}
}

/**
* envio de factura en formato MBOX
*/
function sendFacturaNds_MBOX($fileName=false) {
	echo "\n---> Enviando Nota DocumentoSoporte MBOX";
}
?>