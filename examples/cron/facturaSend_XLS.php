<?php
/**
* envio de factura en formato XLS
*
* @param string $fileName ruta al archivo
*/
function sendFactura_XLS_byConfig($fileName=false, $configFile=NULL) {
	if( !$fileName ) return false;
	else {
		$r= false;
		$fecha= time();
		$newname= '/tmp/'.$fecha;
		$excel = PHPExcel_IOFactory::load($fileName);
		$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
		$writer->setDelimiter("\t");
		$writer->setEnclosure("");
		$writer->save($newname);

		$template= file_get_contents($GLOBALS['templateFacutraJson']);

		if( !$template )
			echo "-[No existe Template..]-";
		else {
			$file= fopen($newname, "r");
			$facturaJson= json_decode($template, true);
			$facturaJson["conceptos"]= array();

			while( ($buf=fgets($file, (15*1024)))!==FALSE ) {
				if( $buf ) {
					$buf= substr($buf, 0, -1);
					$x= explode("\t", $buf);
					$facturaJson["emisor"]= $x[0];
					$facturaJson["receptor"]["nombre"]= $x[1];
					$facturaJson["receptor"]["nit"]= $x[2];
					$facturaJson["receptor"]["telefono"]= $x[3];
					$facturaJson["receptor"]["email"]= $x[4];
					$facturaJson["receptor"]["calle"]= $x[5];
					$facturaJson["receptor"]["colonia"]= $x[6];
					$facturaJson["receptor"]["num_ext"]= $x[7];
					$facturaJson["receptor"]["cp"]= $x[8];
					$facturaJson["receptor"]["ciudad"]= $x[9];
					$facturaJson["receptor"]["estado"]= $x[10];
					$facturaJson["receptor"]["pais"]= $x[11];
					#$facturaJson["receptor"]["regimen_fiscal"]= $x[10]; # 1=PersonaJuridica, 2=PersonaNatural
					#$facturaJson["receptor"]["perfil"]= $x[11]; # 1=Simplificado, 2=Comun, 3=NoAplicable

					$facturaJson["moneda"]= $x[16]; # 1=MXN, 2=USD, 3=EUR, 5=COP
					$facturaJson["moneda_vcambio"]= $x[17]; # valor de cambio divisa
					$facturaJson["metodo_pago"]= $x[14]; # 10=Efectivo, 1=NoIdentificado, 46=Transferencia, 20=Cheque, ZZZ=NoDefinido, 48=T.Credito, 49=T.Debito
					$facturaJson["forma_pago"]= $x[15]; # PUE=PagoUnicaExhibicion,PPD=PagoenParcialoporDefinir
					$facturaJson["subtotal"]= number_format($x[18], 2, '.', ''); # suma de importes
					$facturaJson["impuesto"]= number_format($x[19], 2, '.', ''); # suma de impuestos de cada importe independiente
					$facturaJson["total"]= number_format($x[20], 2, '.', ''); # suma de subtotal e impuestos

					$conceptos=array();
					$elementosBase= 21;
					$elementosDeConcepto= 7;
					$numConc= ((count($x)-$elementosBase)/$elementosDeConcepto); # numero de conceptos a 

					for( $i=0; $i<$numConc; $i++ ) {
						$inicio= (count($x)-($numConc*$elementosDeConcepto))+($i*$elementosDeConcepto);
						$fin= ($inicio+($elementosDeConcepto-1));

						$conceptos[($i+1)]= array(
							"cantidad"=>$x[$inicio],
							"unidad"=>$x[($inicio+1)],
							"concepto"=>$x[($inicio+2)],
							"pu"=>$x[($inicio+3)],
							"ni"=>$x[($inicio+4)],
							"desc"=>$x[($inicio+5)],
							"cps"=>0,
							"impuesto"=>$x[$fin]
						);
					}

					$facturaJson["conceptos"]= $conceptos;
					$hashMd5= md5(json_encode($facturaJson));
					$facturaJson["huella"]= $hashMd5;
					# print_r($facturaJson);
				}
			}

			fclose($file);
			unset($file);

			if( count($facturaJson["conceptos"]) ) { # si hay elementos
				$r= sendToMoneyBox($facturaJson);
			}
		}
	}
	return $r;
}

/**
* envio de factura en formato XLS
*
* @param string $fileName ruta al archivo
*/
function sendFactura_XLS($fileName=false) {
	if( !$fileName ) return false;
	else {
		$r= false;
		$fecha= time();
		$newname= '/tmp/'.$fecha;
		$excel = PHPExcel_IOFactory::load($fileName);
		$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
		$writer->setDelimiter("\t");
		$writer->setEnclosure("");
		$writer->save($newname);

		$template= file_get_contents($GLOBALS['templateFacutraJson']);

		if( !$template )
			echo "-[No existe Template..]-";
		else {
			$file= fopen($newname, "r");
			$facturaJson= json_decode($template, true);
			$facturaJson["conceptos"]= array();

			while( ($buf=fgets($file, (15*1024)))!==FALSE ) {
				if( $buf ) {
					$buf= substr($buf, 0, -1);
					$x= explode("\t", $buf);
					$facturaJson["emisor"]= $x[0];
					$facturaJson["receptor"]["nombre"]= $x[1];
					$facturaJson["receptor"]["nit"]= $x[2];
					$facturaJson["receptor"]["telefono"]= $x[3];
					$facturaJson["receptor"]["email"]= $x[4];
					$facturaJson["receptor"]["calle"]= $x[5];
					$facturaJson["receptor"]["colonia"]= $x[6];
					$facturaJson["receptor"]["num_ext"]= $x[7];
					$facturaJson["receptor"]["cp"]= $x[8];
					$facturaJson["receptor"]["ciudad"]= $x[9];
					$facturaJson["receptor"]["estado"]= $x[10];
					$facturaJson["receptor"]["pais"]= $x[11];
					#$facturaJson["receptor"]["regimen_fiscal"]= $x[10]; # 1=PersonaJuridica, 2=PersonaNatural
					#$facturaJson["receptor"]["perfil"]= $x[11]; # 1=Simplificado, 2=Comun, 3=NoAplicable

					$facturaJson["moneda"]= $x[16]; # 1=MXN, 2=USD, 3=EUR, 5=COP
					$facturaJson["moneda_vcambio"]= $x[17]; # valor de cambio divisa
					$facturaJson["metodo_pago"]= $x[14]; # 10=Efectivo, 1=NoIdentificado, 46=Transferencia, 20=Cheque, ZZZ=NoDefinido, 48=T.Credito, 49=T.Debito
					$facturaJson["forma_pago"]= $x[15]; # PUE=PagoUnicaExhibicion,PPD=PagoenParcialoporDefinir
					$facturaJson["subtotal"]= number_format($x[18], 2, '.', ''); # suma de importes
					$facturaJson["impuesto"]= number_format($x[19], 2, '.', ''); # suma de impuestos de cada importe independiente
					$facturaJson["total"]= number_format($x[20], 2, '.', ''); # suma de subtotal e impuestos

					$conceptos=array();
					$elementosBase= 21;
					$elementosDeConcepto= 7;
					$numConc= ((count($x)-$elementosBase)/$elementosDeConcepto); # numero de conceptos a 

					for( $i=0; $i<$numConc; $i++ ) {
						$inicio= (count($x)-($numConc*$elementosDeConcepto))+($i*$elementosDeConcepto);
						$fin= ($inicio+($elementosDeConcepto-1));

						$conceptos[($i+1)]= array(
							"cantidad"=>$x[$inicio],
							"unidad"=>$x[($inicio+1)],
							"concepto"=>$x[($inicio+2)],
							"pu"=>$x[($inicio+3)],
							"ni"=>$x[($inicio+4)],
							"desc"=>$x[($inicio+5)],
							"cps"=>0,
							"impuesto"=>$x[$fin]
						);
					}

					$facturaJson["conceptos"]= $conceptos;
					$hashMd5= md5(json_encode($facturaJson));
					$facturaJson["huella"]= $hashMd5;
					# print_r($facturaJson);
				}
			}

			fclose($file);
			unset($file);

			if( count($facturaJson["conceptos"]) ) { # si hay elementos
				$r= sendToMoneyBox($facturaJson);
			}
		}
	}
	return $r;
}
?>