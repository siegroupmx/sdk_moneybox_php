
#!/bin/bash
#
#script para demonizar envio de facturas en linux
#chmod +x cron_me.sh
#
# Moneybox Colombia SAS
# https://www.moneybox.business
#

# ruta al php
PHPRUNME=/usr/bin/php

# indicar la ruta donde esta el deamon del sdk
DEAMONPATH=/home/angel/public_html/sdk_moneybox_php/examples/cron

# indica la carpeta donde tomara las facturas
RUTAFACTURAS=/home/angel/Desktop/miempresa/facturas/
RUTANC=/home/angel/Desktop/miempresa/notascredito/
RUTAND=/home/angel/Desktop/miempresa/notasdebito/
RUTADS=/home/angel/Desktop/miempresa/documentosoporte/
RUTANDS=/home/angel/Desktop/miempresa/notadocumentosoporte/

# convertir de formato XML a MBOX por seguridad
# $PHPRUNME $DEAMONPATH/run.php -xml2mbox all
# convertir de formato JSON a JBOX por seguridad
# $PHPRUNME $DEAMONPATH/run.php -json2jbox all

# buscando nuevas facturas
$PHPRUNME $DEAMONPATH/run.php -localsearch $RUTAFACTURAS
$PHPRUNME $DEAMONPATH/run.php -send factura

# buscando nuevas notas credito
# $PHPRUNME $DEAMONPATH/run.php -localsearch $RUTANC
# $PHPRUNME $DEAMONPATH/run.php -send nc

# buscando nuevas notas debito
# $PHPRUNME $DEAMONPATH/run.php -localsearch $RUTAND
# $PHPRUNME $DEAMONPATH/run.php -send nd

# buscando nuevos documento soporte
#$PHPRUNME $DEAMONPATH/run.php -localsearch $RUTADS
#$PHPRUNME $DEAMONPATH/run.php -send ds

# buscando nuevos nota ajuste documento soporte
#$PHPRUNME $DEAMONPATH/run.php -localsearch $RUTANDS
#$PHPRUNME $DEAMONPATH/run.php -send nds

#$PHPRUNME $DEAMONPATH/run.php -clean all

