#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'demo';
$pass= 'gratis123';

/**
* Agregar
*/
#$path= '';
#$data= array(); # vacio

/**
* Modificar
*/
#$path= '';
#$data= array(); # vacio

/**
* Eliminar
*/
#$path= '';
#$data= array(); # vacio

/**
* Listar Todo
*/
#$path= 'almacen/producto/list';
#$limite= 16;
#$data= array(
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # valores: asc, desc
#   "limite"=>$limite, 
#   "pagina"=>rand(1,5), 
#   "minstock"=>1 );

/**
* Listar por Subcatalogo
* En todas las llamadas al $path que terminan en /get, los valores *opcionales* pueden omitirse
*/
#$path= 'almacen/producto/get';
#$limite= 16;
#$data= array(
#   "catalogo"=>"ID_DEL_CATALOGO",  # identificador del catalogo
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # *opcional* valores: asc, desc
#   "limite"=>$limite,          # *opcional*
#   "pagina"=>rand(1,5),        # *opcional*
#   "minstock"=>1 );            # *opcional*

/**
* Listar por Subcatalogo
*/
#$path= 'almacen/producto/get';
#$limite= 16;
#$data= array(
#   "subcatalogo"=>"ID_DEL_SUBCATALOGO",  # identificador del subcatalogo
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # valores: asc, desc
#   "limite"=>$limite, 
#   "pagina"=>rand(1,5), 
#   "minstock"=>1 );

/**
* Buscar por ID
*/
#$path= 'almacen/producto/get';
#$limite= 16;
#$data= array(
#   "id"=>"ID_DEL_PRODUCTO",    # identificador del producto
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # valores: asc, desc
#   "limite"=>$limite, 
#   "pagina"=>rand(1,5), 
#   "minstock"=>1 );

/**
* Buscar por Nombre/Titulo de producto
*/
#$path= 'almacen/producto/get';
#$limite= 16;
#$data= array(
#   "titulo"=>"Texto_del_Producto_o_Muestra",    # texto o frase que este en el titulo del producto
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # valores: asc, desc
#   "limite"=>$limite, 
#   "pagina"=>rand(1,5), 
#   "minstock"=>1 );

/**
* Buscar por Numero de Parte del Fabricante
*/
#$path= 'almacen/producto/get';
#$limite= 16;
#$data= array(
#   "id_control"=>"Numero_Fabicante",    # numero del fabricante o codigo de barras
#   "orden_por"=>"stock",       # valores: stock, precio, costo_src, titulo, descripcion, fecha, catalogo, subcatalogo, catalogo_urlname, subcatalogo_urlname, peso
#   "orden"=>"desc",            # valores: asc, desc
#   "limite"=>$limite, 
#   "pagina"=>rand(1,5), 
#   "minstock"=>1 );

/**
* Buscar en modo Indexacion
* Asi como haz armado las busquedas en el $data anteriores mencionados, en sus diversas permutaciones (id_control, id, titulo, etc...), puedes hacerlo igual pero
* solo cambiaras el $path
*/
#$path= 'almacen/producto/search';
#$data= array();        # arma este Data asu como se indica en los ejemplos de arriba del tipo "Buscar"


$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
else {
	/* Headers */
	#echo "\n\nHeaders Request:\n";
	#print_r($mbox->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($mbox->getHeaderResponse());
	echo "\n\n";
}
?>
