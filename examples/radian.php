#!/usr/bin/php
<?php
include( "../src/cMoneyBox.php" );

$user= 'democo';
$pass= 'gratis123';
$fileXML= 'egreso.xml';
$fileFirma= 'firma.p12';

// Eventos RADIAN
// 030	Acuse de recibo de Factura Electrónica de Venta, emite R (receptor)
// 031	Reclamo de la Factura Electrónica de Venta, emite R
// 032	Recibo del bien o prestación del servicio, emite R
// 033	Aceptación expresa, emite R
// 034	Aceptación Tácita, emite E (emisor)

$path= 'radian/status';
$data= array(
	"from"=>"uuid", 
	"uuid"=>array(
		0=>"3f7f1d6c3af27ee1cb25bbac419fee894e69c06487133a8f76631bf706f28b890e9832031acfd35871bf6151e851ee9c", 
		1=>"97cacbe735f0eacc2219f21606ea9b8d86f0014fd43652ee2f547a81b69bc16927a1cd6bac7a56468775f02259adde35", 
		2=>"b90d3be993f1c758099ec74155c462b1366c27ed4ceddef5325a602894f46221948e12d99090caf04b67eac5bf1212a7"
	)
);

/**
* enviar estado a radian sobre un documento electronico
*/
// $path= 'radian/send';
// $data= array( 
// 	// "pt"=>array(
// 	// 	"nombre"=>"Nombre del PT o Sistema de Negociacion", 
// 	// 	"nit"=>"NIT-CONDV"
// 	// ), 
// 	// "emisor"=>array(), 
// 	// "receptor"=>array(), 
// 	"document"=>array(
// 		// "from"=>"factura", 
// 		// "from"=>"compra", 
// 		// "from"=>"uuid", 
// 		"from"=>"file", 
// 		// "id_factura"=>"xxxxx", # desde un ID factura de la BDD moneyBox, usa from "factura"
// 		// "id_compra"=>"xxxx", # desde un ID compra de la BDD moneyBox, usa from "compra"
// 		// "uuid"=>"__codigo_fiscal__", # apartir del UUID, usa from "uuid"
// 		"xml"=>base64_encode(file_get_contents($fileXML)), # desde un XML embebido en base64, usa from "file", 
// 		"isFactura"=>1 # solo para evento 034 que emite el Proveedor a su propia factura
// 	), 
// 	"firma"=>array(
// 		// "from"=>"local", 
// 		"from"=>"remote", 
// 		// "id_firma"=>"xxxx", # desde un ID firmade la bdd moneybox,usa from "local"
// 		"file"=>base64_encode(file_get_contents($fileFirma)), # apartir de la firma embebida en base64, usa from "remote"
// 		"pass"=>"prueba123" # password para la firma embebebida
// 	),
// 	"emisor"=>array( # emisor y firmante de los eventos
// 		"nombre"=>"Moneybox Colombia SAS", 
// 		"nit"=>"901239719-2", 
// 		"mail"=>"siegroupmx@gmail.com"
// 	), 
// 	"dian"=>array(
// 		"softid"=>"casdad....asdadad5", # software ID
// 		"pin"=>"3333" # pin del software id
// 	), 
// 	"need"=>"xml", # xml=solo el ApResp, all=AR firmado y transaccionado
// 	"eventos"=>array( // los eventos se ejecutaran conforme se indico en su indice del 0 al 3
// 		// "0"=>array(
// 		// 	"serie"=>"RAD", 
// 		// 	"folio"=>"58", 
// 		// 	"clave"=>"030", # acuse de recibido
// 		// 	"sendmail"=>0, # no enviar correo automatico del evento al interesado
// 		// 	"contacto"=>array(
// 		// 		"nombre"=>"Angel", 
// 		// 		"apellidos"=>"Cantu Jauregui", 
// 		// 		"mail"=>"contacto@moneybox.business", 
// 		// 		"telefono"=>"899 871 1722", 
// 		// 		"cargo"=>"Propietario", 
// 		// 		"arealaboral"=>"Gerencia"
// 		// 	)
// 		// ),
// 		// "1"=>array(
// 		// 	"serie"=>"RAD", 
// 		// 	"folio"=>"59", 
// 		// 	"clave"=>"032", # recibo de bienes
// 		// 	"contacto"=>array(
// 		// 		"nombre"=>"Angel", 
// 		// 		"apellidos"=>"Cantu Jauregui", 
// 		// 		"mail"=>"contacto@moneybox.business", 
// 		// 		"telefono"=>"899 871 1722", 
// 		// 		"cargo"=>"Propietario", 
// 		// 		"arealaboral"=>"Gerencia"
// 		// 	)
// 		// ),
// 		// "2"=>array(
// 		// 	"serie"=>"RAD", 
// 		// 	"folio"=>"60", 
// 		// 	"clave"=>"033", 
// 		// 	"contacto"=>array(
// 		// 		"nombre"=>"Angel", 
// 		// 		"apellidos"=>"Cantu Jauregui", 
// 		// 		"mail"=>"contacto@moneybox.business", 
// 		// 		"telefono"=>"899 871 1722", 
// 		// 		"cargo"=>"Propietario", 
// 		// 		"arealaboral"=>"Gerencia"
// 		// 	)
// 		// )
// 		// "3"=>array(
// 		// 	"serie"=>"DE", 
// 		// 	"folio"=>"0004", 
// 		// 	"clave"=>"031", # reclamo
// 		// 	"reclamo"=>"02" # 02=mercancia no entregada totalmente
// 		// )
// 	)
// );

$mbox= new moneyBox($user, $pass, $path, $data);
if( $mbox->getError() ) 	echo '[Error] '. $mbox->getError();
// else {
// 	echo "\nExito:";
// 	echo "\n\nData en Array:\n";
// 	print_r($mbox->getRespuesta());
// 	echo "\n\nData en JSON:\n";
// 	print_r($mbox->getRespuesta("json"));
// }

/* Headers */
echo "\n\nHeaders Request:\n";
print_r($mbox->getHeaderRequest());

echo "\n\nHeaders Response:\n";
print_r($mbox->getHeaderResponse());
echo "\n\n";
?>
